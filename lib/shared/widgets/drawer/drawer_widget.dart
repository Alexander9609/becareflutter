import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:flutter/material.dart';

class MovilRcv2Drawer extends StatelessWidget {
   MovilRcv2Drawer({Key key}) : super(key: key);

   static final double iconSize = 30;

  @override
  Widget build(BuildContext context) {
    return BlocListener<DrawerBloc, DrawerState>(
      listener: (context, state) {
        if(state.isLogout) {
          Navigator.popUntil(context, ModalRoute.withName(MovilRcv2Routes.login));
          Navigator.pushNamed(context, MovilRcv2Routes.login);
        }
      },
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            header(context),
            ListTile(
              leading: Icon(Icons.home, size: iconSize,),
              title: Text('Home'),
              onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.main),
            ),
            ListTile(
              leading: Icon(Icons.settings, size: iconSize,),
              title: Text('Settings'),
              onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.settings),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app, size: iconSize,),
          title: Text('Sign out'),
              onTap: () => context.bloc<DrawerBloc>().add(Logout())
            ),
            Divider(thickness: 2),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Alarmas'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesAlarmaList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Condicions'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesCondicionList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Dispositivos'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesDispositivosList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('EncuestaSintomas'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEncuestaSintomasList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('EstadioPresionArterials'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEstadioPresionArterialList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Eventos'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEventosList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Fisiometria1s'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesFisiometria1List),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Ips'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesIpsList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Medicacions'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesMedicacionList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('Pacientes'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesPacienteList),
            ),
            ListTile(
                leading: Icon(Icons.label, size: iconSize,),
                title: Text('PresionSanguineas'),
                onTap: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesPresionSanguineaList),
            ),
            // jhipster-merlin-needle-menu-entry-add
          ],
        ),
      ),
    );
  }

  Widget header(BuildContext context) {
    return DrawerHeader(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
      ),
      child: Text('Menu',
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline2,
      ),
    );
  }
}
