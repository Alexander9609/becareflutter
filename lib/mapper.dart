import 'package:dart_json_mapper/dart_json_mapper.dart';
import 'package:movilRCV2/shared/models/user.dart';
import 'package:movilRCV2/entities/alarma/alarma_model.dart';
import 'package:movilRCV2/entities/condicion/condicion_model.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';
import 'package:movilRCV2/entities/eventos/eventos_model.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';
import 'package:movilRCV2/entities/ips/ips_model.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';
import 'package:movilRCV2/entities/paciente/paciente_model.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';
// jhipster-merlin-needle-mapper-import-add

void configMapper() {
// YOU HAVE TO DECLARE YOUR DTOs LIST HERE FOR CORRECT SERIALIZATION / DESERIALIZATION
  JsonMapper().useAdapter(JsonMapperAdapter(valueDecorators: {
         typeOf<List<User>>(): (value) => value.cast<User>(),
          typeOf<List<Alarma>>(): (value) => value.cast<Alarma>(),
          typeOf<List<Condicion>>(): (value) => value.cast<Condicion>(),
          typeOf<List<Dispositivos>>(): (value) => value.cast<Dispositivos>(),
          typeOf<List<EncuestaSintomas>>(): (value) => value.cast<EncuestaSintomas>(),
          typeOf<List<EstadioPresionArterial>>(): (value) => value.cast<EstadioPresionArterial>(),
          typeOf<List<Eventos>>(): (value) => value.cast<Eventos>(),
          typeOf<List<Fisiometria1>>(): (value) => value.cast<Fisiometria1>(),
          typeOf<List<Ips>>(): (value) => value.cast<Ips>(),
          typeOf<List<Medicacion>>(): (value) => value.cast<Medicacion>(),
          typeOf<List<Paciente>>(): (value) => value.cast<Paciente>(),
          typeOf<List<PresionSanguinea>>(): (value) => value.cast<PresionSanguinea>(),
          // jhipster-merlin-needle-mapper-list-add
 } ,converters: {
// CODE HERE AS AN EXAMPLE YOU HAVE TO DECLARE YOUR ENUMS HERE FOR CORRECT SERIALIZATION / DESERIALIZATION
//  PackState: EnumConverter(PackState.values)
// jhipster-merlin-needle-mapper-enum-add
  }));
}

class EnumConverter implements ICustomConverter<dynamic> {
  List<dynamic> compareTO;

  EnumConverter(List<dynamic> compareTO) : super() {
    this.compareTO = compareTO;
  }

  @override
  dynamic fromJSON(jsonValue, [JsonProperty jsonProperty]) {
    String jsonValueString = jsonValue.toString().replaceAll('"', '');
    var result;
    if (jsonValueString.indexOf('.') != - 1) {
      result = compareTO.firstWhere((v) => jsonValueString == v.toString(), orElse: () => null);
    } else {
      result =  compareTO.firstWhere((v) => jsonValueString == v.toString().split('.').last, orElse: () => null);
    }
    return result;
  }

  @override
  String toJSON(dynamic object, [JsonProperty jsonProperty]) {
    if(object != null){
      return object.toString().split('.').last;
    } else {
      return '';
    }
  }
}
