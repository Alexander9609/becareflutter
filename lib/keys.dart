import 'package:flutter/widgets.dart';

class MovilRcv2Keys {
  static const loginScreen = Key('__loginScreen__');
  static const registerScreen = Key('__registerScreen__');
  static const mainScreen = Key('__mainScreen__');
  static const settingsScreen = Key('__settingsScreen__');

  //Entities
  static const alarmaListScreen = Key('__alarmaListScreen__'); 
  static const alarmaCreateScreen = Key('__alarmaCreateScreen__'); 
  static const alarmaViewScreen = Key('__alarmaViewScreen__');
  static const condicionListScreen = Key('__condicionListScreen__'); 
  static const condicionCreateScreen = Key('__condicionCreateScreen__'); 
  static const condicionViewScreen = Key('__condicionViewScreen__');
  static const dispositivosListScreen = Key('__dispositivosListScreen__'); 
  static const dispositivosCreateScreen = Key('__dispositivosCreateScreen__'); 
  static const dispositivosViewScreen = Key('__dispositivosViewScreen__');
  static const encuestaSintomasListScreen = Key('__encuestaSintomasListScreen__'); 
  static const encuestaSintomasCreateScreen = Key('__encuestaSintomasCreateScreen__'); 
  static const encuestaSintomasViewScreen = Key('__encuestaSintomasViewScreen__');
  static const estadioPresionArterialListScreen = Key('__estadioPresionArterialListScreen__'); 
  static const estadioPresionArterialCreateScreen = Key('__estadioPresionArterialCreateScreen__'); 
  static const estadioPresionArterialViewScreen = Key('__estadioPresionArterialViewScreen__');
  static const eventosListScreen = Key('__eventosListScreen__'); 
  static const eventosCreateScreen = Key('__eventosCreateScreen__'); 
  static const eventosViewScreen = Key('__eventosViewScreen__');
  static const fisiometria1ListScreen = Key('__fisiometria1ListScreen__'); 
  static const fisiometria1CreateScreen = Key('__fisiometria1CreateScreen__'); 
  static const fisiometria1ViewScreen = Key('__fisiometria1ViewScreen__');
  static const iPSListScreen = Key('__ipsListScreen__'); 
  static const iPSCreateScreen = Key('__ipsCreateScreen__'); 
  static const iPSViewScreen = Key('__ipsViewScreen__');
  static const medicacionListScreen = Key('__medicacionListScreen__'); 
  static const medicacionCreateScreen = Key('__medicacionCreateScreen__'); 
  static const medicacionViewScreen = Key('__medicacionViewScreen__');
  static const pacienteListScreen = Key('__pacienteListScreen__'); 
  static const pacienteCreateScreen = Key('__pacienteCreateScreen__'); 
  static const pacienteViewScreen = Key('__pacienteViewScreen__');
  static const presionSanguineaListScreen = Key('__presionSanguineaListScreen__'); 
  static const presionSanguineaCreateScreen = Key('__presionSanguineaCreateScreen__'); 
  static const presionSanguineaViewScreen = Key('__presionSanguineaViewScreen__');
  // jhipster-merlin-needle-key-add
}
