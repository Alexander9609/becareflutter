import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_repository.dart';
import 'package:movilRCV2/entities/presion_sanguinea/bloc/presion_sanguinea_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'presion_sanguinea_events.dart';
part 'presion_sanguinea_state.dart';

class PresionSanguineaBloc extends Bloc<PresionSanguineaEvent, PresionSanguineaState> {
  final PresionSanguineaRepository _presionSanguineaRepository;

  final presionSistolicaMaxController = TextEditingController();
  final presionSistolicaMinController = TextEditingController();
  final presionSistolicaMedController = TextEditingController();
  final presionDiastolicaMaxController = TextEditingController();
  final presionDiastolicaMinController = TextEditingController();
  final presionDiastolicaMedController = TextEditingController();
  final timeInstantController = TextEditingController();

  PresionSanguineaBloc({@required PresionSanguineaRepository presionSanguineaRepository}) : assert(presionSanguineaRepository != null),
        _presionSanguineaRepository = presionSanguineaRepository, 
  super(PresionSanguineaState(null,));

  @override
  void onTransition(Transition<PresionSanguineaEvent, PresionSanguineaState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<PresionSanguineaState> mapEventToState(PresionSanguineaEvent event) async* {
    if (event is InitPresionSanguineaList) {
      yield* onInitList(event);
    } else if (event is PresionSanguineaFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadPresionSanguineaByIdForEdit) {
      yield* onLoadPresionSanguineaIdForEdit(event);
    } else if (event is DeletePresionSanguineaById) {
      yield* onDeletePresionSanguineaId(event);
    } else if (event is LoadPresionSanguineaByIdForView) {
      yield* onLoadPresionSanguineaIdForView(event);
    }else if (event is PresionSistolicaMaxChanged){
      yield* onPresionSistolicaMaxChange(event);
    }else if (event is PresionSistolicaMinChanged){
      yield* onPresionSistolicaMinChange(event);
    }else if (event is PresionSistolicaMedChanged){
      yield* onPresionSistolicaMedChange(event);
    }else if (event is PresionDiastolicaMaxChanged){
      yield* onPresionDiastolicaMaxChange(event);
    }else if (event is PresionDiastolicaMinChanged){
      yield* onPresionDiastolicaMinChange(event);
    }else if (event is PresionDiastolicaMedChanged){
      yield* onPresionDiastolicaMedChange(event);
    }else if (event is TimeInstantChanged){
      yield* onTimeInstantChange(event);
    }  }

  Stream<PresionSanguineaState> onInitList(InitPresionSanguineaList event) async* {
    yield this.state.copyWith(presionSanguineaStatusUI: PresionSanguineaStatusUI.loading);
    List<PresionSanguinea> presionSanguineas = await _presionSanguineaRepository.getAllPresionSanguineas();
    yield this.state.copyWith(presionSanguineas: presionSanguineas, presionSanguineaStatusUI: PresionSanguineaStatusUI.done);
  }

  Stream<PresionSanguineaState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        PresionSanguinea result;
        if(this.state.editMode) {
          PresionSanguinea newPresionSanguinea = PresionSanguinea(state.loadedPresionSanguinea.id,
            this.state.presionSistolicaMax.value,  
            this.state.presionSistolicaMin.value,  
            this.state.presionSistolicaMed.value,  
            this.state.presionDiastolicaMax.value,  
            this.state.presionDiastolicaMin.value,  
            this.state.presionDiastolicaMed.value,  
            this.state.timeInstant.value,  
            null, 
          );

          result = await _presionSanguineaRepository.update(newPresionSanguinea);
        } else {
          PresionSanguinea newPresionSanguinea = PresionSanguinea(null,
            this.state.presionSistolicaMax.value,  
            this.state.presionSistolicaMin.value,  
            this.state.presionSistolicaMed.value,  
            this.state.presionDiastolicaMax.value,  
            this.state.presionDiastolicaMin.value,  
            this.state.presionDiastolicaMed.value,  
            this.state.timeInstant.value,  
            null, 
          );

          result = await _presionSanguineaRepository.create(newPresionSanguinea);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<PresionSanguineaState> onLoadPresionSanguineaIdForEdit(LoadPresionSanguineaByIdForEdit event) async* {
    yield this.state.copyWith(presionSanguineaStatusUI: PresionSanguineaStatusUI.loading);
    PresionSanguinea loadedPresionSanguinea = await _presionSanguineaRepository.getPresionSanguinea(event.id);

    final presionSistolicaMax = PresionSistolicaMaxInput.dirty(loadedPresionSanguinea?.presionSistolicaMax != null ? loadedPresionSanguinea.presionSistolicaMax: '');
    final presionSistolicaMin = PresionSistolicaMinInput.dirty(loadedPresionSanguinea?.presionSistolicaMin != null ? loadedPresionSanguinea.presionSistolicaMin: '');
    final presionSistolicaMed = PresionSistolicaMedInput.dirty(loadedPresionSanguinea?.presionSistolicaMed != null ? loadedPresionSanguinea.presionSistolicaMed: '');
    final presionDiastolicaMax = PresionDiastolicaMaxInput.dirty(loadedPresionSanguinea?.presionDiastolicaMax != null ? loadedPresionSanguinea.presionDiastolicaMax: '');
    final presionDiastolicaMin = PresionDiastolicaMinInput.dirty(loadedPresionSanguinea?.presionDiastolicaMin != null ? loadedPresionSanguinea.presionDiastolicaMin: '');
    final presionDiastolicaMed = PresionDiastolicaMedInput.dirty(loadedPresionSanguinea?.presionDiastolicaMed != null ? loadedPresionSanguinea.presionDiastolicaMed: '');
    final timeInstant = TimeInstantInput.dirty(loadedPresionSanguinea?.timeInstant != null ? loadedPresionSanguinea.timeInstant: null);

    yield this.state.copyWith(loadedPresionSanguinea: loadedPresionSanguinea, editMode: true,
      presionSistolicaMax: presionSistolicaMax,
      presionSistolicaMin: presionSistolicaMin,
      presionSistolicaMed: presionSistolicaMed,
      presionDiastolicaMax: presionDiastolicaMax,
      presionDiastolicaMin: presionDiastolicaMin,
      presionDiastolicaMed: presionDiastolicaMed,
      timeInstant: timeInstant,
    presionSanguineaStatusUI: PresionSanguineaStatusUI.done);

    presionSistolicaMaxController.text = loadedPresionSanguinea.presionSistolicaMax;
    presionSistolicaMinController.text = loadedPresionSanguinea.presionSistolicaMin;
    presionSistolicaMedController.text = loadedPresionSanguinea.presionSistolicaMed;
    presionDiastolicaMaxController.text = loadedPresionSanguinea.presionDiastolicaMax;
    presionDiastolicaMinController.text = loadedPresionSanguinea.presionDiastolicaMin;
    presionDiastolicaMedController.text = loadedPresionSanguinea.presionDiastolicaMed;
    timeInstantController.text = DateFormat.yMMMMd('en').format(loadedPresionSanguinea?.timeInstant);
  }

  Stream<PresionSanguineaState> onDeletePresionSanguineaId(DeletePresionSanguineaById event) async* {
    try {
      await _presionSanguineaRepository.delete(event.id);
      this.add(InitPresionSanguineaList());
      yield this.state.copyWith(deleteStatus: PresionSanguineaDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: PresionSanguineaDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: PresionSanguineaDeleteStatus.none);
  }

  Stream<PresionSanguineaState> onLoadPresionSanguineaIdForView(LoadPresionSanguineaByIdForView event) async* {
    yield this.state.copyWith(presionSanguineaStatusUI: PresionSanguineaStatusUI.loading);
    try {
      PresionSanguinea loadedPresionSanguinea = await _presionSanguineaRepository.getPresionSanguinea(event.id);
      yield this.state.copyWith(loadedPresionSanguinea: loadedPresionSanguinea, presionSanguineaStatusUI: PresionSanguineaStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedPresionSanguinea: null, presionSanguineaStatusUI: PresionSanguineaStatusUI.error);
    }
  }


  Stream<PresionSanguineaState> onPresionSistolicaMaxChange(PresionSistolicaMaxChanged event) async* {
    final presionSistolicaMax = PresionSistolicaMaxInput.dirty(event.presionSistolicaMax);
    yield this.state.copyWith(
      presionSistolicaMax: presionSistolicaMax,
      formStatus: Formz.validate([
        presionSistolicaMax,
      this.state.presionSistolicaMin,
      this.state.presionSistolicaMed,
      this.state.presionDiastolicaMax,
      this.state.presionDiastolicaMin,
      this.state.presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onPresionSistolicaMinChange(PresionSistolicaMinChanged event) async* {
    final presionSistolicaMin = PresionSistolicaMinInput.dirty(event.presionSistolicaMin);
    yield this.state.copyWith(
      presionSistolicaMin: presionSistolicaMin,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
        presionSistolicaMin,
      this.state.presionSistolicaMed,
      this.state.presionDiastolicaMax,
      this.state.presionDiastolicaMin,
      this.state.presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onPresionSistolicaMedChange(PresionSistolicaMedChanged event) async* {
    final presionSistolicaMed = PresionSistolicaMedInput.dirty(event.presionSistolicaMed);
    yield this.state.copyWith(
      presionSistolicaMed: presionSistolicaMed,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
      this.state.presionSistolicaMin,
        presionSistolicaMed,
      this.state.presionDiastolicaMax,
      this.state.presionDiastolicaMin,
      this.state.presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onPresionDiastolicaMaxChange(PresionDiastolicaMaxChanged event) async* {
    final presionDiastolicaMax = PresionDiastolicaMaxInput.dirty(event.presionDiastolicaMax);
    yield this.state.copyWith(
      presionDiastolicaMax: presionDiastolicaMax,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
      this.state.presionSistolicaMin,
      this.state.presionSistolicaMed,
        presionDiastolicaMax,
      this.state.presionDiastolicaMin,
      this.state.presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onPresionDiastolicaMinChange(PresionDiastolicaMinChanged event) async* {
    final presionDiastolicaMin = PresionDiastolicaMinInput.dirty(event.presionDiastolicaMin);
    yield this.state.copyWith(
      presionDiastolicaMin: presionDiastolicaMin,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
      this.state.presionSistolicaMin,
      this.state.presionSistolicaMed,
      this.state.presionDiastolicaMax,
        presionDiastolicaMin,
      this.state.presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onPresionDiastolicaMedChange(PresionDiastolicaMedChanged event) async* {
    final presionDiastolicaMed = PresionDiastolicaMedInput.dirty(event.presionDiastolicaMed);
    yield this.state.copyWith(
      presionDiastolicaMed: presionDiastolicaMed,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
      this.state.presionSistolicaMin,
      this.state.presionSistolicaMed,
      this.state.presionDiastolicaMax,
      this.state.presionDiastolicaMin,
        presionDiastolicaMed,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<PresionSanguineaState> onTimeInstantChange(TimeInstantChanged event) async* {
    final timeInstant = TimeInstantInput.dirty(event.timeInstant);
    yield this.state.copyWith(
      timeInstant: timeInstant,
      formStatus: Formz.validate([
      this.state.presionSistolicaMax,
      this.state.presionSistolicaMin,
      this.state.presionSistolicaMed,
      this.state.presionDiastolicaMax,
      this.state.presionDiastolicaMin,
      this.state.presionDiastolicaMed,
        timeInstant,
      ]),
    );
  }

  @override
  Future<void> close() {
    presionSistolicaMaxController.dispose();
    presionSistolicaMinController.dispose();
    presionSistolicaMedController.dispose();
    presionDiastolicaMaxController.dispose();
    presionDiastolicaMinController.dispose();
    presionDiastolicaMedController.dispose();
    timeInstantController.dispose();
    return super.close();
  }

}