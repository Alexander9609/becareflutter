import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';

enum PresionSistolicaMaxValidationError { invalid }
class PresionSistolicaMaxInput extends FormzInput<String, PresionSistolicaMaxValidationError> {
  const PresionSistolicaMaxInput.pure() : super.pure('');
  const PresionSistolicaMaxInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionSistolicaMaxValidationError validator(String value) {
    return null;
  }
}

enum PresionSistolicaMinValidationError { invalid }
class PresionSistolicaMinInput extends FormzInput<String, PresionSistolicaMinValidationError> {
  const PresionSistolicaMinInput.pure() : super.pure('');
  const PresionSistolicaMinInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionSistolicaMinValidationError validator(String value) {
    return null;
  }
}

enum PresionSistolicaMedValidationError { invalid }
class PresionSistolicaMedInput extends FormzInput<String, PresionSistolicaMedValidationError> {
  const PresionSistolicaMedInput.pure() : super.pure('');
  const PresionSistolicaMedInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionSistolicaMedValidationError validator(String value) {
    return null;
  }
}

enum PresionDiastolicaMaxValidationError { invalid }
class PresionDiastolicaMaxInput extends FormzInput<String, PresionDiastolicaMaxValidationError> {
  const PresionDiastolicaMaxInput.pure() : super.pure('');
  const PresionDiastolicaMaxInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionDiastolicaMaxValidationError validator(String value) {
    return null;
  }
}

enum PresionDiastolicaMinValidationError { invalid }
class PresionDiastolicaMinInput extends FormzInput<String, PresionDiastolicaMinValidationError> {
  const PresionDiastolicaMinInput.pure() : super.pure('');
  const PresionDiastolicaMinInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionDiastolicaMinValidationError validator(String value) {
    return null;
  }
}

enum PresionDiastolicaMedValidationError { invalid }
class PresionDiastolicaMedInput extends FormzInput<String, PresionDiastolicaMedValidationError> {
  const PresionDiastolicaMedInput.pure() : super.pure('');
  const PresionDiastolicaMedInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionDiastolicaMedValidationError validator(String value) {
    return null;
  }
}

enum TimeInstantValidationError { invalid }
class TimeInstantInput extends FormzInput<DateTime, TimeInstantValidationError> {
  TimeInstantInput.pure() : super.pure(DateTime.now());
  TimeInstantInput.dirty([DateTime value]) : super.dirty(value);

  @override
  TimeInstantValidationError validator(DateTime value) {
    return null;
  }
}

