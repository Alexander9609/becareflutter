part of 'presion_sanguinea_bloc.dart';

abstract class PresionSanguineaEvent extends Equatable {
  const PresionSanguineaEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitPresionSanguineaList extends PresionSanguineaEvent {}

class PresionSistolicaMaxChanged extends PresionSanguineaEvent {
  final String presionSistolicaMax;
  
  const PresionSistolicaMaxChanged({@required this.presionSistolicaMax});
  
  @override
  List<Object> get props => [presionSistolicaMax];
}
class PresionSistolicaMinChanged extends PresionSanguineaEvent {
  final String presionSistolicaMin;
  
  const PresionSistolicaMinChanged({@required this.presionSistolicaMin});
  
  @override
  List<Object> get props => [presionSistolicaMin];
}
class PresionSistolicaMedChanged extends PresionSanguineaEvent {
  final String presionSistolicaMed;
  
  const PresionSistolicaMedChanged({@required this.presionSistolicaMed});
  
  @override
  List<Object> get props => [presionSistolicaMed];
}
class PresionDiastolicaMaxChanged extends PresionSanguineaEvent {
  final String presionDiastolicaMax;
  
  const PresionDiastolicaMaxChanged({@required this.presionDiastolicaMax});
  
  @override
  List<Object> get props => [presionDiastolicaMax];
}
class PresionDiastolicaMinChanged extends PresionSanguineaEvent {
  final String presionDiastolicaMin;
  
  const PresionDiastolicaMinChanged({@required this.presionDiastolicaMin});
  
  @override
  List<Object> get props => [presionDiastolicaMin];
}
class PresionDiastolicaMedChanged extends PresionSanguineaEvent {
  final String presionDiastolicaMed;
  
  const PresionDiastolicaMedChanged({@required this.presionDiastolicaMed});
  
  @override
  List<Object> get props => [presionDiastolicaMed];
}
class TimeInstantChanged extends PresionSanguineaEvent {
  final DateTime timeInstant;
  
  const TimeInstantChanged({@required this.timeInstant});
  
  @override
  List<Object> get props => [timeInstant];
}

class PresionSanguineaFormSubmitted extends PresionSanguineaEvent {}

class LoadPresionSanguineaByIdForEdit extends PresionSanguineaEvent {
  final int id;

  const LoadPresionSanguineaByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeletePresionSanguineaById extends PresionSanguineaEvent {
  final int id;

  const DeletePresionSanguineaById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadPresionSanguineaByIdForView extends PresionSanguineaEvent {
  final int id;

  const LoadPresionSanguineaByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
