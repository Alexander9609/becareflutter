part of 'presion_sanguinea_bloc.dart';

enum PresionSanguineaStatusUI {init, loading, error, done}
enum PresionSanguineaDeleteStatus {ok, ko, none}

class PresionSanguineaState extends Equatable {
  final List<PresionSanguinea> presionSanguineas;
  final PresionSanguinea loadedPresionSanguinea;
  final bool editMode;
  final PresionSanguineaDeleteStatus deleteStatus;
  final PresionSanguineaStatusUI presionSanguineaStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final PresionSistolicaMaxInput presionSistolicaMax;
  final PresionSistolicaMinInput presionSistolicaMin;
  final PresionSistolicaMedInput presionSistolicaMed;
  final PresionDiastolicaMaxInput presionDiastolicaMax;
  final PresionDiastolicaMinInput presionDiastolicaMin;
  final PresionDiastolicaMedInput presionDiastolicaMed;
  final TimeInstantInput timeInstant;

  
  PresionSanguineaState(
TimeInstantInput timeInstant,{
    this.presionSanguineas = const [],
    this.presionSanguineaStatusUI = PresionSanguineaStatusUI.init,
    this.loadedPresionSanguinea = const PresionSanguinea(0,'','','','','','',null,null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = PresionSanguineaDeleteStatus.none,
    this.presionSistolicaMax = const PresionSistolicaMaxInput.pure(),
    this.presionSistolicaMin = const PresionSistolicaMinInput.pure(),
    this.presionSistolicaMed = const PresionSistolicaMedInput.pure(),
    this.presionDiastolicaMax = const PresionDiastolicaMaxInput.pure(),
    this.presionDiastolicaMin = const PresionDiastolicaMinInput.pure(),
    this.presionDiastolicaMed = const PresionDiastolicaMedInput.pure(),
  }):this.timeInstant = timeInstant ?? TimeInstantInput.pure()
;

  PresionSanguineaState copyWith({
    List<PresionSanguinea> presionSanguineas,
    PresionSanguineaStatusUI presionSanguineaStatusUI,
    bool editMode,
    PresionSanguineaDeleteStatus deleteStatus,
    PresionSanguinea loadedPresionSanguinea,
    FormzStatus formStatus,
    String generalNotificationKey,
    PresionSistolicaMaxInput presionSistolicaMax,
    PresionSistolicaMinInput presionSistolicaMin,
    PresionSistolicaMedInput presionSistolicaMed,
    PresionDiastolicaMaxInput presionDiastolicaMax,
    PresionDiastolicaMinInput presionDiastolicaMin,
    PresionDiastolicaMedInput presionDiastolicaMed,
    TimeInstantInput timeInstant,
  }) {
    return PresionSanguineaState(
        timeInstant,
      presionSanguineas: presionSanguineas ?? this.presionSanguineas,
      presionSanguineaStatusUI: presionSanguineaStatusUI ?? this.presionSanguineaStatusUI,
      loadedPresionSanguinea: loadedPresionSanguinea ?? this.loadedPresionSanguinea,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      presionSistolicaMax: presionSistolicaMax ?? this.presionSistolicaMax,
      presionSistolicaMin: presionSistolicaMin ?? this.presionSistolicaMin,
      presionSistolicaMed: presionSistolicaMed ?? this.presionSistolicaMed,
      presionDiastolicaMax: presionDiastolicaMax ?? this.presionDiastolicaMax,
      presionDiastolicaMin: presionDiastolicaMin ?? this.presionDiastolicaMin,
      presionDiastolicaMed: presionDiastolicaMed ?? this.presionDiastolicaMed,
    );
  }

  @override
  List<Object> get props => [presionSanguineas, presionSanguineaStatusUI,
     loadedPresionSanguinea, editMode, deleteStatus, formStatus, generalNotificationKey, 
presionSistolicaMax,presionSistolicaMin,presionSistolicaMed,presionDiastolicaMax,presionDiastolicaMin,presionDiastolicaMed,timeInstant,];

  @override
  bool get stringify => true;
}
