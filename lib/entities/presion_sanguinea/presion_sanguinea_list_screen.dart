import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/presion_sanguinea/bloc/presion_sanguinea_bloc.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class PresionSanguineaListScreen extends StatelessWidget {
    PresionSanguineaListScreen({Key key}) : super(key: MovilRcv2Keys.presionSanguineaListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<PresionSanguineaBloc, PresionSanguineaState>(
      listener: (context, state) {
        if(state.deleteStatus == PresionSanguineaDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('PresionSanguinea deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('PresionSanguineas List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
              buildWhen: (previous, current) => previous.presionSanguineas != current.presionSanguineas,
              builder: (context, state) {
                return Visibility(
                  visible: state.presionSanguineaStatusUI == PresionSanguineaStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (PresionSanguinea presionSanguinea in state.presionSanguineas) presionSanguineaCard(presionSanguinea, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesPresionSanguineaCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget presionSanguineaCard(PresionSanguinea presionSanguinea, BuildContext context) {
    PresionSanguineaBloc presionSanguineaBloc = BlocProvider.of<PresionSanguineaBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Presion Sistolica Max : ${presionSanguinea.presionSistolicaMax.toString()}'),
                  subtitle: Text('Presion Sistolica Min : ${presionSanguinea.presionSistolicaMin.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesPresionSanguineaView,
                    arguments: EntityArguments(presionSanguinea.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesPresionSanguineaEdit,
                    arguments: EntityArguments(presionSanguinea.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(presionSanguineaBloc, context, presionSanguinea.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(PresionSanguineaBloc presionSanguineaBloc, BuildContext context, int id) {
    return BlocProvider<PresionSanguineaBloc>.value(
      value: presionSanguineaBloc,
      child: AlertDialog(
        title: new Text('Delete PresionSanguineas'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              presionSanguineaBloc.add(DeletePresionSanguineaById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
