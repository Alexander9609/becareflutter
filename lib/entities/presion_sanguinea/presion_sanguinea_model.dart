import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:movilRCV2/shared/models/user.dart';

@jsonSerializable
class PresionSanguinea {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'presionSistolicaMax')
  final String presionSistolicaMax;

  @JsonProperty(name: 'presionSistolicaMin')
  final String presionSistolicaMin;

  @JsonProperty(name: 'presionSistolicaMed')
  final String presionSistolicaMed;

  @JsonProperty(name: 'presionDiastolicaMax')
  final String presionDiastolicaMax;

  @JsonProperty(name: 'presionDiastolicaMin')
  final String presionDiastolicaMin;

  @JsonProperty(name: 'presionDiastolicaMed')
  final String presionDiastolicaMed;

  @JsonProperty(name: 'timeInstant', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime timeInstant;

  @JsonProperty(name: 'user')
  final User user;
        
 const PresionSanguinea (
     this.id,
        this.presionSistolicaMax,
        this.presionSistolicaMin,
        this.presionSistolicaMed,
        this.presionDiastolicaMax,
        this.presionDiastolicaMin,
        this.presionDiastolicaMed,
        this.timeInstant,
        this.user,
    );

@override
String toString() {
    return 'PresionSanguinea{'+
    'id: $id,' +
        'presionSistolicaMax: $presionSistolicaMax,' +
        'presionSistolicaMin: $presionSistolicaMin,' +
        'presionSistolicaMed: $presionSistolicaMed,' +
        'presionDiastolicaMax: $presionDiastolicaMax,' +
        'presionDiastolicaMin: $presionDiastolicaMin,' +
        'presionDiastolicaMed: $presionDiastolicaMed,' +
        'timeInstant: $timeInstant,' +
        'user: $user,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is PresionSanguinea &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


