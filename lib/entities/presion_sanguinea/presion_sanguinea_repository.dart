import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class PresionSanguineaRepository {
    PresionSanguineaRepository();
  
  static final String uriEndpoint = '/presion-sanguineas';

  Future<List<PresionSanguinea>> getAllPresionSanguineas() async {
    final allPresionSanguineasRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<PresionSanguinea>>(allPresionSanguineasRequest.body);
  }

  Future<PresionSanguinea> getPresionSanguinea(int id) async {
    final presionSanguineaRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<PresionSanguinea>(presionSanguineaRequest.body);
  }

  Future<PresionSanguinea> create(PresionSanguinea presionSanguinea) async {
    final presionSanguineaRequest = await HttpUtils.postRequest('$uriEndpoint', presionSanguinea);
    return JsonMapper.deserialize<PresionSanguinea>(presionSanguineaRequest.body);
  }

  Future<PresionSanguinea> update(PresionSanguinea presionSanguinea) async {
    final presionSanguineaRequest = await HttpUtils.putRequest('$uriEndpoint', presionSanguinea);
    return JsonMapper.deserialize<PresionSanguinea>(presionSanguineaRequest.body);
  }

  Future<void> delete(int id) async {
    final presionSanguineaRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
