import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/presion_sanguinea/bloc/presion_sanguinea_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';

class PresionSanguineaUpdateScreen extends StatelessWidget {
  PresionSanguineaUpdateScreen({Key key}) : super(key: MovilRcv2Keys.presionSanguineaCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<PresionSanguineaBloc, PresionSanguineaState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesPresionSanguineaList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit PresionSanguineas':
'Create PresionSanguineas';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          presionSistolicaMaxField(),
          presionSistolicaMinField(),
          presionSistolicaMedField(),
          presionDiastolicaMaxField(),
          presionDiastolicaMinField(),
          presionDiastolicaMedField(),
          timeInstantField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget presionSistolicaMaxField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionSistolicaMax != current.presionSistolicaMax,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionSistolicaMaxController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionSistolicaMaxChanged(presionSistolicaMax:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionSistolicaMax'));
            }
        );
      }
      Widget presionSistolicaMinField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionSistolicaMin != current.presionSistolicaMin,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionSistolicaMinController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionSistolicaMinChanged(presionSistolicaMin:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionSistolicaMin'));
            }
        );
      }
      Widget presionSistolicaMedField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionSistolicaMed != current.presionSistolicaMed,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionSistolicaMedController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionSistolicaMedChanged(presionSistolicaMed:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionSistolicaMed'));
            }
        );
      }
      Widget presionDiastolicaMaxField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionDiastolicaMax != current.presionDiastolicaMax,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionDiastolicaMaxController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionDiastolicaMaxChanged(presionDiastolicaMax:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionDiastolicaMax'));
            }
        );
      }
      Widget presionDiastolicaMinField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionDiastolicaMin != current.presionDiastolicaMin,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionDiastolicaMinController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionDiastolicaMinChanged(presionDiastolicaMin:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionDiastolicaMin'));
            }
        );
      }
      Widget presionDiastolicaMedField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.presionDiastolicaMed != current.presionDiastolicaMed,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PresionSanguineaBloc>().presionDiastolicaMedController,
                  onChanged: (value) { context.bloc<PresionSanguineaBloc>()
                    .add(PresionDiastolicaMedChanged(presionDiastolicaMed:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionDiastolicaMed'));
            }
        );
      }
      Widget timeInstantField() {
        return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
            buildWhen: (previous, current) => previous.timeInstant != current.timeInstant,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<PresionSanguineaBloc>().timeInstantController,
                onChanged: (value) { context.bloc<PresionSanguineaBloc>().add(TimeInstantChanged(timeInstant: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'timeInstant',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(PresionSanguineaState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<PresionSanguineaBloc>().add(PresionSanguineaFormSubmitted()) : null,
          );
        }
    );
  }
}
