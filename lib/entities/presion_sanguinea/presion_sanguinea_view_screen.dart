import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/presion_sanguinea/bloc/presion_sanguinea_bloc.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class PresionSanguineaViewScreen extends StatelessWidget {
  PresionSanguineaViewScreen({Key key}) : super(key: MovilRcv2Keys.presionSanguineaCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('PresionSanguineas View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<PresionSanguineaBloc, PresionSanguineaState>(
              buildWhen: (previous, current) => previous.loadedPresionSanguinea != current.loadedPresionSanguinea,
              builder: (context, state) {
                return Visibility(
                  visible: state.presionSanguineaStatusUI == PresionSanguineaStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    presionSanguineaCard(state.loadedPresionSanguinea, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesPresionSanguineaCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget presionSanguineaCard(PresionSanguinea presionSanguinea, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + presionSanguinea.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Sistolica Max : ' + presionSanguinea.presionSistolicaMax.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Sistolica Min : ' + presionSanguinea.presionSistolicaMin.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Sistolica Med : ' + presionSanguinea.presionSistolicaMed.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Diastolica Max : ' + presionSanguinea.presionDiastolicaMax.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Diastolica Min : ' + presionSanguinea.presionDiastolicaMin.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Diastolica Med : ' + presionSanguinea.presionDiastolicaMed.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Time Instant : ' + (presionSanguinea?.timeInstant != null ? DateFormat.yMMMMd('en').format(presionSanguinea.timeInstant) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
