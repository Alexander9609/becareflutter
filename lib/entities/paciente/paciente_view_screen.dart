import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/paciente/bloc/paciente_bloc.dart';
import 'package:movilRCV2/entities/paciente/paciente_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class PacienteViewScreen extends StatelessWidget {
  PacienteViewScreen({Key key}) : super(key: MovilRcv2Keys.pacienteCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Pacientes View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<PacienteBloc, PacienteState>(
              buildWhen: (previous, current) => previous.loadedPaciente != current.loadedPaciente,
              builder: (context, state) {
                return Visibility(
                  visible: state.pacienteStatusUI == PacienteStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    pacienteCard(state.loadedPaciente, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesPacienteCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget pacienteCard(Paciente paciente, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + paciente.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Nombre : ' + paciente.nombre.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Tipo Identifcacion : ' + paciente.tipoIdentifcacion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Identificacion : ' + paciente.identificacion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Edad : ' + paciente.edad.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Sexo : ' + paciente.sexo.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Peso : ' + paciente.peso.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Estatura : ' + paciente.estatura.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Indice Masa Corporal : ' + paciente.indiceMasaCorporal.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Raza : ' + paciente.raza.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Riesgo Vascular : ' + paciente.riesgoVascular.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Oximetria Referencia : ' + paciente.oximetriaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Temperatura Referencia : ' + paciente.temperaturaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Ritmo Cardiaco Referencia : ' + paciente.ritmoCardiacoReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Sistolica Referencia : ' + paciente.presionSistolicaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Diastolica Referencia : ' + paciente.presionDiastolicaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Latitud : ' + paciente.latitud.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Longitud : ' + paciente.longitud.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Comentarios : ' + paciente.comentarios.toString(), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
