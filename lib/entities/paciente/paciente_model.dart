import 'package:dart_json_mapper/dart_json_mapper.dart';

import '../iPS/iPS_model.dart';
import 'package:movilRCV2/shared/models/user.dart';
import '../medicacion/medicacion_model.dart';
import '../condicion/condicion_model.dart';
//import '../estadioPresionArterial/estadioPresionArterial_model.dart';

@jsonSerializable
class Paciente {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'nombre')
  final String nombre;

  @JsonProperty(name: 'tipoIdentifcacion')
  final String tipoIdentifcacion;

  @JsonProperty(name: 'identificacion')
  final String identificacion;

  @JsonProperty(name: 'edad')
  final String edad;

  @JsonProperty(name: 'sexo')
  final String sexo;

  @JsonProperty(name: 'peso')
  final String peso;

  @JsonProperty(name: 'estatura')
  final String estatura;

  @JsonProperty(name: 'indiceMasaCorporal')
  final String indiceMasaCorporal;

  @JsonProperty(name: 'raza')
  final String raza;

  @JsonProperty(name: 'riesgoVascular')
  final String riesgoVascular;

  @JsonProperty(name: 'oximetriaReferencia')
  final String oximetriaReferencia;

  @JsonProperty(name: 'temperaturaReferencia')
  final String temperaturaReferencia;

  @JsonProperty(name: 'ritmoCardiacoReferencia')
  final String ritmoCardiacoReferencia;

  @JsonProperty(name: 'presionSistolicaReferencia')
  final String presionSistolicaReferencia;

  @JsonProperty(name: 'presionDiastolicaReferencia')
  final String presionDiastolicaReferencia;

  @JsonProperty(name: 'latitud')
  final String latitud;

  @JsonProperty(name: 'longitud')
  final String longitud;

  @JsonProperty(name: 'comentarios')
  final String comentarios;

  @JsonProperty(name: 'ips')
  final String ips;

  @JsonProperty(name: 'user')
  final User user;

  @JsonProperty(name: 'medicacion')
  final Medicacion medicacion;

  @JsonProperty(name: 'condicion')
  final Condicion condicion;

  @JsonProperty(name: 'estadioPA')
  final String estadioPA;
        
 const Paciente (
     this.id,
        this.nombre,
        this.tipoIdentifcacion,
        this.identificacion,
        this.edad,
        this.sexo,
        this.peso,
        this.estatura,
        this.indiceMasaCorporal,
        this.raza,
        this.riesgoVascular,
        this.oximetriaReferencia,
        this.temperaturaReferencia,
        this.ritmoCardiacoReferencia,
        this.presionSistolicaReferencia,
        this.presionDiastolicaReferencia,
        this.latitud,
        this.longitud,
        this.comentarios,
        this.ips,
        this.user,
        this.medicacion,
        this.condicion,
        this.estadioPA,
    );

@override
String toString() {
    return 'Paciente{'+
    'id: $id,' +
        'nombre: $nombre,' +
        'tipoIdentifcacion: $tipoIdentifcacion,' +
        'identificacion: $identificacion,' +
        'edad: $edad,' +
        'sexo: $sexo,' +
        'peso: $peso,' +
        'estatura: $estatura,' +
        'indiceMasaCorporal: $indiceMasaCorporal,' +
        'raza: $raza,' +
        'riesgoVascular: $riesgoVascular,' +
        'oximetriaReferencia: $oximetriaReferencia,' +
        'temperaturaReferencia: $temperaturaReferencia,' +
        'ritmoCardiacoReferencia: $ritmoCardiacoReferencia,' +
        'presionSistolicaReferencia: $presionSistolicaReferencia,' +
        'presionDiastolicaReferencia: $presionDiastolicaReferencia,' +
        'latitud: $latitud,' +
        'longitud: $longitud,' +
        'comentarios: $comentarios,' +
        'ips: $ips,' +
        'user: $user,' +
        'medicacion: $medicacion,' +
        'condicion: $condicion,' +
        'estadioPA: $estadioPA,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Paciente &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


