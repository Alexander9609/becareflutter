import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/paciente/bloc/paciente_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/paciente/paciente_model.dart';

class PacienteUpdateScreen extends StatelessWidget {
  PacienteUpdateScreen({Key key}) : super(key: MovilRcv2Keys.pacienteCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<PacienteBloc, PacienteState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesPacienteList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<PacienteBloc, PacienteState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Pacientes':
'Create Pacientes';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          nombreField(),
          tipoIdentifcacionField(),
          identificacionField(),
          edadField(),
          sexoField(),
          pesoField(),
          estaturaField(),
          indiceMasaCorporalField(),
          razaField(),
          riesgoVascularField(),
          oximetriaReferenciaField(),
          temperaturaReferenciaField(),
          ritmoCardiacoReferenciaField(),
          presionSistolicaReferenciaField(),
          presionDiastolicaReferenciaField(),
          latitudField(),
          longitudField(),
          comentariosField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget nombreField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.nombre != current.nombre,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().nombreController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(NombreChanged(nombre:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'nombre'));
            }
        );
      }
      Widget tipoIdentifcacionField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.tipoIdentifcacion != current.tipoIdentifcacion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().tipoIdentifcacionController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(TipoIdentifcacionChanged(tipoIdentifcacion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'tipoIdentifcacion'));
            }
        );
      }
      Widget identificacionField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.identificacion != current.identificacion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().identificacionController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(IdentificacionChanged(identificacion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'identificacion'));
            }
        );
      }
      Widget edadField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.edad != current.edad,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().edadController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(EdadChanged(edad:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'edad'));
            }
        );
      }
      Widget sexoField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.sexo != current.sexo,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().sexoController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(SexoChanged(sexo:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'sexo'));
            }
        );
      }
      Widget pesoField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.peso != current.peso,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().pesoController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(PesoChanged(peso:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'peso'));
            }
        );
      }
      Widget estaturaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.estatura != current.estatura,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().estaturaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(EstaturaChanged(estatura:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'estatura'));
            }
        );
      }
      Widget indiceMasaCorporalField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.indiceMasaCorporal != current.indiceMasaCorporal,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().indiceMasaCorporalController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(IndiceMasaCorporalChanged(indiceMasaCorporal:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'indiceMasaCorporal'));
            }
        );
      }
      Widget razaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.raza != current.raza,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().razaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(RazaChanged(raza:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'raza'));
            }
        );
      }
      Widget riesgoVascularField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.riesgoVascular != current.riesgoVascular,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().riesgoVascularController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(RiesgoVascularChanged(riesgoVascular:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'riesgoVascular'));
            }
        );
      }
      Widget oximetriaReferenciaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.oximetriaReferencia != current.oximetriaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().oximetriaReferenciaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(OximetriaReferenciaChanged(oximetriaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'oximetriaReferencia'));
            }
        );
      }
      Widget temperaturaReferenciaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.temperaturaReferencia != current.temperaturaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().temperaturaReferenciaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(TemperaturaReferenciaChanged(temperaturaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'temperaturaReferencia'));
            }
        );
      }
      Widget ritmoCardiacoReferenciaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.ritmoCardiacoReferencia != current.ritmoCardiacoReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().ritmoCardiacoReferenciaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(RitmoCardiacoReferenciaChanged(ritmoCardiacoReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'ritmoCardiacoReferencia'));
            }
        );
      }
      Widget presionSistolicaReferenciaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.presionSistolicaReferencia != current.presionSistolicaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().presionSistolicaReferenciaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(PresionSistolicaReferenciaChanged(presionSistolicaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionSistolicaReferencia'));
            }
        );
      }
      Widget presionDiastolicaReferenciaField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.presionDiastolicaReferencia != current.presionDiastolicaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().presionDiastolicaReferenciaController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(PresionDiastolicaReferenciaChanged(presionDiastolicaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionDiastolicaReferencia'));
            }
        );
      }
      Widget latitudField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.latitud != current.latitud,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().latitudController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(LatitudChanged(latitud:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'latitud'));
            }
        );
      }
      Widget longitudField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.longitud != current.longitud,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().longitudController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(LongitudChanged(longitud:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'longitud'));
            }
        );
      }
      Widget comentariosField() {
        return BlocBuilder<PacienteBloc, PacienteState>(
            buildWhen: (previous, current) => previous.comentarios != current.comentarios,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<PacienteBloc>().comentariosController,
                  onChanged: (value) { context.bloc<PacienteBloc>()
                    .add(ComentariosChanged(comentarios:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'comentarios'));
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<PacienteBloc, PacienteState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(PacienteState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<PacienteBloc, PacienteState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<PacienteBloc>().add(PacienteFormSubmitted()) : null,
          );
        }
    );
  }
}
