import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/paciente/paciente_model.dart';

enum NombreValidationError { invalid }
class NombreInput extends FormzInput<String, NombreValidationError> {
  const NombreInput.pure() : super.pure('');
  const NombreInput.dirty([String value = '']) : super.dirty(value);

  @override
  NombreValidationError validator(String value) {
    return null;
  }
}

enum TipoIdentifcacionValidationError { invalid }
class TipoIdentifcacionInput extends FormzInput<String, TipoIdentifcacionValidationError> {
  const TipoIdentifcacionInput.pure() : super.pure('');
  const TipoIdentifcacionInput.dirty([String value = '']) : super.dirty(value);

  @override
  TipoIdentifcacionValidationError validator(String value) {
    return null;
  }
}

enum IdentificacionValidationError { invalid }
class IdentificacionInput extends FormzInput<String, IdentificacionValidationError> {
  const IdentificacionInput.pure() : super.pure('');
  const IdentificacionInput.dirty([String value = '']) : super.dirty(value);

  @override
  IdentificacionValidationError validator(String value) {
    return null;
  }
}

enum EdadValidationError { invalid }
class EdadInput extends FormzInput<String, EdadValidationError> {
  const EdadInput.pure() : super.pure('');
  const EdadInput.dirty([String value = '']) : super.dirty(value);

  @override
  EdadValidationError validator(String value) {
    return null;
  }
}

enum SexoValidationError { invalid }
class SexoInput extends FormzInput<String, SexoValidationError> {
  const SexoInput.pure() : super.pure('');
  const SexoInput.dirty([String value = '']) : super.dirty(value);

  @override
  SexoValidationError validator(String value) {
    return null;
  }
}

enum PesoValidationError { invalid }
class PesoInput extends FormzInput<String, PesoValidationError> {
  const PesoInput.pure() : super.pure('');
  const PesoInput.dirty([String value = '']) : super.dirty(value);

  @override
  PesoValidationError validator(String value) {
    return null;
  }
}

enum EstaturaValidationError { invalid }
class EstaturaInput extends FormzInput<String, EstaturaValidationError> {
  const EstaturaInput.pure() : super.pure('');
  const EstaturaInput.dirty([String value = '']) : super.dirty(value);

  @override
  EstaturaValidationError validator(String value) {
    return null;
  }
}

enum IndiceMasaCorporalValidationError { invalid }
class IndiceMasaCorporalInput extends FormzInput<String, IndiceMasaCorporalValidationError> {
  const IndiceMasaCorporalInput.pure() : super.pure('');
  const IndiceMasaCorporalInput.dirty([String value = '']) : super.dirty(value);

  @override
  IndiceMasaCorporalValidationError validator(String value) {
    return null;
  }
}

enum RazaValidationError { invalid }
class RazaInput extends FormzInput<String, RazaValidationError> {
  const RazaInput.pure() : super.pure('');
  const RazaInput.dirty([String value = '']) : super.dirty(value);

  @override
  RazaValidationError validator(String value) {
    return null;
  }
}

enum RiesgoVascularValidationError { invalid }
class RiesgoVascularInput extends FormzInput<String, RiesgoVascularValidationError> {
  const RiesgoVascularInput.pure() : super.pure('');
  const RiesgoVascularInput.dirty([String value = '']) : super.dirty(value);

  @override
  RiesgoVascularValidationError validator(String value) {
    return null;
  }
}

enum OximetriaReferenciaValidationError { invalid }
class OximetriaReferenciaInput extends FormzInput<String, OximetriaReferenciaValidationError> {
  const OximetriaReferenciaInput.pure() : super.pure('');
  const OximetriaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  OximetriaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum TemperaturaReferenciaValidationError { invalid }
class TemperaturaReferenciaInput extends FormzInput<String, TemperaturaReferenciaValidationError> {
  const TemperaturaReferenciaInput.pure() : super.pure('');
  const TemperaturaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  TemperaturaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum RitmoCardiacoReferenciaValidationError { invalid }
class RitmoCardiacoReferenciaInput extends FormzInput<String, RitmoCardiacoReferenciaValidationError> {
  const RitmoCardiacoReferenciaInput.pure() : super.pure('');
  const RitmoCardiacoReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  RitmoCardiacoReferenciaValidationError validator(String value) {
    return null;
  }
}

enum PresionSistolicaReferenciaValidationError { invalid }
class PresionSistolicaReferenciaInput extends FormzInput<String, PresionSistolicaReferenciaValidationError> {
  const PresionSistolicaReferenciaInput.pure() : super.pure('');
  const PresionSistolicaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionSistolicaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum PresionDiastolicaReferenciaValidationError { invalid }
class PresionDiastolicaReferenciaInput extends FormzInput<String, PresionDiastolicaReferenciaValidationError> {
  const PresionDiastolicaReferenciaInput.pure() : super.pure('');
  const PresionDiastolicaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionDiastolicaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum LatitudValidationError { invalid }
class LatitudInput extends FormzInput<String, LatitudValidationError> {
  const LatitudInput.pure() : super.pure('');
  const LatitudInput.dirty([String value = '']) : super.dirty(value);

  @override
  LatitudValidationError validator(String value) {
    return null;
  }
}

enum LongitudValidationError { invalid }
class LongitudInput extends FormzInput<String, LongitudValidationError> {
  const LongitudInput.pure() : super.pure('');
  const LongitudInput.dirty([String value = '']) : super.dirty(value);

  @override
  LongitudValidationError validator(String value) {
    return null;
  }
}

enum ComentariosValidationError { invalid }
class ComentariosInput extends FormzInput<String, ComentariosValidationError> {
  const ComentariosInput.pure() : super.pure('');
  const ComentariosInput.dirty([String value = '']) : super.dirty(value);

  @override
  ComentariosValidationError validator(String value) {
    return null;
  }
}

