import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/paciente/paciente_model.dart';
import 'package:movilRCV2/entities/paciente/paciente_repository.dart';
import 'package:movilRCV2/entities/paciente/bloc/paciente_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'paciente_events.dart';
part 'paciente_state.dart';

class PacienteBloc extends Bloc<PacienteEvent, PacienteState> {
  final PacienteRepository _pacienteRepository;

  final nombreController = TextEditingController();
  final tipoIdentifcacionController = TextEditingController();
  final identificacionController = TextEditingController();
  final edadController = TextEditingController();
  final sexoController = TextEditingController();
  final pesoController = TextEditingController();
  final estaturaController = TextEditingController();
  final indiceMasaCorporalController = TextEditingController();
  final razaController = TextEditingController();
  final riesgoVascularController = TextEditingController();
  final oximetriaReferenciaController = TextEditingController();
  final temperaturaReferenciaController = TextEditingController();
  final ritmoCardiacoReferenciaController = TextEditingController();
  final presionSistolicaReferenciaController = TextEditingController();
  final presionDiastolicaReferenciaController = TextEditingController();
  final latitudController = TextEditingController();
  final longitudController = TextEditingController();
  final comentariosController = TextEditingController();

  PacienteBloc({@required PacienteRepository pacienteRepository}) : assert(pacienteRepository != null),
        _pacienteRepository = pacienteRepository, 
  super(PacienteState());

  @override
  void onTransition(Transition<PacienteEvent, PacienteState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<PacienteState> mapEventToState(PacienteEvent event) async* {
    if (event is InitPacienteList) {
      yield* onInitList(event);
    } else if (event is PacienteFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadPacienteByIdForEdit) {
      yield* onLoadPacienteIdForEdit(event);
    } else if (event is DeletePacienteById) {
      yield* onDeletePacienteId(event);
    } else if (event is LoadPacienteByIdForView) {
      yield* onLoadPacienteIdForView(event);
    }else if (event is NombreChanged){
      yield* onNombreChange(event);
    }else if (event is TipoIdentifcacionChanged){
      yield* onTipoIdentifcacionChange(event);
    }else if (event is IdentificacionChanged){
      yield* onIdentificacionChange(event);
    }else if (event is EdadChanged){
      yield* onEdadChange(event);
    }else if (event is SexoChanged){
      yield* onSexoChange(event);
    }else if (event is PesoChanged){
      yield* onPesoChange(event);
    }else if (event is EstaturaChanged){
      yield* onEstaturaChange(event);
    }else if (event is IndiceMasaCorporalChanged){
      yield* onIndiceMasaCorporalChange(event);
    }else if (event is RazaChanged){
      yield* onRazaChange(event);
    }else if (event is RiesgoVascularChanged){
      yield* onRiesgoVascularChange(event);
    }else if (event is OximetriaReferenciaChanged){
      yield* onOximetriaReferenciaChange(event);
    }else if (event is TemperaturaReferenciaChanged){
      yield* onTemperaturaReferenciaChange(event);
    }else if (event is RitmoCardiacoReferenciaChanged){
      yield* onRitmoCardiacoReferenciaChange(event);
    }else if (event is PresionSistolicaReferenciaChanged){
      yield* onPresionSistolicaReferenciaChange(event);
    }else if (event is PresionDiastolicaReferenciaChanged){
      yield* onPresionDiastolicaReferenciaChange(event);
    }else if (event is LatitudChanged){
      yield* onLatitudChange(event);
    }else if (event is LongitudChanged){
      yield* onLongitudChange(event);
    }else if (event is ComentariosChanged){
      yield* onComentariosChange(event);
    }  }

  Stream<PacienteState> onInitList(InitPacienteList event) async* {
    yield this.state.copyWith(pacienteStatusUI: PacienteStatusUI.loading);
    List<Paciente> pacientes = await _pacienteRepository.getAllPacientes();
    yield this.state.copyWith(pacientes: pacientes, pacienteStatusUI: PacienteStatusUI.done);
  }

  Stream<PacienteState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Paciente result;
        if(this.state.editMode) {
          Paciente newPaciente = Paciente(state.loadedPaciente.id,
            this.state.nombre.value,  
            this.state.tipoIdentifcacion.value,  
            this.state.identificacion.value,  
            this.state.edad.value,  
            this.state.sexo.value,  
            this.state.peso.value,  
            this.state.estatura.value,  
            this.state.indiceMasaCorporal.value,  
            this.state.raza.value,  
            this.state.riesgoVascular.value,  
            this.state.oximetriaReferencia.value,  
            this.state.temperaturaReferencia.value,  
            this.state.ritmoCardiacoReferencia.value,  
            this.state.presionSistolicaReferencia.value,  
            this.state.presionDiastolicaReferencia.value,  
            this.state.latitud.value,  
            this.state.longitud.value,  
            this.state.comentarios.value,  
            null, 
            null, 
            null, 
            null, 
            null, 
          );

          result = await _pacienteRepository.update(newPaciente);
        } else {
          Paciente newPaciente = Paciente(null,
            this.state.nombre.value,  
            this.state.tipoIdentifcacion.value,  
            this.state.identificacion.value,  
            this.state.edad.value,  
            this.state.sexo.value,  
            this.state.peso.value,  
            this.state.estatura.value,  
            this.state.indiceMasaCorporal.value,  
            this.state.raza.value,  
            this.state.riesgoVascular.value,  
            this.state.oximetriaReferencia.value,  
            this.state.temperaturaReferencia.value,  
            this.state.ritmoCardiacoReferencia.value,  
            this.state.presionSistolicaReferencia.value,  
            this.state.presionDiastolicaReferencia.value,  
            this.state.latitud.value,  
            this.state.longitud.value,  
            this.state.comentarios.value,  
            null, 
            null, 
            null, 
            null, 
            null, 
          );

          result = await _pacienteRepository.create(newPaciente);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<PacienteState> onLoadPacienteIdForEdit(LoadPacienteByIdForEdit event) async* {
    yield this.state.copyWith(pacienteStatusUI: PacienteStatusUI.loading);
    Paciente loadedPaciente = await _pacienteRepository.getPaciente(event.id);

    final nombre = NombreInput.dirty(loadedPaciente?.nombre != null ? loadedPaciente.nombre: '');
    final tipoIdentifcacion = TipoIdentifcacionInput.dirty(loadedPaciente?.tipoIdentifcacion != null ? loadedPaciente.tipoIdentifcacion: '');
    final identificacion = IdentificacionInput.dirty(loadedPaciente?.identificacion != null ? loadedPaciente.identificacion: '');
    final edad = EdadInput.dirty(loadedPaciente?.edad != null ? loadedPaciente.edad: '');
    final sexo = SexoInput.dirty(loadedPaciente?.sexo != null ? loadedPaciente.sexo: '');
    final peso = PesoInput.dirty(loadedPaciente?.peso != null ? loadedPaciente.peso: '');
    final estatura = EstaturaInput.dirty(loadedPaciente?.estatura != null ? loadedPaciente.estatura: '');
    final indiceMasaCorporal = IndiceMasaCorporalInput.dirty(loadedPaciente?.indiceMasaCorporal != null ? loadedPaciente.indiceMasaCorporal: '');
    final raza = RazaInput.dirty(loadedPaciente?.raza != null ? loadedPaciente.raza: '');
    final riesgoVascular = RiesgoVascularInput.dirty(loadedPaciente?.riesgoVascular != null ? loadedPaciente.riesgoVascular: '');
    final oximetriaReferencia = OximetriaReferenciaInput.dirty(loadedPaciente?.oximetriaReferencia != null ? loadedPaciente.oximetriaReferencia: '');
    final temperaturaReferencia = TemperaturaReferenciaInput.dirty(loadedPaciente?.temperaturaReferencia != null ? loadedPaciente.temperaturaReferencia: '');
    final ritmoCardiacoReferencia = RitmoCardiacoReferenciaInput.dirty(loadedPaciente?.ritmoCardiacoReferencia != null ? loadedPaciente.ritmoCardiacoReferencia: '');
    final presionSistolicaReferencia = PresionSistolicaReferenciaInput.dirty(loadedPaciente?.presionSistolicaReferencia != null ? loadedPaciente.presionSistolicaReferencia: '');
    final presionDiastolicaReferencia = PresionDiastolicaReferenciaInput.dirty(loadedPaciente?.presionDiastolicaReferencia != null ? loadedPaciente.presionDiastolicaReferencia: '');
    final latitud = LatitudInput.dirty(loadedPaciente?.latitud != null ? loadedPaciente.latitud: '');
    final longitud = LongitudInput.dirty(loadedPaciente?.longitud != null ? loadedPaciente.longitud: '');
    final comentarios = ComentariosInput.dirty(loadedPaciente?.comentarios != null ? loadedPaciente.comentarios: '');

    yield this.state.copyWith(loadedPaciente: loadedPaciente, editMode: true,
      nombre: nombre,
      tipoIdentifcacion: tipoIdentifcacion,
      identificacion: identificacion,
      edad: edad,
      sexo: sexo,
      peso: peso,
      estatura: estatura,
      indiceMasaCorporal: indiceMasaCorporal,
      raza: raza,
      riesgoVascular: riesgoVascular,
      oximetriaReferencia: oximetriaReferencia,
      temperaturaReferencia: temperaturaReferencia,
      ritmoCardiacoReferencia: ritmoCardiacoReferencia,
      presionSistolicaReferencia: presionSistolicaReferencia,
      presionDiastolicaReferencia: presionDiastolicaReferencia,
      latitud: latitud,
      longitud: longitud,
      comentarios: comentarios,
    pacienteStatusUI: PacienteStatusUI.done);

    nombreController.text = loadedPaciente.nombre;
    tipoIdentifcacionController.text = loadedPaciente.tipoIdentifcacion;
    identificacionController.text = loadedPaciente.identificacion;
    edadController.text = loadedPaciente.edad;
    sexoController.text = loadedPaciente.sexo;
    pesoController.text = loadedPaciente.peso;
    estaturaController.text = loadedPaciente.estatura;
    indiceMasaCorporalController.text = loadedPaciente.indiceMasaCorporal;
    razaController.text = loadedPaciente.raza;
    riesgoVascularController.text = loadedPaciente.riesgoVascular;
    oximetriaReferenciaController.text = loadedPaciente.oximetriaReferencia;
    temperaturaReferenciaController.text = loadedPaciente.temperaturaReferencia;
    ritmoCardiacoReferenciaController.text = loadedPaciente.ritmoCardiacoReferencia;
    presionSistolicaReferenciaController.text = loadedPaciente.presionSistolicaReferencia;
    presionDiastolicaReferenciaController.text = loadedPaciente.presionDiastolicaReferencia;
    latitudController.text = loadedPaciente.latitud;
    longitudController.text = loadedPaciente.longitud;
    comentariosController.text = loadedPaciente.comentarios;
  }

  Stream<PacienteState> onDeletePacienteId(DeletePacienteById event) async* {
    try {
      await _pacienteRepository.delete(event.id);
      this.add(InitPacienteList());
      yield this.state.copyWith(deleteStatus: PacienteDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: PacienteDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: PacienteDeleteStatus.none);
  }

  Stream<PacienteState> onLoadPacienteIdForView(LoadPacienteByIdForView event) async* {
    yield this.state.copyWith(pacienteStatusUI: PacienteStatusUI.loading);
    try {
      Paciente loadedPaciente = await _pacienteRepository.getPaciente(event.id);
      yield this.state.copyWith(loadedPaciente: loadedPaciente, pacienteStatusUI: PacienteStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedPaciente: null, pacienteStatusUI: PacienteStatusUI.error);
    }
  }


  Stream<PacienteState> onNombreChange(NombreChanged event) async* {
    final nombre = NombreInput.dirty(event.nombre);
    yield this.state.copyWith(
      nombre: nombre,
      formStatus: Formz.validate([
        nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onTipoIdentifcacionChange(TipoIdentifcacionChanged event) async* {
    final tipoIdentifcacion = TipoIdentifcacionInput.dirty(event.tipoIdentifcacion);
    yield this.state.copyWith(
      tipoIdentifcacion: tipoIdentifcacion,
      formStatus: Formz.validate([
      this.state.nombre,
        tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onIdentificacionChange(IdentificacionChanged event) async* {
    final identificacion = IdentificacionInput.dirty(event.identificacion);
    yield this.state.copyWith(
      identificacion: identificacion,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
        identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onEdadChange(EdadChanged event) async* {
    final edad = EdadInput.dirty(event.edad);
    yield this.state.copyWith(
      edad: edad,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
        edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onSexoChange(SexoChanged event) async* {
    final sexo = SexoInput.dirty(event.sexo);
    yield this.state.copyWith(
      sexo: sexo,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
        sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onPesoChange(PesoChanged event) async* {
    final peso = PesoInput.dirty(event.peso);
    yield this.state.copyWith(
      peso: peso,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
        peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onEstaturaChange(EstaturaChanged event) async* {
    final estatura = EstaturaInput.dirty(event.estatura);
    yield this.state.copyWith(
      estatura: estatura,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
        estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onIndiceMasaCorporalChange(IndiceMasaCorporalChanged event) async* {
    final indiceMasaCorporal = IndiceMasaCorporalInput.dirty(event.indiceMasaCorporal);
    yield this.state.copyWith(
      indiceMasaCorporal: indiceMasaCorporal,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
        indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onRazaChange(RazaChanged event) async* {
    final raza = RazaInput.dirty(event.raza);
    yield this.state.copyWith(
      raza: raza,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
        raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onRiesgoVascularChange(RiesgoVascularChanged event) async* {
    final riesgoVascular = RiesgoVascularInput.dirty(event.riesgoVascular);
    yield this.state.copyWith(
      riesgoVascular: riesgoVascular,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
        riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onOximetriaReferenciaChange(OximetriaReferenciaChanged event) async* {
    final oximetriaReferencia = OximetriaReferenciaInput.dirty(event.oximetriaReferencia);
    yield this.state.copyWith(
      oximetriaReferencia: oximetriaReferencia,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
        oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onTemperaturaReferenciaChange(TemperaturaReferenciaChanged event) async* {
    final temperaturaReferencia = TemperaturaReferenciaInput.dirty(event.temperaturaReferencia);
    yield this.state.copyWith(
      temperaturaReferencia: temperaturaReferencia,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
        temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onRitmoCardiacoReferenciaChange(RitmoCardiacoReferenciaChanged event) async* {
    final ritmoCardiacoReferencia = RitmoCardiacoReferenciaInput.dirty(event.ritmoCardiacoReferencia);
    yield this.state.copyWith(
      ritmoCardiacoReferencia: ritmoCardiacoReferencia,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
        ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onPresionSistolicaReferenciaChange(PresionSistolicaReferenciaChanged event) async* {
    final presionSistolicaReferencia = PresionSistolicaReferenciaInput.dirty(event.presionSistolicaReferencia);
    yield this.state.copyWith(
      presionSistolicaReferencia: presionSistolicaReferencia,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
        presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onPresionDiastolicaReferenciaChange(PresionDiastolicaReferenciaChanged event) async* {
    final presionDiastolicaReferencia = PresionDiastolicaReferenciaInput.dirty(event.presionDiastolicaReferencia);
    yield this.state.copyWith(
      presionDiastolicaReferencia: presionDiastolicaReferencia,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
        presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onLatitudChange(LatitudChanged event) async* {
    final latitud = LatitudInput.dirty(event.latitud);
    yield this.state.copyWith(
      latitud: latitud,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
        latitud,
      this.state.longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onLongitudChange(LongitudChanged event) async* {
    final longitud = LongitudInput.dirty(event.longitud);
    yield this.state.copyWith(
      longitud: longitud,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
        longitud,
      this.state.comentarios,
      ]),
    );
  }
  Stream<PacienteState> onComentariosChange(ComentariosChanged event) async* {
    final comentarios = ComentariosInput.dirty(event.comentarios);
    yield this.state.copyWith(
      comentarios: comentarios,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.tipoIdentifcacion,
      this.state.identificacion,
      this.state.edad,
      this.state.sexo,
      this.state.peso,
      this.state.estatura,
      this.state.indiceMasaCorporal,
      this.state.raza,
      this.state.riesgoVascular,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.latitud,
      this.state.longitud,
        comentarios,
      ]),
    );
  }

  @override
  Future<void> close() {
    nombreController.dispose();
    tipoIdentifcacionController.dispose();
    identificacionController.dispose();
    edadController.dispose();
    sexoController.dispose();
    pesoController.dispose();
    estaturaController.dispose();
    indiceMasaCorporalController.dispose();
    razaController.dispose();
    riesgoVascularController.dispose();
    oximetriaReferenciaController.dispose();
    temperaturaReferenciaController.dispose();
    ritmoCardiacoReferenciaController.dispose();
    presionSistolicaReferenciaController.dispose();
    presionDiastolicaReferenciaController.dispose();
    latitudController.dispose();
    longitudController.dispose();
    comentariosController.dispose();
    return super.close();
  }

}