part of 'paciente_bloc.dart';

enum PacienteStatusUI {init, loading, error, done}
enum PacienteDeleteStatus {ok, ko, none}

class PacienteState extends Equatable {
  final List<Paciente> pacientes;
  final Paciente loadedPaciente;
  final bool editMode;
  final PacienteDeleteStatus deleteStatus;
  final PacienteStatusUI pacienteStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final NombreInput nombre;
  final TipoIdentifcacionInput tipoIdentifcacion;
  final IdentificacionInput identificacion;
  final EdadInput edad;
  final SexoInput sexo;
  final PesoInput peso;
  final EstaturaInput estatura;
  final IndiceMasaCorporalInput indiceMasaCorporal;
  final RazaInput raza;
  final RiesgoVascularInput riesgoVascular;
  final OximetriaReferenciaInput oximetriaReferencia;
  final TemperaturaReferenciaInput temperaturaReferencia;
  final RitmoCardiacoReferenciaInput ritmoCardiacoReferencia;
  final PresionSistolicaReferenciaInput presionSistolicaReferencia;
  final PresionDiastolicaReferenciaInput presionDiastolicaReferencia;
  final LatitudInput latitud;
  final LongitudInput longitud;
  final ComentariosInput comentarios;

  
  PacienteState(
{
    this.pacientes = const [],
    this.pacienteStatusUI = PacienteStatusUI.init,
    this.loadedPaciente = const Paciente(0,'','','','','','','','','','','','','','','','','','',null,null,null,null,null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = PacienteDeleteStatus.none,
    this.nombre = const NombreInput.pure(),
    this.tipoIdentifcacion = const TipoIdentifcacionInput.pure(),
    this.identificacion = const IdentificacionInput.pure(),
    this.edad = const EdadInput.pure(),
    this.sexo = const SexoInput.pure(),
    this.peso = const PesoInput.pure(),
    this.estatura = const EstaturaInput.pure(),
    this.indiceMasaCorporal = const IndiceMasaCorporalInput.pure(),
    this.raza = const RazaInput.pure(),
    this.riesgoVascular = const RiesgoVascularInput.pure(),
    this.oximetriaReferencia = const OximetriaReferenciaInput.pure(),
    this.temperaturaReferencia = const TemperaturaReferenciaInput.pure(),
    this.ritmoCardiacoReferencia = const RitmoCardiacoReferenciaInput.pure(),
    this.presionSistolicaReferencia = const PresionSistolicaReferenciaInput.pure(),
    this.presionDiastolicaReferencia = const PresionDiastolicaReferenciaInput.pure(),
    this.latitud = const LatitudInput.pure(),
    this.longitud = const LongitudInput.pure(),
    this.comentarios = const ComentariosInput.pure(),
  });

  PacienteState copyWith({
    List<Paciente> pacientes,
    PacienteStatusUI pacienteStatusUI,
    bool editMode,
    PacienteDeleteStatus deleteStatus,
    Paciente loadedPaciente,
    FormzStatus formStatus,
    String generalNotificationKey,
    NombreInput nombre,
    TipoIdentifcacionInput tipoIdentifcacion,
    IdentificacionInput identificacion,
    EdadInput edad,
    SexoInput sexo,
    PesoInput peso,
    EstaturaInput estatura,
    IndiceMasaCorporalInput indiceMasaCorporal,
    RazaInput raza,
    RiesgoVascularInput riesgoVascular,
    OximetriaReferenciaInput oximetriaReferencia,
    TemperaturaReferenciaInput temperaturaReferencia,
    RitmoCardiacoReferenciaInput ritmoCardiacoReferencia,
    PresionSistolicaReferenciaInput presionSistolicaReferencia,
    PresionDiastolicaReferenciaInput presionDiastolicaReferencia,
    LatitudInput latitud,
    LongitudInput longitud,
    ComentariosInput comentarios,
  }) {
    return PacienteState(
      pacientes: pacientes ?? this.pacientes,
      pacienteStatusUI: pacienteStatusUI ?? this.pacienteStatusUI,
      loadedPaciente: loadedPaciente ?? this.loadedPaciente,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      nombre: nombre ?? this.nombre,
      tipoIdentifcacion: tipoIdentifcacion ?? this.tipoIdentifcacion,
      identificacion: identificacion ?? this.identificacion,
      edad: edad ?? this.edad,
      sexo: sexo ?? this.sexo,
      peso: peso ?? this.peso,
      estatura: estatura ?? this.estatura,
      indiceMasaCorporal: indiceMasaCorporal ?? this.indiceMasaCorporal,
      raza: raza ?? this.raza,
      riesgoVascular: riesgoVascular ?? this.riesgoVascular,
      oximetriaReferencia: oximetriaReferencia ?? this.oximetriaReferencia,
      temperaturaReferencia: temperaturaReferencia ?? this.temperaturaReferencia,
      ritmoCardiacoReferencia: ritmoCardiacoReferencia ?? this.ritmoCardiacoReferencia,
      presionSistolicaReferencia: presionSistolicaReferencia ?? this.presionSistolicaReferencia,
      presionDiastolicaReferencia: presionDiastolicaReferencia ?? this.presionDiastolicaReferencia,
      latitud: latitud ?? this.latitud,
      longitud: longitud ?? this.longitud,
      comentarios: comentarios ?? this.comentarios,
    );
  }

  @override
  List<Object> get props => [pacientes, pacienteStatusUI,
     loadedPaciente, editMode, deleteStatus, formStatus, generalNotificationKey, 
nombre,tipoIdentifcacion,identificacion,edad,sexo,peso,estatura,indiceMasaCorporal,raza,riesgoVascular,oximetriaReferencia,temperaturaReferencia,ritmoCardiacoReferencia,presionSistolicaReferencia,presionDiastolicaReferencia,latitud,longitud,comentarios,];

  @override
  bool get stringify => true;
}
