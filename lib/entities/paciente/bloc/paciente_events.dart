part of 'paciente_bloc.dart';

abstract class PacienteEvent extends Equatable {
  const PacienteEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitPacienteList extends PacienteEvent {}

class NombreChanged extends PacienteEvent {
  final String nombre;
  
  const NombreChanged({@required this.nombre});
  
  @override
  List<Object> get props => [nombre];
}
class TipoIdentifcacionChanged extends PacienteEvent {
  final String tipoIdentifcacion;
  
  const TipoIdentifcacionChanged({@required this.tipoIdentifcacion});
  
  @override
  List<Object> get props => [tipoIdentifcacion];
}
class IdentificacionChanged extends PacienteEvent {
  final String identificacion;
  
  const IdentificacionChanged({@required this.identificacion});
  
  @override
  List<Object> get props => [identificacion];
}
class EdadChanged extends PacienteEvent {
  final String edad;
  
  const EdadChanged({@required this.edad});
  
  @override
  List<Object> get props => [edad];
}
class SexoChanged extends PacienteEvent {
  final String sexo;
  
  const SexoChanged({@required this.sexo});
  
  @override
  List<Object> get props => [sexo];
}
class PesoChanged extends PacienteEvent {
  final String peso;
  
  const PesoChanged({@required this.peso});
  
  @override
  List<Object> get props => [peso];
}
class EstaturaChanged extends PacienteEvent {
  final String estatura;
  
  const EstaturaChanged({@required this.estatura});
  
  @override
  List<Object> get props => [estatura];
}
class IndiceMasaCorporalChanged extends PacienteEvent {
  final String indiceMasaCorporal;
  
  const IndiceMasaCorporalChanged({@required this.indiceMasaCorporal});
  
  @override
  List<Object> get props => [indiceMasaCorporal];
}
class RazaChanged extends PacienteEvent {
  final String raza;
  
  const RazaChanged({@required this.raza});
  
  @override
  List<Object> get props => [raza];
}
class RiesgoVascularChanged extends PacienteEvent {
  final String riesgoVascular;
  
  const RiesgoVascularChanged({@required this.riesgoVascular});
  
  @override
  List<Object> get props => [riesgoVascular];
}
class OximetriaReferenciaChanged extends PacienteEvent {
  final String oximetriaReferencia;
  
  const OximetriaReferenciaChanged({@required this.oximetriaReferencia});
  
  @override
  List<Object> get props => [oximetriaReferencia];
}
class TemperaturaReferenciaChanged extends PacienteEvent {
  final String temperaturaReferencia;
  
  const TemperaturaReferenciaChanged({@required this.temperaturaReferencia});
  
  @override
  List<Object> get props => [temperaturaReferencia];
}
class RitmoCardiacoReferenciaChanged extends PacienteEvent {
  final String ritmoCardiacoReferencia;
  
  const RitmoCardiacoReferenciaChanged({@required this.ritmoCardiacoReferencia});
  
  @override
  List<Object> get props => [ritmoCardiacoReferencia];
}
class PresionSistolicaReferenciaChanged extends PacienteEvent {
  final String presionSistolicaReferencia;
  
  const PresionSistolicaReferenciaChanged({@required this.presionSistolicaReferencia});
  
  @override
  List<Object> get props => [presionSistolicaReferencia];
}
class PresionDiastolicaReferenciaChanged extends PacienteEvent {
  final String presionDiastolicaReferencia;
  
  const PresionDiastolicaReferenciaChanged({@required this.presionDiastolicaReferencia});
  
  @override
  List<Object> get props => [presionDiastolicaReferencia];
}
class LatitudChanged extends PacienteEvent {
  final String latitud;
  
  const LatitudChanged({@required this.latitud});
  
  @override
  List<Object> get props => [latitud];
}
class LongitudChanged extends PacienteEvent {
  final String longitud;
  
  const LongitudChanged({@required this.longitud});
  
  @override
  List<Object> get props => [longitud];
}
class ComentariosChanged extends PacienteEvent {
  final String comentarios;
  
  const ComentariosChanged({@required this.comentarios});
  
  @override
  List<Object> get props => [comentarios];
}

class PacienteFormSubmitted extends PacienteEvent {}

class LoadPacienteByIdForEdit extends PacienteEvent {
  final int id;

  const LoadPacienteByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeletePacienteById extends PacienteEvent {
  final int id;

  const DeletePacienteById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadPacienteByIdForView extends PacienteEvent {
  final int id;

  const LoadPacienteByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
