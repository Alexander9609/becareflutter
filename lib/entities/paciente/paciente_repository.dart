import 'package:movilRCV2/entities/paciente/paciente_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class PacienteRepository {
    PacienteRepository();
  
  static final String uriEndpoint = '/pacientes';

  Future<List<Paciente>> getAllPacientes() async {
    final allPacientesRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Paciente>>(allPacientesRequest.body);
  }

  Future<Paciente> getPaciente(int id) async {
    final pacienteRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Paciente>(pacienteRequest.body);
  }

  Future<Paciente> create(Paciente paciente) async {
    final pacienteRequest = await HttpUtils.postRequest('$uriEndpoint', paciente);
    return JsonMapper.deserialize<Paciente>(pacienteRequest.body);
  }

  Future<Paciente> update(Paciente paciente) async {
    final pacienteRequest = await HttpUtils.putRequest('$uriEndpoint', paciente);
    return JsonMapper.deserialize<Paciente>(pacienteRequest.body);
  }

  Future<void> delete(int id) async {
    final pacienteRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
