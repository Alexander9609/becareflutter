import 'package:movilRCV2/entities/ips/ips_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class IpsRepository {
    IpsRepository();
  
  static final String uriEndpoint = '/ips';

  Future<List<Ips>> getAllIps() async {
    final allIpsRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Ips>>(allIpsRequest.body);
  }

  Future<Ips> getIps(int id) async {
    final iPSRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Ips>(iPSRequest.body);
  }

  Future<Ips> create(Ips iPS) async {
    final iPSRequest = await HttpUtils.postRequest('$uriEndpoint', iPS);
    return JsonMapper.deserialize<Ips>(iPSRequest.body);
  }

  Future<Ips> update(Ips iPS) async {
    final iPSRequest = await HttpUtils.putRequest('$uriEndpoint', iPS);
    return JsonMapper.deserialize<Ips>(iPSRequest.body);
  }

  Future<void> delete(int id) async {
    final iPSRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
