part of 'ips_bloc.dart';

enum IpsStatusUI {init, loading, error, done}
enum IpsDeleteStatus {ok, ko, none}

class IpsState extends Equatable {
  final List<Ips> ips;
  final Ips loadedIps;
  final bool editMode;
  final IpsDeleteStatus deleteStatus;
  final IpsStatusUI iPSStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final NombreInput nombre;
  final NitInput nit;
  final DireccionInput direccion;
  final TelefonoInput telefono;
  final CorreoElectronicoInput correoElectronico;

  
  IpsState(
{
    this.ips = const [],
    this.iPSStatusUI = IpsStatusUI.init,
    this.loadedIps = const Ips(0,'','','','','',),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = IpsDeleteStatus.none,
    this.nombre = const NombreInput.pure(),
    this.nit = const NitInput.pure(),
    this.direccion = const DireccionInput.pure(),
    this.telefono = const TelefonoInput.pure(),
    this.correoElectronico = const CorreoElectronicoInput.pure(),
  });

  IpsState copyWith({
    List<Ips> ips,
    IpsStatusUI iPSStatusUI,
    bool editMode,
    IpsDeleteStatus deleteStatus,
    Ips loadedIps,
    FormzStatus formStatus,
    String generalNotificationKey,
    NombreInput nombre,
    NitInput nit,
    DireccionInput direccion,
    TelefonoInput telefono,
    CorreoElectronicoInput correoElectronico,
  }) {
    return IpsState(
      ips: ips ?? this.ips,
      iPSStatusUI: iPSStatusUI ?? this.iPSStatusUI,
      loadedIps: loadedIps ?? this.loadedIps,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      nombre: nombre ?? this.nombre,
      nit: nit ?? this.nit,
      direccion: direccion ?? this.direccion,
      telefono: telefono ?? this.telefono,
      correoElectronico: correoElectronico ?? this.correoElectronico,
    );
  }

  @override
  List<Object> get props => [ips, iPSStatusUI,
     loadedIps, editMode, deleteStatus, formStatus, generalNotificationKey, 
nombre,nit,direccion,telefono,correoElectronico,];

  @override
  bool get stringify => true;
}
