import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/ips/ips_model.dart';

enum NombreValidationError { invalid }
class NombreInput extends FormzInput<String, NombreValidationError> {
  const NombreInput.pure() : super.pure('');
  const NombreInput.dirty([String value = '']) : super.dirty(value);

  @override
  NombreValidationError validator(String value) {
    return null;
  }
}

enum NitValidationError { invalid }
class NitInput extends FormzInput<String, NitValidationError> {
  const NitInput.pure() : super.pure('');
  const NitInput.dirty([String value = '']) : super.dirty(value);

  @override
  NitValidationError validator(String value) {
    return null;
  }
}

enum DireccionValidationError { invalid }
class DireccionInput extends FormzInput<String, DireccionValidationError> {
  const DireccionInput.pure() : super.pure('');
  const DireccionInput.dirty([String value = '']) : super.dirty(value);

  @override
  DireccionValidationError validator(String value) {
    return null;
  }
}

enum TelefonoValidationError { invalid }
class TelefonoInput extends FormzInput<String, TelefonoValidationError> {
  const TelefonoInput.pure() : super.pure('');
  const TelefonoInput.dirty([String value = '']) : super.dirty(value);

  @override
  TelefonoValidationError validator(String value) {
    return null;
  }
}

enum CorreoElectronicoValidationError { invalid }
class CorreoElectronicoInput extends FormzInput<String, CorreoElectronicoValidationError> {
  const CorreoElectronicoInput.pure() : super.pure('');
  const CorreoElectronicoInput.dirty([String value = '']) : super.dirty(value);

  @override
  CorreoElectronicoValidationError validator(String value) {
    return null;
  }
}

