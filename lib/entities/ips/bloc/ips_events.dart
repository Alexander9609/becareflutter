part of 'ips_bloc.dart';

abstract class IpsEvent extends Equatable {
  const IpsEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitIpsList extends IpsEvent {}

class NombreChanged extends IpsEvent {
  final String nombre;
  
  const NombreChanged({@required this.nombre});
  
  @override
  List<Object> get props => [nombre];
}
class NitChanged extends IpsEvent {
  final String nit;
  
  const NitChanged({@required this.nit});
  
  @override
  List<Object> get props => [nit];
}
class DireccionChanged extends IpsEvent {
  final String direccion;
  
  const DireccionChanged({@required this.direccion});
  
  @override
  List<Object> get props => [direccion];
}
class TelefonoChanged extends IpsEvent {
  final String telefono;
  
  const TelefonoChanged({@required this.telefono});
  
  @override
  List<Object> get props => [telefono];
}
class CorreoElectronicoChanged extends IpsEvent {
  final String correoElectronico;
  
  const CorreoElectronicoChanged({@required this.correoElectronico});
  
  @override
  List<Object> get props => [correoElectronico];
}

class IpsFormSubmitted extends IpsEvent {}

class LoadIpsByIdForEdit extends IpsEvent {
  final int id;

  const LoadIpsByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteIpsById extends IpsEvent {
  final int id;

  const DeleteIpsById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadIpsByIdForView extends IpsEvent {
  final int id;

  const LoadIpsByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
