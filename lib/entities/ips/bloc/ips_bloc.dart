import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/ips/ips_model.dart';
import 'package:movilRCV2/entities/ips/ips_repository.dart';
import 'package:movilRCV2/entities/ips/bloc/ips_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'ips_events.dart';
part 'ips_state.dart';

class IpsBloc extends Bloc<IpsEvent, IpsState> {
  final IpsRepository _iPSRepository;

  final nombreController = TextEditingController();
  final nitController = TextEditingController();
  final direccionController = TextEditingController();
  final telefonoController = TextEditingController();
  final correoElectronicoController = TextEditingController();

  IpsBloc({@required IpsRepository iPSRepository}) : assert(iPSRepository != null),
        _iPSRepository = iPSRepository, 
  super(IpsState());

  @override
  void onTransition(Transition<IpsEvent, IpsState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<IpsState> mapEventToState(IpsEvent event) async* {
    if (event is InitIpsList) {
      yield* onInitList(event);
    } else if (event is IpsFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadIpsByIdForEdit) {
      yield* onLoadIpsIdForEdit(event);
    } else if (event is DeleteIpsById) {
      yield* onDeleteIpsId(event);
    } else if (event is LoadIpsByIdForView) {
      yield* onLoadIpsIdForView(event);
    }else if (event is NombreChanged){
      yield* onNombreChange(event);
    }else if (event is NitChanged){
      yield* onNitChange(event);
    }else if (event is DireccionChanged){
      yield* onDireccionChange(event);
    }else if (event is TelefonoChanged){
      yield* onTelefonoChange(event);
    }else if (event is CorreoElectronicoChanged){
      yield* onCorreoElectronicoChange(event);
    }  }

  Stream<IpsState> onInitList(InitIpsList event) async* {
    yield this.state.copyWith(iPSStatusUI: IpsStatusUI.loading);
    List<Ips> ips = await _iPSRepository.getAllIps();
    yield this.state.copyWith(ips: ips, iPSStatusUI: IpsStatusUI.done);
  }

  Stream<IpsState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Ips result;
        if(this.state.editMode) {
          Ips newIps = Ips(state.loadedIps.id,
            this.state.nombre.value,  
            this.state.nit.value,  
            this.state.direccion.value,  
            this.state.telefono.value,  
            this.state.correoElectronico.value,  
          );

          result = await _iPSRepository.update(newIps);
        } else {
          Ips newIps = Ips(null,
            this.state.nombre.value,  
            this.state.nit.value,  
            this.state.direccion.value,  
            this.state.telefono.value,  
            this.state.correoElectronico.value,  
          );

          result = await _iPSRepository.create(newIps);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<IpsState> onLoadIpsIdForEdit(LoadIpsByIdForEdit event) async* {
    yield this.state.copyWith(iPSStatusUI: IpsStatusUI.loading);
    Ips loadedIps = await _iPSRepository.getIps(event.id);

    final nombre = NombreInput.dirty(loadedIps?.nombre != null ? loadedIps.nombre: '');
    final nit = NitInput.dirty(loadedIps?.nit != null ? loadedIps.nit: '');
    final direccion = DireccionInput.dirty(loadedIps?.direccion != null ? loadedIps.direccion: '');
    final telefono = TelefonoInput.dirty(loadedIps?.telefono != null ? loadedIps.telefono: '');
    final correoElectronico = CorreoElectronicoInput.dirty(loadedIps?.correoElectronico != null ? loadedIps.correoElectronico: '');

    yield this.state.copyWith(loadedIps: loadedIps, editMode: true,
      nombre: nombre,
      nit: nit,
      direccion: direccion,
      telefono: telefono,
      correoElectronico: correoElectronico,
    iPSStatusUI: IpsStatusUI.done);

    nombreController.text = loadedIps.nombre;
    nitController.text = loadedIps.nit;
    direccionController.text = loadedIps.direccion;
    telefonoController.text = loadedIps.telefono;
    correoElectronicoController.text = loadedIps.correoElectronico;
  }

  Stream<IpsState> onDeleteIpsId(DeleteIpsById event) async* {
    try {
      await _iPSRepository.delete(event.id);
      this.add(InitIpsList());
      yield this.state.copyWith(deleteStatus: IpsDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: IpsDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: IpsDeleteStatus.none);
  }

  Stream<IpsState> onLoadIpsIdForView(LoadIpsByIdForView event) async* {
    yield this.state.copyWith(iPSStatusUI: IpsStatusUI.loading);
    try {
      Ips loadedIps = await _iPSRepository.getIps(event.id);
      yield this.state.copyWith(loadedIps: loadedIps, iPSStatusUI: IpsStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedIps: null, iPSStatusUI: IpsStatusUI.error);
    }
  }


  Stream<IpsState> onNombreChange(NombreChanged event) async* {
    final nombre = NombreInput.dirty(event.nombre);
    yield this.state.copyWith(
      nombre: nombre,
      formStatus: Formz.validate([
        nombre,
      this.state.nit,
      this.state.direccion,
      this.state.telefono,
      this.state.correoElectronico,
      ]),
    );
  }
  Stream<IpsState> onNitChange(NitChanged event) async* {
    final nit = NitInput.dirty(event.nit);
    yield this.state.copyWith(
      nit: nit,
      formStatus: Formz.validate([
      this.state.nombre,
        nit,
      this.state.direccion,
      this.state.telefono,
      this.state.correoElectronico,
      ]),
    );
  }
  Stream<IpsState> onDireccionChange(DireccionChanged event) async* {
    final direccion = DireccionInput.dirty(event.direccion);
    yield this.state.copyWith(
      direccion: direccion,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.nit,
        direccion,
      this.state.telefono,
      this.state.correoElectronico,
      ]),
    );
  }
  Stream<IpsState> onTelefonoChange(TelefonoChanged event) async* {
    final telefono = TelefonoInput.dirty(event.telefono);
    yield this.state.copyWith(
      telefono: telefono,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.nit,
      this.state.direccion,
        telefono,
      this.state.correoElectronico,
      ]),
    );
  }
  Stream<IpsState> onCorreoElectronicoChange(CorreoElectronicoChanged event) async* {
    final correoElectronico = CorreoElectronicoInput.dirty(event.correoElectronico);
    yield this.state.copyWith(
      correoElectronico: correoElectronico,
      formStatus: Formz.validate([
      this.state.nombre,
      this.state.nit,
      this.state.direccion,
      this.state.telefono,
        correoElectronico,
      ]),
    );
  }

  @override
  Future<void> close() {
    nombreController.dispose();
    nitController.dispose();
    direccionController.dispose();
    telefonoController.dispose();
    correoElectronicoController.dispose();
    return super.close();
  }

}