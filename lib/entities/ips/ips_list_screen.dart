import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/ips/bloc/ips_bloc.dart';
import 'package:movilRCV2/entities/ips/ips_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class IpsListScreen extends StatelessWidget {
    IpsListScreen({Key key}) : super(key: MovilRcv2Keys.iPSListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<IpsBloc, IpsState>(
      listener: (context, state) {
        if(state.deleteStatus == IpsDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('Ips deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('Ips List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<IpsBloc, IpsState>(
              buildWhen: (previous, current) => previous.ips != current.ips,
              builder: (context, state) {
                return Visibility(
                  visible: state.iPSStatusUI == IpsStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (Ips iPS in state.ips) iPSCard(iPS, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesIpsCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget iPSCard(Ips iPS, BuildContext context) {
    IpsBloc iPSBloc = BlocProvider.of<IpsBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Nombre : ${iPS.nombre.toString()}'),
                  subtitle: Text('Nit : ${iPS.nit.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesIpsView,
                    arguments: EntityArguments(iPS.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesIpsEdit,
                    arguments: EntityArguments(iPS.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(iPSBloc, context, iPS.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(IpsBloc iPSBloc, BuildContext context, int id) {
    return BlocProvider<IpsBloc>.value(
      value: iPSBloc,
      child: AlertDialog(
        title: new Text('Delete Ips'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              iPSBloc.add(DeleteIpsById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
