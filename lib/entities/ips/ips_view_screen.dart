import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/ips/bloc/ips_bloc.dart';
import 'package:movilRCV2/entities/ips/ips_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class IpsViewScreen extends StatelessWidget {
  IpsViewScreen({Key key}) : super(key: MovilRcv2Keys.iPSCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Ips View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<IpsBloc, IpsState>(
              buildWhen: (previous, current) => previous.loadedIps != current.loadedIps,
              builder: (context, state) {
                return Visibility(
                  visible: state.iPSStatusUI == IpsStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    iPSCard(state.loadedIps, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesIpsCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget iPSCard(Ips iPS, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + iPS.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Nombre : ' + iPS.nombre.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Nit : ' + iPS.nit.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Direccion : ' + iPS.direccion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Telefono : ' + iPS.telefono.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Correo Electronico : ' + iPS.correoElectronico.toString(), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
