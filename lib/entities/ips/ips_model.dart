import 'package:dart_json_mapper/dart_json_mapper.dart';


@jsonSerializable
class Ips {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'nombre')
  final String nombre;

  @JsonProperty(name: 'nit')
  final String nit;

  @JsonProperty(name: 'direccion')
  final String direccion;

  @JsonProperty(name: 'telefono')
  final String telefono;

  @JsonProperty(name: 'correoElectronico')
  final String correoElectronico;
        
 const Ips (
     this.id,
        this.nombre,
        this.nit,
        this.direccion,
        this.telefono,
        this.correoElectronico,
    );

@override
String toString() {
    return 'Ips{'+
    'id: $id,' +
        'nombre: $nombre,' +
        'nit: $nit,' +
        'direccion: $direccion,' +
        'telefono: $telefono,' +
        'correoElectronico: $correoElectronico,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Ips &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


