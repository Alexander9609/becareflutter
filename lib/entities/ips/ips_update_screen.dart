import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/ips/bloc/ips_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/ips/ips_model.dart';

class IpsUpdateScreen extends StatelessWidget {
  IpsUpdateScreen({Key key}) : super(key: MovilRcv2Keys.iPSCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<IpsBloc, IpsState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesIpsList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<IpsBloc, IpsState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Ips':
'Create Ips';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          nombreField(),
          nitField(),
          direccionField(),
          telefonoField(),
          correoElectronicoField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget nombreField() {
        return BlocBuilder<IpsBloc, IpsState>(
            buildWhen: (previous, current) => previous.nombre != current.nombre,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<IpsBloc>().nombreController,
                  onChanged: (value) { context.bloc<IpsBloc>()
                    .add(NombreChanged(nombre:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'nombre'));
            }
        );
      }
      Widget nitField() {
        return BlocBuilder<IpsBloc, IpsState>(
            buildWhen: (previous, current) => previous.nit != current.nit,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<IpsBloc>().nitController,
                  onChanged: (value) { context.bloc<IpsBloc>()
                    .add(NitChanged(nit:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'nit'));
            }
        );
      }
      Widget direccionField() {
        return BlocBuilder<IpsBloc, IpsState>(
            buildWhen: (previous, current) => previous.direccion != current.direccion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<IpsBloc>().direccionController,
                  onChanged: (value) { context.bloc<IpsBloc>()
                    .add(DireccionChanged(direccion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'direccion'));
            }
        );
      }
      Widget telefonoField() {
        return BlocBuilder<IpsBloc, IpsState>(
            buildWhen: (previous, current) => previous.telefono != current.telefono,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<IpsBloc>().telefonoController,
                  onChanged: (value) { context.bloc<IpsBloc>()
                    .add(TelefonoChanged(telefono:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'telefono'));
            }
        );
      }
      Widget correoElectronicoField() {
        return BlocBuilder<IpsBloc, IpsState>(
            buildWhen: (previous, current) => previous.correoElectronico != current.correoElectronico,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<IpsBloc>().correoElectronicoController,
                  onChanged: (value) { context.bloc<IpsBloc>()
                    .add(CorreoElectronicoChanged(correoElectronico:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'correoElectronico'));
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<IpsBloc, IpsState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(IpsState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<IpsBloc, IpsState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<IpsBloc>().add(IpsFormSubmitted()) : null,
          );
        }
    );
  }
}
