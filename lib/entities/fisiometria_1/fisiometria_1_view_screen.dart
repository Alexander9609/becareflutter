import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/fisiometria_1/bloc/fisiometria_1_bloc.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class Fisiometria1ViewScreen extends StatelessWidget {
  Fisiometria1ViewScreen({Key key}) : super(key: MovilRcv2Keys.fisiometria1CreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Fisiometria1s View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
              buildWhen: (previous, current) => previous.loadedFisiometria1 != current.loadedFisiometria1,
              builder: (context, state) {
                return Visibility(
                  visible: state.fisiometria1StatusUI == Fisiometria1StatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    fisiometria1Card(state.loadedFisiometria1, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesFisiometria1Create),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget fisiometria1Card(Fisiometria1 fisiometria1, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + fisiometria1.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Ritmo Cardiaco : ' + fisiometria1.ritmoCardiaco.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Ritmo Respiratorio : ' + fisiometria1.ritmoRespiratorio.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Oximetria : ' + fisiometria1.oximetria.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Arterial Sistolica : ' + fisiometria1.presionArterialSistolica.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Arterial Diastolica : ' + fisiometria1.presionArterialDiastolica.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Temperatura : ' + fisiometria1.temperatura.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Time Instant : ' + (fisiometria1?.timeInstant != null ? DateFormat.yMMMMd('en').format(fisiometria1.timeInstant) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
