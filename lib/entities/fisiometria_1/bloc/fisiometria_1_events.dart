part of 'fisiometria_1_bloc.dart';

abstract class Fisiometria1Event extends Equatable {
  const Fisiometria1Event();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitFisiometria1List extends Fisiometria1Event {}

class RitmoCardiacoChanged extends Fisiometria1Event {
  final String ritmoCardiaco;
  
  const RitmoCardiacoChanged({@required this.ritmoCardiaco});
  
  @override
  List<Object> get props => [ritmoCardiaco];
}
class RitmoRespiratorioChanged extends Fisiometria1Event {
  final String ritmoRespiratorio;
  
  const RitmoRespiratorioChanged({@required this.ritmoRespiratorio});
  
  @override
  List<Object> get props => [ritmoRespiratorio];
}
class OximetriaChanged extends Fisiometria1Event {
  final String oximetria;
  
  const OximetriaChanged({@required this.oximetria});
  
  @override
  List<Object> get props => [oximetria];
}
class PresionArterialSistolicaChanged extends Fisiometria1Event {
  final String presionArterialSistolica;
  
  const PresionArterialSistolicaChanged({@required this.presionArterialSistolica});
  
  @override
  List<Object> get props => [presionArterialSistolica];
}
class PresionArterialDiastolicaChanged extends Fisiometria1Event {
  final String presionArterialDiastolica;
  
  const PresionArterialDiastolicaChanged({@required this.presionArterialDiastolica});
  
  @override
  List<Object> get props => [presionArterialDiastolica];
}
class TemperaturaChanged extends Fisiometria1Event {
  final String temperatura;
  
  const TemperaturaChanged({@required this.temperatura});
  
  @override
  List<Object> get props => [temperatura];
}
class TimeInstantChanged extends Fisiometria1Event {
  final DateTime timeInstant;
  
  const TimeInstantChanged({@required this.timeInstant});
  
  @override
  List<Object> get props => [timeInstant];
}

class Fisiometria1FormSubmitted extends Fisiometria1Event {}

class LoadFisiometria1ByIdForEdit extends Fisiometria1Event {
  final int id;

  const LoadFisiometria1ByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteFisiometria1ById extends Fisiometria1Event {
  final int id;

  const DeleteFisiometria1ById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadFisiometria1ByIdForView extends Fisiometria1Event {
  final int id;

  const LoadFisiometria1ByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
