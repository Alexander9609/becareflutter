import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';

enum RitmoCardiacoValidationError { invalid }
class RitmoCardiacoInput extends FormzInput<String, RitmoCardiacoValidationError> {
  const RitmoCardiacoInput.pure() : super.pure('');
  const RitmoCardiacoInput.dirty([String value = '']) : super.dirty(value);

  @override
  RitmoCardiacoValidationError validator(String value) {
    return null;
  }
}

enum RitmoRespiratorioValidationError { invalid }
class RitmoRespiratorioInput extends FormzInput<String, RitmoRespiratorioValidationError> {
  const RitmoRespiratorioInput.pure() : super.pure('');
  const RitmoRespiratorioInput.dirty([String value = '']) : super.dirty(value);

  @override
  RitmoRespiratorioValidationError validator(String value) {
    return null;
  }
}

enum OximetriaValidationError { invalid }
class OximetriaInput extends FormzInput<String, OximetriaValidationError> {
  const OximetriaInput.pure() : super.pure('');
  const OximetriaInput.dirty([String value = '']) : super.dirty(value);

  @override
  OximetriaValidationError validator(String value) {
    return null;
  }
}

enum PresionArterialSistolicaValidationError { invalid }
class PresionArterialSistolicaInput extends FormzInput<String, PresionArterialSistolicaValidationError> {
  const PresionArterialSistolicaInput.pure() : super.pure('');
  const PresionArterialSistolicaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionArterialSistolicaValidationError validator(String value) {
    return null;
  }
}

enum PresionArterialDiastolicaValidationError { invalid }
class PresionArterialDiastolicaInput extends FormzInput<String, PresionArterialDiastolicaValidationError> {
  const PresionArterialDiastolicaInput.pure() : super.pure('');
  const PresionArterialDiastolicaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionArterialDiastolicaValidationError validator(String value) {
    return null;
  }
}

enum TemperaturaValidationError { invalid }
class TemperaturaInput extends FormzInput<String, TemperaturaValidationError> {
  const TemperaturaInput.pure() : super.pure('');
  const TemperaturaInput.dirty([String value = '']) : super.dirty(value);

  @override
  TemperaturaValidationError validator(String value) {
    return null;
  }
}

enum TimeInstantValidationError { invalid }
class TimeInstantInput extends FormzInput<DateTime, TimeInstantValidationError> {
  TimeInstantInput.pure() : super.pure(DateTime.now());
  TimeInstantInput.dirty([DateTime value]) : super.dirty(value);

  @override
  TimeInstantValidationError validator(DateTime value) {
    return null;
  }
}

