part of 'fisiometria_1_bloc.dart';

enum Fisiometria1StatusUI {init, loading, error, done}
enum Fisiometria1DeleteStatus {ok, ko, none}

class Fisiometria1State extends Equatable {
  final List<Fisiometria1> fisiometria1S;
  final Fisiometria1 loadedFisiometria1;
  final bool editMode;
  final Fisiometria1DeleteStatus deleteStatus;
  final Fisiometria1StatusUI fisiometria1StatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final RitmoCardiacoInput ritmoCardiaco;
  final RitmoRespiratorioInput ritmoRespiratorio;
  final OximetriaInput oximetria;
  final PresionArterialSistolicaInput presionArterialSistolica;
  final PresionArterialDiastolicaInput presionArterialDiastolica;
  final TemperaturaInput temperatura;
  final TimeInstantInput timeInstant;

  
  Fisiometria1State(
TimeInstantInput timeInstant,{
    this.fisiometria1S = const [],
    this.fisiometria1StatusUI = Fisiometria1StatusUI.init,
    this.loadedFisiometria1 = const Fisiometria1(0,'','','','','','',null,null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = Fisiometria1DeleteStatus.none,
    this.ritmoCardiaco = const RitmoCardiacoInput.pure(),
    this.ritmoRespiratorio = const RitmoRespiratorioInput.pure(),
    this.oximetria = const OximetriaInput.pure(),
    this.presionArterialSistolica = const PresionArterialSistolicaInput.pure(),
    this.presionArterialDiastolica = const PresionArterialDiastolicaInput.pure(),
    this.temperatura = const TemperaturaInput.pure(),
  }):this.timeInstant = timeInstant ?? TimeInstantInput.pure()
;

  Fisiometria1State copyWith({
    List<Fisiometria1> fisiometria1S,
    Fisiometria1StatusUI fisiometria1StatusUI,
    bool editMode,
    Fisiometria1DeleteStatus deleteStatus,
    Fisiometria1 loadedFisiometria1,
    FormzStatus formStatus,
    String generalNotificationKey,
    RitmoCardiacoInput ritmoCardiaco,
    RitmoRespiratorioInput ritmoRespiratorio,
    OximetriaInput oximetria,
    PresionArterialSistolicaInput presionArterialSistolica,
    PresionArterialDiastolicaInput presionArterialDiastolica,
    TemperaturaInput temperatura,
    TimeInstantInput timeInstant,
  }) {
    return Fisiometria1State(
        timeInstant,
      fisiometria1S: fisiometria1S ?? this.fisiometria1S,
      fisiometria1StatusUI: fisiometria1StatusUI ?? this.fisiometria1StatusUI,
      loadedFisiometria1: loadedFisiometria1 ?? this.loadedFisiometria1,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      ritmoCardiaco: ritmoCardiaco ?? this.ritmoCardiaco,
      ritmoRespiratorio: ritmoRespiratorio ?? this.ritmoRespiratorio,
      oximetria: oximetria ?? this.oximetria,
      presionArterialSistolica: presionArterialSistolica ?? this.presionArterialSistolica,
      presionArterialDiastolica: presionArterialDiastolica ?? this.presionArterialDiastolica,
      temperatura: temperatura ?? this.temperatura,
    );
  }

  @override
  List<Object> get props => [fisiometria1S, fisiometria1StatusUI,
     loadedFisiometria1, editMode, deleteStatus, formStatus, generalNotificationKey, 
ritmoCardiaco,ritmoRespiratorio,oximetria,presionArterialSistolica,presionArterialDiastolica,temperatura,timeInstant,];

  @override
  bool get stringify => true;
}
