import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_repository.dart';
import 'package:movilRCV2/entities/fisiometria_1/bloc/fisiometria_1_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'fisiometria_1_events.dart';
part 'fisiometria_1_state.dart';

class Fisiometria1Bloc extends Bloc<Fisiometria1Event, Fisiometria1State> {
  final Fisiometria1Repository _fisiometria1Repository;

  final ritmoCardiacoController = TextEditingController();
  final ritmoRespiratorioController = TextEditingController();
  final oximetriaController = TextEditingController();
  final presionArterialSistolicaController = TextEditingController();
  final presionArterialDiastolicaController = TextEditingController();
  final temperaturaController = TextEditingController();
  final timeInstantController = TextEditingController();

  Fisiometria1Bloc({@required Fisiometria1Repository fisiometria1Repository}) : assert(fisiometria1Repository != null),
        _fisiometria1Repository = fisiometria1Repository, 
  super(Fisiometria1State(null,));

  @override
  void onTransition(Transition<Fisiometria1Event, Fisiometria1State> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<Fisiometria1State> mapEventToState(Fisiometria1Event event) async* {
    if (event is InitFisiometria1List) {
      yield* onInitList(event);
    } else if (event is Fisiometria1FormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadFisiometria1ByIdForEdit) {
      yield* onLoadFisiometria1IdForEdit(event);
    } else if (event is DeleteFisiometria1ById) {
      yield* onDeleteFisiometria1Id(event);
    } else if (event is LoadFisiometria1ByIdForView) {
      yield* onLoadFisiometria1IdForView(event);
    }else if (event is RitmoCardiacoChanged){
      yield* onRitmoCardiacoChange(event);
    }else if (event is RitmoRespiratorioChanged){
      yield* onRitmoRespiratorioChange(event);
    }else if (event is OximetriaChanged){
      yield* onOximetriaChange(event);
    }else if (event is PresionArterialSistolicaChanged){
      yield* onPresionArterialSistolicaChange(event);
    }else if (event is PresionArterialDiastolicaChanged){
      yield* onPresionArterialDiastolicaChange(event);
    }else if (event is TemperaturaChanged){
      yield* onTemperaturaChange(event);
    }else if (event is TimeInstantChanged){
      yield* onTimeInstantChange(event);
    }  }

  Stream<Fisiometria1State> onInitList(InitFisiometria1List event) async* {
    yield this.state.copyWith(fisiometria1StatusUI: Fisiometria1StatusUI.loading);
    List<Fisiometria1> fisiometria1S = await _fisiometria1Repository.getAllFisiometria1s();
    yield this.state.copyWith(fisiometria1S: fisiometria1S, fisiometria1StatusUI: Fisiometria1StatusUI.done);
  }

  Stream<Fisiometria1State> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Fisiometria1 result;
        if(this.state.editMode) {
          Fisiometria1 newFisiometria1 = Fisiometria1(state.loadedFisiometria1.id,
            this.state.ritmoCardiaco.value,  
            this.state.ritmoRespiratorio.value,  
            this.state.oximetria.value,  
            this.state.presionArterialSistolica.value,  
            this.state.presionArterialDiastolica.value,  
            this.state.temperatura.value,  
            this.state.timeInstant.value,  
            null, 
          );

          result = await _fisiometria1Repository.update(newFisiometria1);
        } else {
          Fisiometria1 newFisiometria1 = Fisiometria1(null,
            this.state.ritmoCardiaco.value,  
            this.state.ritmoRespiratorio.value,  
            this.state.oximetria.value,  
            this.state.presionArterialSistolica.value,  
            this.state.presionArterialDiastolica.value,  
            this.state.temperatura.value,  
            this.state.timeInstant.value,  
            null, 
          );

          result = await _fisiometria1Repository.create(newFisiometria1);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<Fisiometria1State> onLoadFisiometria1IdForEdit(LoadFisiometria1ByIdForEdit event) async* {
    yield this.state.copyWith(fisiometria1StatusUI: Fisiometria1StatusUI.loading);
    Fisiometria1 loadedFisiometria1 = await _fisiometria1Repository.getFisiometria1(event.id);

    final ritmoCardiaco = RitmoCardiacoInput.dirty(loadedFisiometria1?.ritmoCardiaco != null ? loadedFisiometria1.ritmoCardiaco: '');
    final ritmoRespiratorio = RitmoRespiratorioInput.dirty(loadedFisiometria1?.ritmoRespiratorio != null ? loadedFisiometria1.ritmoRespiratorio: '');
    final oximetria = OximetriaInput.dirty(loadedFisiometria1?.oximetria != null ? loadedFisiometria1.oximetria: '');
    final presionArterialSistolica = PresionArterialSistolicaInput.dirty(loadedFisiometria1?.presionArterialSistolica != null ? loadedFisiometria1.presionArterialSistolica: '');
    final presionArterialDiastolica = PresionArterialDiastolicaInput.dirty(loadedFisiometria1?.presionArterialDiastolica != null ? loadedFisiometria1.presionArterialDiastolica: '');
    final temperatura = TemperaturaInput.dirty(loadedFisiometria1?.temperatura != null ? loadedFisiometria1.temperatura: '');
    final timeInstant = TimeInstantInput.dirty(loadedFisiometria1?.timeInstant != null ? loadedFisiometria1.timeInstant: null);

    yield this.state.copyWith(loadedFisiometria1: loadedFisiometria1, editMode: true,
      ritmoCardiaco: ritmoCardiaco,
      ritmoRespiratorio: ritmoRespiratorio,
      oximetria: oximetria,
      presionArterialSistolica: presionArterialSistolica,
      presionArterialDiastolica: presionArterialDiastolica,
      temperatura: temperatura,
      timeInstant: timeInstant,
    fisiometria1StatusUI: Fisiometria1StatusUI.done);

    ritmoCardiacoController.text = loadedFisiometria1.ritmoCardiaco;
    ritmoRespiratorioController.text = loadedFisiometria1.ritmoRespiratorio;
    oximetriaController.text = loadedFisiometria1.oximetria;
    presionArterialSistolicaController.text = loadedFisiometria1.presionArterialSistolica;
    presionArterialDiastolicaController.text = loadedFisiometria1.presionArterialDiastolica;
    temperaturaController.text = loadedFisiometria1.temperatura;
    timeInstantController.text = DateFormat.yMMMMd('en').format(loadedFisiometria1?.timeInstant);
  }

  Stream<Fisiometria1State> onDeleteFisiometria1Id(DeleteFisiometria1ById event) async* {
    try {
      await _fisiometria1Repository.delete(event.id);
      this.add(InitFisiometria1List());
      yield this.state.copyWith(deleteStatus: Fisiometria1DeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: Fisiometria1DeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: Fisiometria1DeleteStatus.none);
  }

  Stream<Fisiometria1State> onLoadFisiometria1IdForView(LoadFisiometria1ByIdForView event) async* {
    yield this.state.copyWith(fisiometria1StatusUI: Fisiometria1StatusUI.loading);
    try {
      Fisiometria1 loadedFisiometria1 = await _fisiometria1Repository.getFisiometria1(event.id);
      yield this.state.copyWith(loadedFisiometria1: loadedFisiometria1, fisiometria1StatusUI: Fisiometria1StatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedFisiometria1: null, fisiometria1StatusUI: Fisiometria1StatusUI.error);
    }
  }


  Stream<Fisiometria1State> onRitmoCardiacoChange(RitmoCardiacoChanged event) async* {
    final ritmoCardiaco = RitmoCardiacoInput.dirty(event.ritmoCardiaco);
    yield this.state.copyWith(
      ritmoCardiaco: ritmoCardiaco,
      formStatus: Formz.validate([
        ritmoCardiaco,
      this.state.ritmoRespiratorio,
      this.state.oximetria,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
      this.state.temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onRitmoRespiratorioChange(RitmoRespiratorioChanged event) async* {
    final ritmoRespiratorio = RitmoRespiratorioInput.dirty(event.ritmoRespiratorio);
    yield this.state.copyWith(
      ritmoRespiratorio: ritmoRespiratorio,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
        ritmoRespiratorio,
      this.state.oximetria,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
      this.state.temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onOximetriaChange(OximetriaChanged event) async* {
    final oximetria = OximetriaInput.dirty(event.oximetria);
    yield this.state.copyWith(
      oximetria: oximetria,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
      this.state.ritmoRespiratorio,
        oximetria,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
      this.state.temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onPresionArterialSistolicaChange(PresionArterialSistolicaChanged event) async* {
    final presionArterialSistolica = PresionArterialSistolicaInput.dirty(event.presionArterialSistolica);
    yield this.state.copyWith(
      presionArterialSistolica: presionArterialSistolica,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
      this.state.ritmoRespiratorio,
      this.state.oximetria,
        presionArterialSistolica,
      this.state.presionArterialDiastolica,
      this.state.temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onPresionArterialDiastolicaChange(PresionArterialDiastolicaChanged event) async* {
    final presionArterialDiastolica = PresionArterialDiastolicaInput.dirty(event.presionArterialDiastolica);
    yield this.state.copyWith(
      presionArterialDiastolica: presionArterialDiastolica,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
      this.state.ritmoRespiratorio,
      this.state.oximetria,
      this.state.presionArterialSistolica,
        presionArterialDiastolica,
      this.state.temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onTemperaturaChange(TemperaturaChanged event) async* {
    final temperatura = TemperaturaInput.dirty(event.temperatura);
    yield this.state.copyWith(
      temperatura: temperatura,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
      this.state.ritmoRespiratorio,
      this.state.oximetria,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
        temperatura,
      this.state.timeInstant,
      ]),
    );
  }
  Stream<Fisiometria1State> onTimeInstantChange(TimeInstantChanged event) async* {
    final timeInstant = TimeInstantInput.dirty(event.timeInstant);
    yield this.state.copyWith(
      timeInstant: timeInstant,
      formStatus: Formz.validate([
      this.state.ritmoCardiaco,
      this.state.ritmoRespiratorio,
      this.state.oximetria,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
      this.state.temperatura,
        timeInstant,
      ]),
    );
  }

  @override
  Future<void> close() {
    ritmoCardiacoController.dispose();
    ritmoRespiratorioController.dispose();
    oximetriaController.dispose();
    presionArterialSistolicaController.dispose();
    presionArterialDiastolicaController.dispose();
    temperaturaController.dispose();
    timeInstantController.dispose();
    return super.close();
  }

}