import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class Fisiometria1Repository {
    Fisiometria1Repository();
  
  static final String uriEndpoint = '/fisiometria-1-s';

  Future<List<Fisiometria1>> getAllFisiometria1s() async {
    final allFisiometria1sRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Fisiometria1>>(allFisiometria1sRequest.body);
  }

  Future<Fisiometria1> getFisiometria1(int id) async {
    final fisiometria1Request = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Fisiometria1>(fisiometria1Request.body);
  }

  Future<Fisiometria1> create(Fisiometria1 fisiometria1) async {
    final fisiometria1Request = await HttpUtils.postRequest('$uriEndpoint', fisiometria1);
    return JsonMapper.deserialize<Fisiometria1>(fisiometria1Request.body);
  }

  Future<Fisiometria1> update(Fisiometria1 fisiometria1) async {
    final fisiometria1Request = await HttpUtils.putRequest('$uriEndpoint', fisiometria1);
    return JsonMapper.deserialize<Fisiometria1>(fisiometria1Request.body);
  }

  Future<void> delete(int id) async {
    final fisiometria1Request = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
