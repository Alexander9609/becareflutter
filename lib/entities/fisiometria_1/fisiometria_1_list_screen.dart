import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/fisiometria_1/bloc/fisiometria_1_bloc.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class Fisiometria1ListScreen extends StatelessWidget {
    Fisiometria1ListScreen({Key key}) : super(key: MovilRcv2Keys.fisiometria1ListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<Fisiometria1Bloc, Fisiometria1State>(
      listener: (context, state) {
        if(state.deleteStatus == Fisiometria1DeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('Fisiometria1 deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('Fisiometria1s List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
              buildWhen: (previous, current) => previous.fisiometria1S != current.fisiometria1S,
              builder: (context, state) {
                return Visibility(
                  visible: state.fisiometria1StatusUI == Fisiometria1StatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (Fisiometria1 fisiometria1 in state.fisiometria1S) fisiometria1Card(fisiometria1, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesFisiometria1Create),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget fisiometria1Card(Fisiometria1 fisiometria1, BuildContext context) {
    Fisiometria1Bloc fisiometria1Bloc = BlocProvider.of<Fisiometria1Bloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Ritmo Cardiaco : ${fisiometria1.ritmoCardiaco.toString()}'),
                  subtitle: Text('Ritmo Respiratorio : ${fisiometria1.ritmoRespiratorio.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesFisiometria1View,
                    arguments: EntityArguments(fisiometria1.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesFisiometria1Edit,
                    arguments: EntityArguments(fisiometria1.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(fisiometria1Bloc, context, fisiometria1.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(Fisiometria1Bloc fisiometria1Bloc, BuildContext context, int id) {
    return BlocProvider<Fisiometria1Bloc>.value(
      value: fisiometria1Bloc,
      child: AlertDialog(
        title: new Text('Delete Fisiometria1s'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              fisiometria1Bloc.add(DeleteFisiometria1ById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
