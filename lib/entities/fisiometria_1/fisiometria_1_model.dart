import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:movilRCV2/shared/models/user.dart';

@jsonSerializable
class Fisiometria1 {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'ritmoCardiaco')
  final String ritmoCardiaco;

  @JsonProperty(name: 'ritmoRespiratorio')
  final String ritmoRespiratorio;

  @JsonProperty(name: 'oximetria')
  final String oximetria;

  @JsonProperty(name: 'presionArterialSistolica')
  final String presionArterialSistolica;

  @JsonProperty(name: 'presionArterialDiastolica')
  final String presionArterialDiastolica;

  @JsonProperty(name: 'temperatura')
  final String temperatura;

  @JsonProperty(name: 'timeInstant', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime timeInstant;

  @JsonProperty(name: 'user')
  final User user;
        
 const Fisiometria1 (
     this.id,
        this.ritmoCardiaco,
        this.ritmoRespiratorio,
        this.oximetria,
        this.presionArterialSistolica,
        this.presionArterialDiastolica,
        this.temperatura,
        this.timeInstant,
        this.user,
    );

@override
String toString() {
    return 'Fisiometria1{'+
    'id: $id,' +
        'ritmoCardiaco: $ritmoCardiaco,' +
        'ritmoRespiratorio: $ritmoRespiratorio,' +
        'oximetria: $oximetria,' +
        'presionArterialSistolica: $presionArterialSistolica,' +
        'presionArterialDiastolica: $presionArterialDiastolica,' +
        'temperatura: $temperatura,' +
        'timeInstant: $timeInstant,' +
        'user: $user,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Fisiometria1 &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


