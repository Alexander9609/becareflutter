import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/fisiometria_1/bloc/fisiometria_1_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_model.dart';

class Fisiometria1UpdateScreen extends StatelessWidget {
  Fisiometria1UpdateScreen({Key key}) : super(key: MovilRcv2Keys.fisiometria1CreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<Fisiometria1Bloc, Fisiometria1State>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesFisiometria1List);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Fisiometria1s':
'Create Fisiometria1s';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          ritmoCardiacoField(),
          ritmoRespiratorioField(),
          oximetriaField(),
          presionArterialSistolicaField(),
          presionArterialDiastolicaField(),
          temperaturaField(),
          timeInstantField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget ritmoCardiacoField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.ritmoCardiaco != current.ritmoCardiaco,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().ritmoCardiacoController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(RitmoCardiacoChanged(ritmoCardiaco:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'ritmoCardiaco'));
            }
        );
      }
      Widget ritmoRespiratorioField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.ritmoRespiratorio != current.ritmoRespiratorio,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().ritmoRespiratorioController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(RitmoRespiratorioChanged(ritmoRespiratorio:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'ritmoRespiratorio'));
            }
        );
      }
      Widget oximetriaField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.oximetria != current.oximetria,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().oximetriaController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(OximetriaChanged(oximetria:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'oximetria'));
            }
        );
      }
      Widget presionArterialSistolicaField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.presionArterialSistolica != current.presionArterialSistolica,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().presionArterialSistolicaController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(PresionArterialSistolicaChanged(presionArterialSistolica:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionArterialSistolica'));
            }
        );
      }
      Widget presionArterialDiastolicaField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.presionArterialDiastolica != current.presionArterialDiastolica,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().presionArterialDiastolicaController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(PresionArterialDiastolicaChanged(presionArterialDiastolica:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionArterialDiastolica'));
            }
        );
      }
      Widget temperaturaField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.temperatura != current.temperatura,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<Fisiometria1Bloc>().temperaturaController,
                  onChanged: (value) { context.bloc<Fisiometria1Bloc>()
                    .add(TemperaturaChanged(temperatura:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'temperatura'));
            }
        );
      }
      Widget timeInstantField() {
        return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
            buildWhen: (previous, current) => previous.timeInstant != current.timeInstant,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<Fisiometria1Bloc>().timeInstantController,
                onChanged: (value) { context.bloc<Fisiometria1Bloc>().add(TimeInstantChanged(timeInstant: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'timeInstant',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(Fisiometria1State state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<Fisiometria1Bloc, Fisiometria1State>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<Fisiometria1Bloc>().add(Fisiometria1FormSubmitted()) : null,
          );
        }
    );
  }
}
