import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/alarma/bloc/alarma_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/alarma/alarma_model.dart';

class AlarmaUpdateScreen extends StatelessWidget {
  AlarmaUpdateScreen({Key key}) : super(key: MovilRcv2Keys.alarmaCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AlarmaBloc, AlarmaState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesAlarmaList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<AlarmaBloc, AlarmaState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Alarmas':
'Create Alarmas';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          timeInstantField(),
          descripcionField(),
          procedimientoField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget timeInstantField() {
        return BlocBuilder<AlarmaBloc, AlarmaState>(
            buildWhen: (previous, current) => previous.timeInstant != current.timeInstant,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<AlarmaBloc>().timeInstantController,
                onChanged: (value) { context.bloc<AlarmaBloc>().add(TimeInstantChanged(timeInstant: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'timeInstant',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }
      Widget descripcionField() {
        return BlocBuilder<AlarmaBloc, AlarmaState>(
            buildWhen: (previous, current) => previous.descripcion != current.descripcion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<AlarmaBloc>().descripcionController,
                  onChanged: (value) { context.bloc<AlarmaBloc>()
                    .add(DescripcionChanged(descripcion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'descripcion'));
            }
        );
      }
      Widget procedimientoField() {
        return BlocBuilder<AlarmaBloc, AlarmaState>(
            buildWhen: (previous, current) => previous.procedimiento != current.procedimiento,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<AlarmaBloc>().procedimientoController,
                  onChanged: (value) { context.bloc<AlarmaBloc>()
                    .add(ProcedimientoChanged(procedimiento:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'procedimiento'));
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<AlarmaBloc, AlarmaState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(AlarmaState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<AlarmaBloc, AlarmaState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<AlarmaBloc>().add(AlarmaFormSubmitted()) : null,
          );
        }
    );
  }
}
