part of 'alarma_bloc.dart';

abstract class AlarmaEvent extends Equatable {
  const AlarmaEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitAlarmaList extends AlarmaEvent {}

class TimeInstantChanged extends AlarmaEvent {
  final DateTime timeInstant;
  
  const TimeInstantChanged({@required this.timeInstant});
  
  @override
  List<Object> get props => [timeInstant];
}
class DescripcionChanged extends AlarmaEvent {
  final String descripcion;
  
  const DescripcionChanged({@required this.descripcion});
  
  @override
  List<Object> get props => [descripcion];
}
class ProcedimientoChanged extends AlarmaEvent {
  final String procedimiento;
  
  const ProcedimientoChanged({@required this.procedimiento});
  
  @override
  List<Object> get props => [procedimiento];
}

class AlarmaFormSubmitted extends AlarmaEvent {}

class LoadAlarmaByIdForEdit extends AlarmaEvent {
  final int id;

  const LoadAlarmaByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteAlarmaById extends AlarmaEvent {
  final int id;

  const DeleteAlarmaById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadAlarmaByIdForView extends AlarmaEvent {
  final int id;

  const LoadAlarmaByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
