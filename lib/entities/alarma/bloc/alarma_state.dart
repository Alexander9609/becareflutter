part of 'alarma_bloc.dart';

enum AlarmaStatusUI {init, loading, error, done}
enum AlarmaDeleteStatus {ok, ko, none}

class AlarmaState extends Equatable {
  final List<Alarma> alarmas;
  final Alarma loadedAlarma;
  final bool editMode;
  final AlarmaDeleteStatus deleteStatus;
  final AlarmaStatusUI alarmaStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final TimeInstantInput timeInstant;
  final DescripcionInput descripcion;
  final ProcedimientoInput procedimiento;

  
  AlarmaState(
TimeInstantInput timeInstant,{
    this.alarmas = const [],
    this.alarmaStatusUI = AlarmaStatusUI.init,
    this.loadedAlarma = const Alarma(0,null,'','',null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = AlarmaDeleteStatus.none,
    this.descripcion = const DescripcionInput.pure(),
    this.procedimiento = const ProcedimientoInput.pure(),
  }):this.timeInstant = timeInstant ?? TimeInstantInput.pure()
;

  AlarmaState copyWith({
    List<Alarma> alarmas,
    AlarmaStatusUI alarmaStatusUI,
    bool editMode,
    AlarmaDeleteStatus deleteStatus,
    Alarma loadedAlarma,
    FormzStatus formStatus,
    String generalNotificationKey,
    TimeInstantInput timeInstant,
    DescripcionInput descripcion,
    ProcedimientoInput procedimiento,
  }) {
    return AlarmaState(
        timeInstant,
      alarmas: alarmas ?? this.alarmas,
      alarmaStatusUI: alarmaStatusUI ?? this.alarmaStatusUI,
      loadedAlarma: loadedAlarma ?? this.loadedAlarma,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      descripcion: descripcion ?? this.descripcion,
      procedimiento: procedimiento ?? this.procedimiento,
    );
  }

  @override
  List<Object> get props => [alarmas, alarmaStatusUI,
     loadedAlarma, editMode, deleteStatus, formStatus, generalNotificationKey, 
timeInstant,descripcion,procedimiento,];

  @override
  bool get stringify => true;
}
