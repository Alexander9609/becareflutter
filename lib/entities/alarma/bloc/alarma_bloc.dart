import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/alarma/alarma_model.dart';
import 'package:movilRCV2/entities/alarma/alarma_repository.dart';
import 'package:movilRCV2/entities/alarma/bloc/alarma_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'alarma_events.dart';
part 'alarma_state.dart';

class AlarmaBloc extends Bloc<AlarmaEvent, AlarmaState> {
  final AlarmaRepository _alarmaRepository;

  final timeInstantController = TextEditingController();
  final descripcionController = TextEditingController();
  final procedimientoController = TextEditingController();

  AlarmaBloc({@required AlarmaRepository alarmaRepository}) : assert(alarmaRepository != null),
        _alarmaRepository = alarmaRepository, 
  super(AlarmaState(null,));

  @override
  void onTransition(Transition<AlarmaEvent, AlarmaState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<AlarmaState> mapEventToState(AlarmaEvent event) async* {
    if (event is InitAlarmaList) {
      yield* onInitList(event);
    } else if (event is AlarmaFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadAlarmaByIdForEdit) {
      yield* onLoadAlarmaIdForEdit(event);
    } else if (event is DeleteAlarmaById) {
      yield* onDeleteAlarmaId(event);
    } else if (event is LoadAlarmaByIdForView) {
      yield* onLoadAlarmaIdForView(event);
    }else if (event is TimeInstantChanged){
      yield* onTimeInstantChange(event);
    }else if (event is DescripcionChanged){
      yield* onDescripcionChange(event);
    }else if (event is ProcedimientoChanged){
      yield* onProcedimientoChange(event);
    }  }

  Stream<AlarmaState> onInitList(InitAlarmaList event) async* {
    yield this.state.copyWith(alarmaStatusUI: AlarmaStatusUI.loading);
    List<Alarma> alarmas = await _alarmaRepository.getAllAlarmas();
    yield this.state.copyWith(alarmas: alarmas, alarmaStatusUI: AlarmaStatusUI.done);
  }

  Stream<AlarmaState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Alarma result;
        if(this.state.editMode) {
          Alarma newAlarma = Alarma(state.loadedAlarma.id,
            this.state.timeInstant.value,  
            this.state.descripcion.value,  
            this.state.procedimiento.value,  
            null, 
          );

          result = await _alarmaRepository.update(newAlarma);
        } else {
          Alarma newAlarma = Alarma(null,
            this.state.timeInstant.value,  
            this.state.descripcion.value,  
            this.state.procedimiento.value,  
            null, 
          );

          result = await _alarmaRepository.create(newAlarma);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<AlarmaState> onLoadAlarmaIdForEdit(LoadAlarmaByIdForEdit event) async* {
    yield this.state.copyWith(alarmaStatusUI: AlarmaStatusUI.loading);
    Alarma loadedAlarma = await _alarmaRepository.getAlarma(event.id);

    final timeInstant = TimeInstantInput.dirty(loadedAlarma?.timeInstant != null ? loadedAlarma.timeInstant: null);
    final descripcion = DescripcionInput.dirty(loadedAlarma?.descripcion != null ? loadedAlarma.descripcion: '');
    final procedimiento = ProcedimientoInput.dirty(loadedAlarma?.procedimiento != null ? loadedAlarma.procedimiento: '');

    yield this.state.copyWith(loadedAlarma: loadedAlarma, editMode: true,
      timeInstant: timeInstant,
      descripcion: descripcion,
      procedimiento: procedimiento,
    alarmaStatusUI: AlarmaStatusUI.done);

    timeInstantController.text = DateFormat.yMMMMd('en').format(loadedAlarma?.timeInstant);
    descripcionController.text = loadedAlarma.descripcion;
    procedimientoController.text = loadedAlarma.procedimiento;
  }

  Stream<AlarmaState> onDeleteAlarmaId(DeleteAlarmaById event) async* {
    try {
      await _alarmaRepository.delete(event.id);
      this.add(InitAlarmaList());
      yield this.state.copyWith(deleteStatus: AlarmaDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: AlarmaDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: AlarmaDeleteStatus.none);
  }

  Stream<AlarmaState> onLoadAlarmaIdForView(LoadAlarmaByIdForView event) async* {
    yield this.state.copyWith(alarmaStatusUI: AlarmaStatusUI.loading);
    try {
      Alarma loadedAlarma = await _alarmaRepository.getAlarma(event.id);
      yield this.state.copyWith(loadedAlarma: loadedAlarma, alarmaStatusUI: AlarmaStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedAlarma: null, alarmaStatusUI: AlarmaStatusUI.error);
    }
  }


  Stream<AlarmaState> onTimeInstantChange(TimeInstantChanged event) async* {
    final timeInstant = TimeInstantInput.dirty(event.timeInstant);
    yield this.state.copyWith(
      timeInstant: timeInstant,
      formStatus: Formz.validate([
        timeInstant,
      this.state.descripcion,
      this.state.procedimiento,
      ]),
    );
  }
  Stream<AlarmaState> onDescripcionChange(DescripcionChanged event) async* {
    final descripcion = DescripcionInput.dirty(event.descripcion);
    yield this.state.copyWith(
      descripcion: descripcion,
      formStatus: Formz.validate([
      this.state.timeInstant,
        descripcion,
      this.state.procedimiento,
      ]),
    );
  }
  Stream<AlarmaState> onProcedimientoChange(ProcedimientoChanged event) async* {
    final procedimiento = ProcedimientoInput.dirty(event.procedimiento);
    yield this.state.copyWith(
      procedimiento: procedimiento,
      formStatus: Formz.validate([
      this.state.timeInstant,
      this.state.descripcion,
        procedimiento,
      ]),
    );
  }

  @override
  Future<void> close() {
    timeInstantController.dispose();
    descripcionController.dispose();
    procedimientoController.dispose();
    return super.close();
  }

}