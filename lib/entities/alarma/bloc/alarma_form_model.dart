import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/alarma/alarma_model.dart';

enum TimeInstantValidationError { invalid }
class TimeInstantInput extends FormzInput<DateTime, TimeInstantValidationError> {
  TimeInstantInput.pure() : super.pure(DateTime.now());
  TimeInstantInput.dirty([DateTime value]) : super.dirty(value);

  @override
  TimeInstantValidationError validator(DateTime value) {
    return null;
  }
}

enum DescripcionValidationError { invalid }
class DescripcionInput extends FormzInput<String, DescripcionValidationError> {
  const DescripcionInput.pure() : super.pure('');
  const DescripcionInput.dirty([String value = '']) : super.dirty(value);

  @override
  DescripcionValidationError validator(String value) {
    return null;
  }
}

enum ProcedimientoValidationError { invalid }
class ProcedimientoInput extends FormzInput<String, ProcedimientoValidationError> {
  const ProcedimientoInput.pure() : super.pure('');
  const ProcedimientoInput.dirty([String value = '']) : super.dirty(value);

  @override
  ProcedimientoValidationError validator(String value) {
    return null;
  }
}

