import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/alarma/bloc/alarma_bloc.dart';
import 'package:movilRCV2/entities/alarma/alarma_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class AlarmaViewScreen extends StatelessWidget {
  AlarmaViewScreen({Key key}) : super(key: MovilRcv2Keys.alarmaCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Alarmas View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<AlarmaBloc, AlarmaState>(
              buildWhen: (previous, current) => previous.loadedAlarma != current.loadedAlarma,
              builder: (context, state) {
                return Visibility(
                  visible: state.alarmaStatusUI == AlarmaStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    alarmaCard(state.loadedAlarma, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesAlarmaCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget alarmaCard(Alarma alarma, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + alarma.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Time Instant : ' + (alarma?.timeInstant != null ? DateFormat.yMMMMd('en').format(alarma.timeInstant) : ''), style: Theme.of(context).textTheme.bodyText1,),
                Text('Descripcion : ' + alarma.descripcion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Procedimiento : ' + alarma.procedimiento.toString(), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
