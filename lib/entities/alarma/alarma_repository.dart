import 'package:movilRCV2/entities/alarma/alarma_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class AlarmaRepository {
    AlarmaRepository();
  
  static final String uriEndpoint = '/alarmas';

  Future<List<Alarma>> getAllAlarmas() async {
    final allAlarmasRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Alarma>>(allAlarmasRequest.body);
  }

  Future<Alarma> getAlarma(int id) async {
    final alarmaRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Alarma>(alarmaRequest.body);
  }

  Future<Alarma> create(Alarma alarma) async {
    final alarmaRequest = await HttpUtils.postRequest('$uriEndpoint', alarma);
    return JsonMapper.deserialize<Alarma>(alarmaRequest.body);
  }

  Future<Alarma> update(Alarma alarma) async {
    final alarmaRequest = await HttpUtils.putRequest('$uriEndpoint', alarma);
    return JsonMapper.deserialize<Alarma>(alarmaRequest.body);
  }

  Future<void> delete(int id) async {
    final alarmaRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
