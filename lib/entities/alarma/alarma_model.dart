import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:movilRCV2/shared/models/user.dart';

@jsonSerializable
class Alarma {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'timeInstant', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime timeInstant;

  @JsonProperty(name: 'descripcion')
  final String descripcion;

  @JsonProperty(name: 'procedimiento')
  final String procedimiento;

  @JsonProperty(name: 'user')
  final User user;
        
 const Alarma (
     this.id,
        this.timeInstant,
        this.descripcion,
        this.procedimiento,
        this.user,
    );

@override
String toString() {
    return 'Alarma{'+
    'id: $id,' +
        'timeInstant: $timeInstant,' +
        'descripcion: $descripcion,' +
        'procedimiento: $procedimiento,' +
        'user: $user,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Alarma &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


