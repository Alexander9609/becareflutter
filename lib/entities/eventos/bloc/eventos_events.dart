part of 'eventos_bloc.dart';

abstract class EventosEvent extends Equatable {
  const EventosEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitEventosList extends EventosEvent {}

class FechaChanged extends EventosEvent {
  final DateTime fecha;
  
  const FechaChanged({@required this.fecha});
  
  @override
  List<Object> get props => [fecha];
}
class DescripcionChanged extends EventosEvent {
  final String descripcion;
  
  const DescripcionChanged({@required this.descripcion});
  
  @override
  List<Object> get props => [descripcion];
}

class EventosFormSubmitted extends EventosEvent {}

class LoadEventosByIdForEdit extends EventosEvent {
  final int id;

  const LoadEventosByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteEventosById extends EventosEvent {
  final int id;

  const DeleteEventosById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadEventosByIdForView extends EventosEvent {
  final int id;

  const LoadEventosByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
