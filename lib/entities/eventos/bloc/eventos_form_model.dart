import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/eventos/eventos_model.dart';

enum FechaValidationError { invalid }
class FechaInput extends FormzInput<DateTime, FechaValidationError> {
  FechaInput.pure() : super.pure(DateTime.now());
  FechaInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaValidationError validator(DateTime value) {
    return null;
  }
}

enum DescripcionValidationError { invalid }
class DescripcionInput extends FormzInput<String, DescripcionValidationError> {
  const DescripcionInput.pure() : super.pure('');
  const DescripcionInput.dirty([String value = '']) : super.dirty(value);

  @override
  DescripcionValidationError validator(String value) {
    return null;
  }
}

