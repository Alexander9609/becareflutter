import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/eventos/eventos_model.dart';
import 'package:movilRCV2/entities/eventos/eventos_repository.dart';
import 'package:movilRCV2/entities/eventos/bloc/eventos_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'eventos_events.dart';
part 'eventos_state.dart';

class EventosBloc extends Bloc<EventosEvent, EventosState> {
  final EventosRepository _eventosRepository;

  final fechaController = TextEditingController();
  final descripcionController = TextEditingController();

  EventosBloc({@required EventosRepository eventosRepository}) : assert(eventosRepository != null),
        _eventosRepository = eventosRepository, 
  super(EventosState(null,));

  @override
  void onTransition(Transition<EventosEvent, EventosState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<EventosState> mapEventToState(EventosEvent event) async* {
    if (event is InitEventosList) {
      yield* onInitList(event);
    } else if (event is EventosFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadEventosByIdForEdit) {
      yield* onLoadEventosIdForEdit(event);
    } else if (event is DeleteEventosById) {
      yield* onDeleteEventosId(event);
    } else if (event is LoadEventosByIdForView) {
      yield* onLoadEventosIdForView(event);
    }else if (event is FechaChanged){
      yield* onFechaChange(event);
    }else if (event is DescripcionChanged){
      yield* onDescripcionChange(event);
    }  }

  Stream<EventosState> onInitList(InitEventosList event) async* {
    yield this.state.copyWith(eventosStatusUI: EventosStatusUI.loading);
    List<Eventos> eventos = await _eventosRepository.getAllEventos();
    yield this.state.copyWith(eventos: eventos, eventosStatusUI: EventosStatusUI.done);
  }

  Stream<EventosState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Eventos result;
        if(this.state.editMode) {
          Eventos newEventos = Eventos(state.loadedEventos.id,
            this.state.fecha.value,  
            this.state.descripcion.value,  
            null, 
          );

          result = await _eventosRepository.update(newEventos);
        } else {
          Eventos newEventos = Eventos(null,
            this.state.fecha.value,  
            this.state.descripcion.value,  
            null, 
          );

          result = await _eventosRepository.create(newEventos);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<EventosState> onLoadEventosIdForEdit(LoadEventosByIdForEdit event) async* {
    yield this.state.copyWith(eventosStatusUI: EventosStatusUI.loading);
    Eventos loadedEventos = await _eventosRepository.getEventos(event.id);

    final fecha = FechaInput.dirty(loadedEventos?.fecha != null ? loadedEventos.fecha: null);
    final descripcion = DescripcionInput.dirty(loadedEventos?.descripcion != null ? loadedEventos.descripcion: '');

    yield this.state.copyWith(loadedEventos: loadedEventos, editMode: true,
      fecha: fecha,
      descripcion: descripcion,
    eventosStatusUI: EventosStatusUI.done);

    fechaController.text = DateFormat.yMMMMd('en').format(loadedEventos?.fecha);
    descripcionController.text = loadedEventos.descripcion;
  }

  Stream<EventosState> onDeleteEventosId(DeleteEventosById event) async* {
    try {
      await _eventosRepository.delete(event.id);
      this.add(InitEventosList());
      yield this.state.copyWith(deleteStatus: EventosDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: EventosDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: EventosDeleteStatus.none);
  }

  Stream<EventosState> onLoadEventosIdForView(LoadEventosByIdForView event) async* {
    yield this.state.copyWith(eventosStatusUI: EventosStatusUI.loading);
    try {
      Eventos loadedEventos = await _eventosRepository.getEventos(event.id);
      yield this.state.copyWith(loadedEventos: loadedEventos, eventosStatusUI: EventosStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedEventos: null, eventosStatusUI: EventosStatusUI.error);
    }
  }


  Stream<EventosState> onFechaChange(FechaChanged event) async* {
    final fecha = FechaInput.dirty(event.fecha);
    yield this.state.copyWith(
      fecha: fecha,
      formStatus: Formz.validate([
        fecha,
      this.state.descripcion,
      ]),
    );
  }
  Stream<EventosState> onDescripcionChange(DescripcionChanged event) async* {
    final descripcion = DescripcionInput.dirty(event.descripcion);
    yield this.state.copyWith(
      descripcion: descripcion,
      formStatus: Formz.validate([
      this.state.fecha,
        descripcion,
      ]),
    );
  }

  @override
  Future<void> close() {
    fechaController.dispose();
    descripcionController.dispose();
    return super.close();
  }

}