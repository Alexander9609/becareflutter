part of 'eventos_bloc.dart';

enum EventosStatusUI {init, loading, error, done}
enum EventosDeleteStatus {ok, ko, none}

class EventosState extends Equatable {
  final List<Eventos> eventos;
  final Eventos loadedEventos;
  final bool editMode;
  final EventosDeleteStatus deleteStatus;
  final EventosStatusUI eventosStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final FechaInput fecha;
  final DescripcionInput descripcion;

  
  EventosState(
FechaInput fecha,{
    this.eventos = const [],
    this.eventosStatusUI = EventosStatusUI.init,
    this.loadedEventos = const Eventos(0,null,'',null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = EventosDeleteStatus.none,
    this.descripcion = const DescripcionInput.pure(),
  }):this.fecha = fecha ?? FechaInput.pure()
;

  EventosState copyWith({
    List<Eventos> eventos,
    EventosStatusUI eventosStatusUI,
    bool editMode,
    EventosDeleteStatus deleteStatus,
    Eventos loadedEventos,
    FormzStatus formStatus,
    String generalNotificationKey,
    FechaInput fecha,
    DescripcionInput descripcion,
  }) {
    return EventosState(
        fecha,
      eventos: eventos ?? this.eventos,
      eventosStatusUI: eventosStatusUI ?? this.eventosStatusUI,
      loadedEventos: loadedEventos ?? this.loadedEventos,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      descripcion: descripcion ?? this.descripcion,
    );
  }

  @override
  List<Object> get props => [eventos, eventosStatusUI,
     loadedEventos, editMode, deleteStatus, formStatus, generalNotificationKey, 
fecha,descripcion,];

  @override
  bool get stringify => true;
}
