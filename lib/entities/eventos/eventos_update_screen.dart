import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/eventos/bloc/eventos_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/eventos/eventos_model.dart';

class EventosUpdateScreen extends StatelessWidget {
  EventosUpdateScreen({Key key}) : super(key: MovilRcv2Keys.eventosCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<EventosBloc, EventosState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesEventosList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<EventosBloc, EventosState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Eventos':
'Create Eventos';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          fechaField(),
          descripcionField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget fechaField() {
        return BlocBuilder<EventosBloc, EventosState>(
            buildWhen: (previous, current) => previous.fecha != current.fecha,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<EventosBloc>().fechaController,
                onChanged: (value) { context.bloc<EventosBloc>().add(FechaChanged(fecha: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'fecha',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }
      Widget descripcionField() {
        return BlocBuilder<EventosBloc, EventosState>(
            buildWhen: (previous, current) => previous.descripcion != current.descripcion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<EventosBloc>().descripcionController,
                  onChanged: (value) { context.bloc<EventosBloc>()
                    .add(DescripcionChanged(descripcion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'descripcion'));
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<EventosBloc, EventosState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(EventosState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<EventosBloc, EventosState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<EventosBloc>().add(EventosFormSubmitted()) : null,
          );
        }
    );
  }
}
