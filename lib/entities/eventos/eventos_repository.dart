import 'package:movilRCV2/entities/eventos/eventos_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class EventosRepository {
    EventosRepository();
  
  static final String uriEndpoint = '/eventos';

  Future<List<Eventos>> getAllEventos() async {
    final allEventosRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Eventos>>(allEventosRequest.body);
  }

  Future<Eventos> getEventos(int id) async {
    final eventosRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Eventos>(eventosRequest.body);
  }

  Future<Eventos> create(Eventos eventos) async {
    final eventosRequest = await HttpUtils.postRequest('$uriEndpoint', eventos);
    return JsonMapper.deserialize<Eventos>(eventosRequest.body);
  }

  Future<Eventos> update(Eventos eventos) async {
    final eventosRequest = await HttpUtils.putRequest('$uriEndpoint', eventos);
    return JsonMapper.deserialize<Eventos>(eventosRequest.body);
  }

  Future<void> delete(int id) async {
    final eventosRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
