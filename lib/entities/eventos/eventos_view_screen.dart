import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/eventos/bloc/eventos_bloc.dart';
import 'package:movilRCV2/entities/eventos/eventos_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class EventosViewScreen extends StatelessWidget {
  EventosViewScreen({Key key}) : super(key: MovilRcv2Keys.eventosCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Eventos View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<EventosBloc, EventosState>(
              buildWhen: (previous, current) => previous.loadedEventos != current.loadedEventos,
              builder: (context, state) {
                return Visibility(
                  visible: state.eventosStatusUI == EventosStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    eventosCard(state.loadedEventos, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEventosCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget eventosCard(Eventos eventos, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + eventos.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha : ' + (eventos?.fecha != null ? DateFormat.yMMMMd('en').format(eventos.fecha) : ''), style: Theme.of(context).textTheme.bodyText1,),
                Text('Descripcion : ' + eventos.descripcion.toString(), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
