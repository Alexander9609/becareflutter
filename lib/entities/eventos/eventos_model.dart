import 'package:dart_json_mapper/dart_json_mapper.dart';

import '../dispositivos/dispositivos_model.dart';

@jsonSerializable
class Eventos {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'fecha', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fecha;

  @JsonProperty(name: 'descripcion')
  final String descripcion;

  @JsonProperty(name: 'dispositivo')
  final Dispositivos dispositivo;
        
 const Eventos (
     this.id,
        this.fecha,
        this.descripcion,
        this.dispositivo,
    );

@override
String toString() {
    return 'Eventos{'+
    'id: $id,' +
        'fecha: $fecha,' +
        'descripcion: $descripcion,' +
        'dispositivo: $dispositivo,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Eventos &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


