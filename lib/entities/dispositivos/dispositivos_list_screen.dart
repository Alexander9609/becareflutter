import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/dispositivos/bloc/dispositivos_bloc.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class DispositivosListScreen extends StatelessWidget {
    DispositivosListScreen({Key key}) : super(key: MovilRcv2Keys.dispositivosListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<DispositivosBloc, DispositivosState>(
      listener: (context, state) {
        if(state.deleteStatus == DispositivosDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('Dispositivos deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('Dispositivos List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<DispositivosBloc, DispositivosState>(
              buildWhen: (previous, current) => previous.dispositivos != current.dispositivos,
              builder: (context, state) {
                return Visibility(
                  visible: state.dispositivosStatusUI == DispositivosStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (Dispositivos dispositivos in state.dispositivos) dispositivosCard(dispositivos, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesDispositivosCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget dispositivosCard(Dispositivos dispositivos, BuildContext context) {
    DispositivosBloc dispositivosBloc = BlocProvider.of<DispositivosBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Mac : ${dispositivos.mac.toString()}'),
                  subtitle: Text('Modelo : ${dispositivos.modelo.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesDispositivosView,
                    arguments: EntityArguments(dispositivos.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesDispositivosEdit,
                    arguments: EntityArguments(dispositivos.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(dispositivosBloc, context, dispositivos.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(DispositivosBloc dispositivosBloc, BuildContext context, int id) {
    return BlocProvider<DispositivosBloc>.value(
      value: dispositivosBloc,
      child: AlertDialog(
        title: new Text('Delete Dispositivos'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              dispositivosBloc.add(DeleteDispositivosById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
