import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/dispositivos/bloc/dispositivos_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';

class DispositivosUpdateScreen extends StatelessWidget {
  DispositivosUpdateScreen({Key key}) : super(key: MovilRcv2Keys.dispositivosCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<DispositivosBloc, DispositivosState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesDispositivosList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<DispositivosBloc, DispositivosState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Dispositivos':
'Create Dispositivos';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          macField(),
          modeloField(),
          conectadoField(),
          fechaInicioField(),
          fechaFinaField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget macField() {
        return BlocBuilder<DispositivosBloc, DispositivosState>(
            buildWhen: (previous, current) => previous.mac != current.mac,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<DispositivosBloc>().macController,
                  onChanged: (value) { context.bloc<DispositivosBloc>()
                    .add(MacChanged(mac:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'mac'));
            }
        );
      }
      Widget modeloField() {
        return BlocBuilder<DispositivosBloc, DispositivosState>(
            buildWhen: (previous, current) => previous.modelo != current.modelo,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<DispositivosBloc>().modeloController,
                  onChanged: (value) { context.bloc<DispositivosBloc>()
                    .add(ModeloChanged(modelo:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'modelo'));
            }
        );
      }
        Widget conectadoField() {
          return BlocBuilder<DispositivosBloc,DispositivosState>(
              buildWhen: (previous, current) => previous.conectado != current.conectado,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('conectado', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.conectado.value,
                          onChanged: (value) { context.bloc<DispositivosBloc>().add(ConectadoChanged(conectado: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
      Widget fechaInicioField() {
        return BlocBuilder<DispositivosBloc, DispositivosState>(
            buildWhen: (previous, current) => previous.fechaInicio != current.fechaInicio,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<DispositivosBloc>().fechaInicioController,
                onChanged: (value) { context.bloc<DispositivosBloc>().add(FechaInicioChanged(fechaInicio: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'fechaInicio',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }
      Widget fechaFinaField() {
        return BlocBuilder<DispositivosBloc, DispositivosState>(
            buildWhen: (previous, current) => previous.fechaFina != current.fechaFina,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<DispositivosBloc>().fechaFinaController,
                onChanged: (value) { context.bloc<DispositivosBloc>().add(FechaFinaChanged(fechaFina: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'fechaFina',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<DispositivosBloc, DispositivosState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(DispositivosState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<DispositivosBloc, DispositivosState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<DispositivosBloc>().add(DispositivosFormSubmitted()) : null,
          );
        }
    );
  }
}
