import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/dispositivos/bloc/dispositivos_bloc.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class DispositivosViewScreen extends StatelessWidget {
  DispositivosViewScreen({Key key}) : super(key: MovilRcv2Keys.dispositivosCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Dispositivos View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<DispositivosBloc, DispositivosState>(
              buildWhen: (previous, current) => previous.loadedDispositivos != current.loadedDispositivos,
              builder: (context, state) {
                return Visibility(
                  visible: state.dispositivosStatusUI == DispositivosStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    dispositivosCard(state.loadedDispositivos, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesDispositivosCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget dispositivosCard(Dispositivos dispositivos, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + dispositivos.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Mac : ' + dispositivos.mac.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Modelo : ' + dispositivos.modelo.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Conectado : ' + dispositivos.conectado.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha Inicio : ' + (dispositivos?.fechaInicio != null ? DateFormat.yMMMMd('en').format(dispositivos.fechaInicio) : ''), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha Fina : ' + (dispositivos?.fechaFina != null ? DateFormat.yMMMMd('en').format(dispositivos.fechaFina) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
