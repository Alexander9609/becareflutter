import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:movilRCV2/shared/models/user.dart';

@jsonSerializable
class Dispositivos {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'mac')
  final String mac;

  @JsonProperty(name: 'modelo')
  final String modelo;

  @JsonProperty(name: 'conectado')
  final bool conectado;

  @JsonProperty(name: 'fechaInicio', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fechaInicio;

  @JsonProperty(name: 'fechaFina', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fechaFina;

  @JsonProperty(name: 'user')
  final User user;
        
 const Dispositivos (
     this.id,
        this.mac,
        this.modelo,
        this.conectado,
        this.fechaInicio,
        this.fechaFina,
        this.user,
    );

@override
String toString() {
    return 'Dispositivos{'+
    'id: $id,' +
        'mac: $mac,' +
        'modelo: $modelo,' +
        'conectado: $conectado,' +
        'fechaInicio: $fechaInicio,' +
        'fechaFina: $fechaFina,' +
        'user: $user,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Dispositivos &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


