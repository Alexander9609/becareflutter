import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class DispositivosRepository {
    DispositivosRepository();
  
  static final String uriEndpoint = '/dispositivos';

  Future<List<Dispositivos>> getAllDispositivos() async {
    final allDispositivosRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Dispositivos>>(allDispositivosRequest.body);
  }

  Future<Dispositivos> getDispositivos(int id) async {
    final dispositivosRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Dispositivos>(dispositivosRequest.body);
  }

  Future<Dispositivos> create(Dispositivos dispositivos) async {
    final dispositivosRequest = await HttpUtils.postRequest('$uriEndpoint', dispositivos);
    return JsonMapper.deserialize<Dispositivos>(dispositivosRequest.body);
  }

  Future<Dispositivos> update(Dispositivos dispositivos) async {
    final dispositivosRequest = await HttpUtils.putRequest('$uriEndpoint', dispositivos);
    return JsonMapper.deserialize<Dispositivos>(dispositivosRequest.body);
  }

  Future<void> delete(int id) async {
    final dispositivosRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
