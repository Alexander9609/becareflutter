part of 'dispositivos_bloc.dart';

enum DispositivosStatusUI {init, loading, error, done}
enum DispositivosDeleteStatus {ok, ko, none}

class DispositivosState extends Equatable {
  final List<Dispositivos> dispositivos;
  final Dispositivos loadedDispositivos;
  final bool editMode;
  final DispositivosDeleteStatus deleteStatus;
  final DispositivosStatusUI dispositivosStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final MacInput mac;
  final ModeloInput modelo;
  final ConectadoInput conectado;
  final FechaInicioInput fechaInicio;
  final FechaFinaInput fechaFina;

  
  DispositivosState(
FechaInicioInput fechaInicio,FechaFinaInput fechaFina,{
    this.dispositivos = const [],
    this.dispositivosStatusUI = DispositivosStatusUI.init,
    this.loadedDispositivos = const Dispositivos(0,'','',false,null,null,null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = DispositivosDeleteStatus.none,
    this.mac = const MacInput.pure(),
    this.modelo = const ModeloInput.pure(),
    this.conectado = const ConectadoInput.pure(),
  }):this.fechaInicio = fechaInicio ?? FechaInicioInput.pure(),
this.fechaFina = fechaFina ?? FechaFinaInput.pure()
;

  DispositivosState copyWith({
    List<Dispositivos> dispositivos,
    DispositivosStatusUI dispositivosStatusUI,
    bool editMode,
    DispositivosDeleteStatus deleteStatus,
    Dispositivos loadedDispositivos,
    FormzStatus formStatus,
    String generalNotificationKey,
    MacInput mac,
    ModeloInput modelo,
    ConectadoInput conectado,
    FechaInicioInput fechaInicio,
    FechaFinaInput fechaFina,
  }) {
    return DispositivosState(
        fechaInicio,
        fechaFina,
      dispositivos: dispositivos ?? this.dispositivos,
      dispositivosStatusUI: dispositivosStatusUI ?? this.dispositivosStatusUI,
      loadedDispositivos: loadedDispositivos ?? this.loadedDispositivos,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      mac: mac ?? this.mac,
      modelo: modelo ?? this.modelo,
      conectado: conectado ?? this.conectado,
    );
  }

  @override
  List<Object> get props => [dispositivos, dispositivosStatusUI,
     loadedDispositivos, editMode, deleteStatus, formStatus, generalNotificationKey, 
mac,modelo,conectado,fechaInicio,fechaFina,];

  @override
  bool get stringify => true;
}
