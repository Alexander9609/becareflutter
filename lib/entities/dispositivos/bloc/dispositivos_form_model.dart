import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';

enum MacValidationError { invalid }
class MacInput extends FormzInput<String, MacValidationError> {
  const MacInput.pure() : super.pure('');
  const MacInput.dirty([String value = '']) : super.dirty(value);

  @override
  MacValidationError validator(String value) {
    return null;
  }
}

enum ModeloValidationError { invalid }
class ModeloInput extends FormzInput<String, ModeloValidationError> {
  const ModeloInput.pure() : super.pure('');
  const ModeloInput.dirty([String value = '']) : super.dirty(value);

  @override
  ModeloValidationError validator(String value) {
    return null;
  }
}

enum ConectadoValidationError { invalid }
class ConectadoInput extends FormzInput<bool, ConectadoValidationError> {
  const ConectadoInput.pure() : super.pure(false);
  const ConectadoInput.dirty([bool value = false]) : super.dirty(value);

  @override
  ConectadoValidationError validator(bool value) {
    return null;
  }
}

enum FechaInicioValidationError { invalid }
class FechaInicioInput extends FormzInput<DateTime, FechaInicioValidationError> {
  FechaInicioInput.pure() : super.pure(DateTime.now());
  FechaInicioInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaInicioValidationError validator(DateTime value) {
    return null;
  }
}

enum FechaFinaValidationError { invalid }
class FechaFinaInput extends FormzInput<DateTime, FechaFinaValidationError> {
  FechaFinaInput.pure() : super.pure(DateTime.now());
  FechaFinaInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaFinaValidationError validator(DateTime value) {
    return null;
  }
}

