part of 'dispositivos_bloc.dart';

abstract class DispositivosEvent extends Equatable {
  const DispositivosEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitDispositivosList extends DispositivosEvent {}

class MacChanged extends DispositivosEvent {
  final String mac;
  
  const MacChanged({@required this.mac});
  
  @override
  List<Object> get props => [mac];
}
class ModeloChanged extends DispositivosEvent {
  final String modelo;
  
  const ModeloChanged({@required this.modelo});
  
  @override
  List<Object> get props => [modelo];
}
class ConectadoChanged extends DispositivosEvent {
  final bool conectado;
  
  const ConectadoChanged({@required this.conectado});
  
  @override
  List<Object> get props => [conectado];
}
class FechaInicioChanged extends DispositivosEvent {
  final DateTime fechaInicio;
  
  const FechaInicioChanged({@required this.fechaInicio});
  
  @override
  List<Object> get props => [fechaInicio];
}
class FechaFinaChanged extends DispositivosEvent {
  final DateTime fechaFina;
  
  const FechaFinaChanged({@required this.fechaFina});
  
  @override
  List<Object> get props => [fechaFina];
}

class DispositivosFormSubmitted extends DispositivosEvent {}

class LoadDispositivosByIdForEdit extends DispositivosEvent {
  final int id;

  const LoadDispositivosByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteDispositivosById extends DispositivosEvent {
  final int id;

  const DeleteDispositivosById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadDispositivosByIdForView extends DispositivosEvent {
  final int id;

  const LoadDispositivosByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
