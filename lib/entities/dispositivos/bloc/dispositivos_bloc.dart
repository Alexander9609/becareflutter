import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/dispositivos/dispositivos_model.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_repository.dart';
import 'package:movilRCV2/entities/dispositivos/bloc/dispositivos_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'dispositivos_events.dart';
part 'dispositivos_state.dart';

class DispositivosBloc extends Bloc<DispositivosEvent, DispositivosState> {
  final DispositivosRepository _dispositivosRepository;

  final macController = TextEditingController();
  final modeloController = TextEditingController();
  final fechaInicioController = TextEditingController();
  final fechaFinaController = TextEditingController();

  DispositivosBloc({@required DispositivosRepository dispositivosRepository}) : assert(dispositivosRepository != null),
        _dispositivosRepository = dispositivosRepository, 
  super(DispositivosState(null,null,));

  @override
  void onTransition(Transition<DispositivosEvent, DispositivosState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<DispositivosState> mapEventToState(DispositivosEvent event) async* {
    if (event is InitDispositivosList) {
      yield* onInitList(event);
    } else if (event is DispositivosFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadDispositivosByIdForEdit) {
      yield* onLoadDispositivosIdForEdit(event);
    } else if (event is DeleteDispositivosById) {
      yield* onDeleteDispositivosId(event);
    } else if (event is LoadDispositivosByIdForView) {
      yield* onLoadDispositivosIdForView(event);
    }else if (event is MacChanged){
      yield* onMacChange(event);
    }else if (event is ModeloChanged){
      yield* onModeloChange(event);
    }else if (event is ConectadoChanged){
      yield* onConectadoChange(event);
    }else if (event is FechaInicioChanged){
      yield* onFechaInicioChange(event);
    }else if (event is FechaFinaChanged){
      yield* onFechaFinaChange(event);
    }  }

  Stream<DispositivosState> onInitList(InitDispositivosList event) async* {
    yield this.state.copyWith(dispositivosStatusUI: DispositivosStatusUI.loading);
    List<Dispositivos> dispositivos = await _dispositivosRepository.getAllDispositivos();
    yield this.state.copyWith(dispositivos: dispositivos, dispositivosStatusUI: DispositivosStatusUI.done);
  }

  Stream<DispositivosState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Dispositivos result;
        if(this.state.editMode) {
          Dispositivos newDispositivos = Dispositivos(state.loadedDispositivos.id,
            this.state.mac.value,  
            this.state.modelo.value,  
            this.state.conectado.value,  
            this.state.fechaInicio.value,  
            this.state.fechaFina.value,  
            null, 
          );

          result = await _dispositivosRepository.update(newDispositivos);
        } else {
          Dispositivos newDispositivos = Dispositivos(null,
            this.state.mac.value,  
            this.state.modelo.value,  
            this.state.conectado.value,  
            this.state.fechaInicio.value,  
            this.state.fechaFina.value,  
            null, 
          );

          result = await _dispositivosRepository.create(newDispositivos);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<DispositivosState> onLoadDispositivosIdForEdit(LoadDispositivosByIdForEdit event) async* {
    yield this.state.copyWith(dispositivosStatusUI: DispositivosStatusUI.loading);
    Dispositivos loadedDispositivos = await _dispositivosRepository.getDispositivos(event.id);

    final mac = MacInput.dirty(loadedDispositivos?.mac != null ? loadedDispositivos.mac: '');
    final modelo = ModeloInput.dirty(loadedDispositivos?.modelo != null ? loadedDispositivos.modelo: '');
    final conectado = ConectadoInput.dirty(loadedDispositivos?.conectado != null ? loadedDispositivos.conectado: false);
    final fechaInicio = FechaInicioInput.dirty(loadedDispositivos?.fechaInicio != null ? loadedDispositivos.fechaInicio: null);
    final fechaFina = FechaFinaInput.dirty(loadedDispositivos?.fechaFina != null ? loadedDispositivos.fechaFina: null);

    yield this.state.copyWith(loadedDispositivos: loadedDispositivos, editMode: true,
      mac: mac,
      modelo: modelo,
      conectado: conectado,
      fechaInicio: fechaInicio,
      fechaFina: fechaFina,
    dispositivosStatusUI: DispositivosStatusUI.done);

    macController.text = loadedDispositivos.mac;
    modeloController.text = loadedDispositivos.modelo;
    fechaInicioController.text = DateFormat.yMMMMd('en').format(loadedDispositivos?.fechaInicio);
    fechaFinaController.text = DateFormat.yMMMMd('en').format(loadedDispositivos?.fechaFina);
  }

  Stream<DispositivosState> onDeleteDispositivosId(DeleteDispositivosById event) async* {
    try {
      await _dispositivosRepository.delete(event.id);
      this.add(InitDispositivosList());
      yield this.state.copyWith(deleteStatus: DispositivosDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: DispositivosDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: DispositivosDeleteStatus.none);
  }

  Stream<DispositivosState> onLoadDispositivosIdForView(LoadDispositivosByIdForView event) async* {
    yield this.state.copyWith(dispositivosStatusUI: DispositivosStatusUI.loading);
    try {
      Dispositivos loadedDispositivos = await _dispositivosRepository.getDispositivos(event.id);
      yield this.state.copyWith(loadedDispositivos: loadedDispositivos, dispositivosStatusUI: DispositivosStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedDispositivos: null, dispositivosStatusUI: DispositivosStatusUI.error);
    }
  }


  Stream<DispositivosState> onMacChange(MacChanged event) async* {
    final mac = MacInput.dirty(event.mac);
    yield this.state.copyWith(
      mac: mac,
      formStatus: Formz.validate([
        mac,
      this.state.modelo,
      this.state.conectado,
      this.state.fechaInicio,
      this.state.fechaFina,
      ]),
    );
  }
  Stream<DispositivosState> onModeloChange(ModeloChanged event) async* {
    final modelo = ModeloInput.dirty(event.modelo);
    yield this.state.copyWith(
      modelo: modelo,
      formStatus: Formz.validate([
      this.state.mac,
        modelo,
      this.state.conectado,
      this.state.fechaInicio,
      this.state.fechaFina,
      ]),
    );
  }
  Stream<DispositivosState> onConectadoChange(ConectadoChanged event) async* {
    final conectado = ConectadoInput.dirty(event.conectado);
    yield this.state.copyWith(
      conectado: conectado,
      formStatus: Formz.validate([
      this.state.mac,
      this.state.modelo,
        conectado,
      this.state.fechaInicio,
      this.state.fechaFina,
      ]),
    );
  }
  Stream<DispositivosState> onFechaInicioChange(FechaInicioChanged event) async* {
    final fechaInicio = FechaInicioInput.dirty(event.fechaInicio);
    yield this.state.copyWith(
      fechaInicio: fechaInicio,
      formStatus: Formz.validate([
      this.state.mac,
      this.state.modelo,
      this.state.conectado,
        fechaInicio,
      this.state.fechaFina,
      ]),
    );
  }
  Stream<DispositivosState> onFechaFinaChange(FechaFinaChanged event) async* {
    final fechaFina = FechaFinaInput.dirty(event.fechaFina);
    yield this.state.copyWith(
      fechaFina: fechaFina,
      formStatus: Formz.validate([
      this.state.mac,
      this.state.modelo,
      this.state.conectado,
      this.state.fechaInicio,
        fechaFina,
      ]),
    );
  }

  @override
  Future<void> close() {
    macController.dispose();
    modeloController.dispose();
    fechaInicioController.dispose();
    fechaFinaController.dispose();
    return super.close();
  }

}