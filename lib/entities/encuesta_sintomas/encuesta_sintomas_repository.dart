import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class EncuestaSintomasRepository {
    EncuestaSintomasRepository();
  
  static final String uriEndpoint = '/encuesta-sintomas';

  Future<List<EncuestaSintomas>> getAllEncuestaSintomas() async {
    final allEncuestaSintomasRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<EncuestaSintomas>>(allEncuestaSintomasRequest.body);
  }

  Future<EncuestaSintomas> getEncuestaSintomas(int id) async {
    final encuestaSintomasRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<EncuestaSintomas>(encuestaSintomasRequest.body);
  }

  Future<EncuestaSintomas> create(EncuestaSintomas encuestaSintomas) async {
    final encuestaSintomasRequest = await HttpUtils.postRequest('$uriEndpoint', encuestaSintomas);
    return JsonMapper.deserialize<EncuestaSintomas>(encuestaSintomasRequest.body);
  }

  Future<EncuestaSintomas> update(EncuestaSintomas encuestaSintomas) async {
    final encuestaSintomasRequest = await HttpUtils.putRequest('$uriEndpoint', encuestaSintomas);
    return JsonMapper.deserialize<EncuestaSintomas>(encuestaSintomasRequest.body);
  }

  Future<void> delete(int id) async {
    final encuestaSintomasRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
