import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/bloc/encuesta_sintomas_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';

class EncuestaSintomasUpdateScreen extends StatelessWidget {
  EncuestaSintomasUpdateScreen({Key key}) : super(key: MovilRcv2Keys.encuestaSintomasCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<EncuestaSintomasBloc, EncuestaSintomasState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesEncuestaSintomasList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit EncuestaSintomas':
'Create EncuestaSintomas';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          dolorDeCabezaField(),
          mareoField(),
          palpitacionesField(),
          fatigaField(),
          escalofrioField(),
          dolorMuscularField(),
          ningunoField(),
          fechaField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

        Widget dolorDeCabezaField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.dolorDeCabeza != current.dolorDeCabeza,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('dolorDeCabeza', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.dolorDeCabeza.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(DolorDeCabezaChanged(dolorDeCabeza: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget mareoField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.mareo != current.mareo,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('mareo', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.mareo.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(MareoChanged(mareo: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget palpitacionesField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.palpitaciones != current.palpitaciones,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('palpitaciones', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.palpitaciones.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(PalpitacionesChanged(palpitaciones: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget fatigaField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.fatiga != current.fatiga,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('fatiga', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.fatiga.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(FatigaChanged(fatiga: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget escalofrioField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.escalofrio != current.escalofrio,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('escalofrio', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.escalofrio.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(EscalofrioChanged(escalofrio: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget dolorMuscularField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.dolorMuscular != current.dolorMuscular,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('dolorMuscular', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.dolorMuscular.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(DolorMuscularChanged(dolorMuscular: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
        Widget ningunoField() {
          return BlocBuilder<EncuestaSintomasBloc,EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.ninguno != current.ninguno,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('ninguno', style: Theme.of(context).textTheme.bodyText1,),
                      Switch(
                          value: state.ninguno.value,
                          onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(NingunoChanged(ninguno: value)); },
                          activeColor: Theme.of(context).primaryColor,),
                    ],
                  ),
                );
              });
        }
      Widget fechaField() {
        return BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
            buildWhen: (previous, current) => previous.fecha != current.fecha,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<EncuestaSintomasBloc>().fechaController,
                onChanged: (value) { context.bloc<EncuestaSintomasBloc>().add(FechaChanged(fecha: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'fecha',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(EncuestaSintomasState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<EncuestaSintomasBloc>().add(EncuestaSintomasFormSubmitted()) : null,
          );
        }
    );
  }
}
