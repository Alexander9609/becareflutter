import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/bloc/encuesta_sintomas_bloc.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class EncuestaSintomasViewScreen extends StatelessWidget {
  EncuestaSintomasViewScreen({Key key}) : super(key: MovilRcv2Keys.encuestaSintomasCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('EncuestaSintomas View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.loadedEncuestaSintomas != current.loadedEncuestaSintomas,
              builder: (context, state) {
                return Visibility(
                  visible: state.encuestaSintomasStatusUI == EncuestaSintomasStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    encuestaSintomasCard(state.loadedEncuestaSintomas, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEncuestaSintomasCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget encuestaSintomasCard(EncuestaSintomas encuestaSintomas, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + encuestaSintomas.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Dolor De Cabeza : ' + encuestaSintomas.dolorDeCabeza.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Mareo : ' + encuestaSintomas.mareo.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Palpitaciones : ' + encuestaSintomas.palpitaciones.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Fatiga : ' + encuestaSintomas.fatiga.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Escalofrio : ' + encuestaSintomas.escalofrio.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Dolor Muscular : ' + encuestaSintomas.dolorMuscular.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Ninguno : ' + encuestaSintomas.ninguno.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha : ' + (encuestaSintomas?.fecha != null ? DateFormat.yMMMMd('en').format(encuestaSintomas.fecha) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
