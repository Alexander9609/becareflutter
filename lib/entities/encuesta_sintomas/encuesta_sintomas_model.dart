import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'package:movilRCV2/shared/models/user.dart';

@jsonSerializable
class EncuestaSintomas {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'dolorDeCabeza')
  final bool dolorDeCabeza;

  @JsonProperty(name: 'mareo')
  final bool mareo;

  @JsonProperty(name: 'palpitaciones')
  final bool palpitaciones;

  @JsonProperty(name: 'fatiga')
  final bool fatiga;

  @JsonProperty(name: 'escalofrio')
  final bool escalofrio;

  @JsonProperty(name: 'dolorMuscular')
  final bool dolorMuscular;

  @JsonProperty(name: 'ninguno')
  final bool ninguno;

  @JsonProperty(name: 'fecha', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fecha;

  @JsonProperty(name: 'user')
  final User user;
        
 const EncuestaSintomas (
     this.id,
        this.dolorDeCabeza,
        this.mareo,
        this.palpitaciones,
        this.fatiga,
        this.escalofrio,
        this.dolorMuscular,
        this.ninguno,
        this.fecha,
        this.user,
    );

@override
String toString() {
    return 'EncuestaSintomas{'+
    'id: $id,' +
        'dolorDeCabeza: $dolorDeCabeza,' +
        'mareo: $mareo,' +
        'palpitaciones: $palpitaciones,' +
        'fatiga: $fatiga,' +
        'escalofrio: $escalofrio,' +
        'dolorMuscular: $dolorMuscular,' +
        'ninguno: $ninguno,' +
        'fecha: $fecha,' +
        'user: $user,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is EncuestaSintomas &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


