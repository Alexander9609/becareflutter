import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';

enum DolorDeCabezaValidationError { invalid }
class DolorDeCabezaInput extends FormzInput<bool, DolorDeCabezaValidationError> {
  const DolorDeCabezaInput.pure() : super.pure(false);
  const DolorDeCabezaInput.dirty([bool value = false]) : super.dirty(value);

  @override
  DolorDeCabezaValidationError validator(bool value) {
    return null;
  }
}

enum MareoValidationError { invalid }
class MareoInput extends FormzInput<bool, MareoValidationError> {
  const MareoInput.pure() : super.pure(false);
  const MareoInput.dirty([bool value = false]) : super.dirty(value);

  @override
  MareoValidationError validator(bool value) {
    return null;
  }
}

enum PalpitacionesValidationError { invalid }
class PalpitacionesInput extends FormzInput<bool, PalpitacionesValidationError> {
  const PalpitacionesInput.pure() : super.pure(false);
  const PalpitacionesInput.dirty([bool value = false]) : super.dirty(value);

  @override
  PalpitacionesValidationError validator(bool value) {
    return null;
  }
}

enum FatigaValidationError { invalid }
class FatigaInput extends FormzInput<bool, FatigaValidationError> {
  const FatigaInput.pure() : super.pure(false);
  const FatigaInput.dirty([bool value = false]) : super.dirty(value);

  @override
  FatigaValidationError validator(bool value) {
    return null;
  }
}

enum EscalofrioValidationError { invalid }
class EscalofrioInput extends FormzInput<bool, EscalofrioValidationError> {
  const EscalofrioInput.pure() : super.pure(false);
  const EscalofrioInput.dirty([bool value = false]) : super.dirty(value);

  @override
  EscalofrioValidationError validator(bool value) {
    return null;
  }
}

enum DolorMuscularValidationError { invalid }
class DolorMuscularInput extends FormzInput<bool, DolorMuscularValidationError> {
  const DolorMuscularInput.pure() : super.pure(false);
  const DolorMuscularInput.dirty([bool value = false]) : super.dirty(value);

  @override
  DolorMuscularValidationError validator(bool value) {
    return null;
  }
}

enum NingunoValidationError { invalid }
class NingunoInput extends FormzInput<bool, NingunoValidationError> {
  const NingunoInput.pure() : super.pure(false);
  const NingunoInput.dirty([bool value = false]) : super.dirty(value);

  @override
  NingunoValidationError validator(bool value) {
    return null;
  }
}

enum FechaValidationError { invalid }
class FechaInput extends FormzInput<DateTime, FechaValidationError> {
  FechaInput.pure() : super.pure(DateTime.now());
  FechaInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaValidationError validator(DateTime value) {
    return null;
  }
}

