part of 'encuesta_sintomas_bloc.dart';

abstract class EncuestaSintomasEvent extends Equatable {
  const EncuestaSintomasEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitEncuestaSintomasList extends EncuestaSintomasEvent {}

class DolorDeCabezaChanged extends EncuestaSintomasEvent {
  final bool dolorDeCabeza;
  
  const DolorDeCabezaChanged({@required this.dolorDeCabeza});
  
  @override
  List<Object> get props => [dolorDeCabeza];
}
class MareoChanged extends EncuestaSintomasEvent {
  final bool mareo;
  
  const MareoChanged({@required this.mareo});
  
  @override
  List<Object> get props => [mareo];
}
class PalpitacionesChanged extends EncuestaSintomasEvent {
  final bool palpitaciones;
  
  const PalpitacionesChanged({@required this.palpitaciones});
  
  @override
  List<Object> get props => [palpitaciones];
}
class FatigaChanged extends EncuestaSintomasEvent {
  final bool fatiga;
  
  const FatigaChanged({@required this.fatiga});
  
  @override
  List<Object> get props => [fatiga];
}
class EscalofrioChanged extends EncuestaSintomasEvent {
  final bool escalofrio;
  
  const EscalofrioChanged({@required this.escalofrio});
  
  @override
  List<Object> get props => [escalofrio];
}
class DolorMuscularChanged extends EncuestaSintomasEvent {
  final bool dolorMuscular;
  
  const DolorMuscularChanged({@required this.dolorMuscular});
  
  @override
  List<Object> get props => [dolorMuscular];
}
class NingunoChanged extends EncuestaSintomasEvent {
  final bool ninguno;
  
  const NingunoChanged({@required this.ninguno});
  
  @override
  List<Object> get props => [ninguno];
}
class FechaChanged extends EncuestaSintomasEvent {
  final DateTime fecha;
  
  const FechaChanged({@required this.fecha});
  
  @override
  List<Object> get props => [fecha];
}

class EncuestaSintomasFormSubmitted extends EncuestaSintomasEvent {}

class LoadEncuestaSintomasByIdForEdit extends EncuestaSintomasEvent {
  final int id;

  const LoadEncuestaSintomasByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteEncuestaSintomasById extends EncuestaSintomasEvent {
  final int id;

  const DeleteEncuestaSintomasById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadEncuestaSintomasByIdForView extends EncuestaSintomasEvent {
  final int id;

  const LoadEncuestaSintomasByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
