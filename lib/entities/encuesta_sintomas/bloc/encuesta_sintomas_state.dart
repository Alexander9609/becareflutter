part of 'encuesta_sintomas_bloc.dart';

enum EncuestaSintomasStatusUI {init, loading, error, done}
enum EncuestaSintomasDeleteStatus {ok, ko, none}

class EncuestaSintomasState extends Equatable {
  final List<EncuestaSintomas> encuestaSintomas;
  final EncuestaSintomas loadedEncuestaSintomas;
  final bool editMode;
  final EncuestaSintomasDeleteStatus deleteStatus;
  final EncuestaSintomasStatusUI encuestaSintomasStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final DolorDeCabezaInput dolorDeCabeza;
  final MareoInput mareo;
  final PalpitacionesInput palpitaciones;
  final FatigaInput fatiga;
  final EscalofrioInput escalofrio;
  final DolorMuscularInput dolorMuscular;
  final NingunoInput ninguno;
  final FechaInput fecha;

  
  EncuestaSintomasState(
FechaInput fecha,{
    this.encuestaSintomas = const [],
    this.encuestaSintomasStatusUI = EncuestaSintomasStatusUI.init,
    this.loadedEncuestaSintomas = const EncuestaSintomas(0,false,false,false,false,false,false,false,null,null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = EncuestaSintomasDeleteStatus.none,
    this.dolorDeCabeza = const DolorDeCabezaInput.pure(),
    this.mareo = const MareoInput.pure(),
    this.palpitaciones = const PalpitacionesInput.pure(),
    this.fatiga = const FatigaInput.pure(),
    this.escalofrio = const EscalofrioInput.pure(),
    this.dolorMuscular = const DolorMuscularInput.pure(),
    this.ninguno = const NingunoInput.pure(),
  }):this.fecha = fecha ?? FechaInput.pure()
;

  EncuestaSintomasState copyWith({
    List<EncuestaSintomas> encuestaSintomas,
    EncuestaSintomasStatusUI encuestaSintomasStatusUI,
    bool editMode,
    EncuestaSintomasDeleteStatus deleteStatus,
    EncuestaSintomas loadedEncuestaSintomas,
    FormzStatus formStatus,
    String generalNotificationKey,
    DolorDeCabezaInput dolorDeCabeza,
    MareoInput mareo,
    PalpitacionesInput palpitaciones,
    FatigaInput fatiga,
    EscalofrioInput escalofrio,
    DolorMuscularInput dolorMuscular,
    NingunoInput ninguno,
    FechaInput fecha,
  }) {
    return EncuestaSintomasState(
        fecha,
      encuestaSintomas: encuestaSintomas ?? this.encuestaSintomas,
      encuestaSintomasStatusUI: encuestaSintomasStatusUI ?? this.encuestaSintomasStatusUI,
      loadedEncuestaSintomas: loadedEncuestaSintomas ?? this.loadedEncuestaSintomas,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      dolorDeCabeza: dolorDeCabeza ?? this.dolorDeCabeza,
      mareo: mareo ?? this.mareo,
      palpitaciones: palpitaciones ?? this.palpitaciones,
      fatiga: fatiga ?? this.fatiga,
      escalofrio: escalofrio ?? this.escalofrio,
      dolorMuscular: dolorMuscular ?? this.dolorMuscular,
      ninguno: ninguno ?? this.ninguno,
    );
  }

  @override
  List<Object> get props => [encuestaSintomas, encuestaSintomasStatusUI,
     loadedEncuestaSintomas, editMode, deleteStatus, formStatus, generalNotificationKey, 
dolorDeCabeza,mareo,palpitaciones,fatiga,escalofrio,dolorMuscular,ninguno,fecha,];

  @override
  bool get stringify => true;
}
