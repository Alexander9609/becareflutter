import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_repository.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/bloc/encuesta_sintomas_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'encuesta_sintomas_events.dart';
part 'encuesta_sintomas_state.dart';

class EncuestaSintomasBloc extends Bloc<EncuestaSintomasEvent, EncuestaSintomasState> {
  final EncuestaSintomasRepository _encuestaSintomasRepository;

  final fechaController = TextEditingController();

  EncuestaSintomasBloc({@required EncuestaSintomasRepository encuestaSintomasRepository}) : assert(encuestaSintomasRepository != null),
        _encuestaSintomasRepository = encuestaSintomasRepository, 
  super(EncuestaSintomasState(null,));

  @override
  void onTransition(Transition<EncuestaSintomasEvent, EncuestaSintomasState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<EncuestaSintomasState> mapEventToState(EncuestaSintomasEvent event) async* {
    if (event is InitEncuestaSintomasList) {
      yield* onInitList(event);
    } else if (event is EncuestaSintomasFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadEncuestaSintomasByIdForEdit) {
      yield* onLoadEncuestaSintomasIdForEdit(event);
    } else if (event is DeleteEncuestaSintomasById) {
      yield* onDeleteEncuestaSintomasId(event);
    } else if (event is LoadEncuestaSintomasByIdForView) {
      yield* onLoadEncuestaSintomasIdForView(event);
    }else if (event is DolorDeCabezaChanged){
      yield* onDolorDeCabezaChange(event);
    }else if (event is MareoChanged){
      yield* onMareoChange(event);
    }else if (event is PalpitacionesChanged){
      yield* onPalpitacionesChange(event);
    }else if (event is FatigaChanged){
      yield* onFatigaChange(event);
    }else if (event is EscalofrioChanged){
      yield* onEscalofrioChange(event);
    }else if (event is DolorMuscularChanged){
      yield* onDolorMuscularChange(event);
    }else if (event is NingunoChanged){
      yield* onNingunoChange(event);
    }else if (event is FechaChanged){
      yield* onFechaChange(event);
    }  }

  Stream<EncuestaSintomasState> onInitList(InitEncuestaSintomasList event) async* {
    yield this.state.copyWith(encuestaSintomasStatusUI: EncuestaSintomasStatusUI.loading);
    List<EncuestaSintomas> encuestaSintomas = await _encuestaSintomasRepository.getAllEncuestaSintomas();
    yield this.state.copyWith(encuestaSintomas: encuestaSintomas, encuestaSintomasStatusUI: EncuestaSintomasStatusUI.done);
  }

  Stream<EncuestaSintomasState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        EncuestaSintomas result;
        if(this.state.editMode) {
          EncuestaSintomas newEncuestaSintomas = EncuestaSintomas(state.loadedEncuestaSintomas.id,
            this.state.dolorDeCabeza.value,  
            this.state.mareo.value,  
            this.state.palpitaciones.value,  
            this.state.fatiga.value,  
            this.state.escalofrio.value,  
            this.state.dolorMuscular.value,  
            this.state.ninguno.value,  
            this.state.fecha.value,  
            null, 
          );

          result = await _encuestaSintomasRepository.update(newEncuestaSintomas);
        } else {
          EncuestaSintomas newEncuestaSintomas = EncuestaSintomas(null,
            this.state.dolorDeCabeza.value,  
            this.state.mareo.value,  
            this.state.palpitaciones.value,  
            this.state.fatiga.value,  
            this.state.escalofrio.value,  
            this.state.dolorMuscular.value,  
            this.state.ninguno.value,  
            this.state.fecha.value,  
            null, 
          );

          result = await _encuestaSintomasRepository.create(newEncuestaSintomas);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<EncuestaSintomasState> onLoadEncuestaSintomasIdForEdit(LoadEncuestaSintomasByIdForEdit event) async* {
    yield this.state.copyWith(encuestaSintomasStatusUI: EncuestaSintomasStatusUI.loading);
    EncuestaSintomas loadedEncuestaSintomas = await _encuestaSintomasRepository.getEncuestaSintomas(event.id);

    final dolorDeCabeza = DolorDeCabezaInput.dirty(loadedEncuestaSintomas?.dolorDeCabeza != null ? loadedEncuestaSintomas.dolorDeCabeza: false);
    final mareo = MareoInput.dirty(loadedEncuestaSintomas?.mareo != null ? loadedEncuestaSintomas.mareo: false);
    final palpitaciones = PalpitacionesInput.dirty(loadedEncuestaSintomas?.palpitaciones != null ? loadedEncuestaSintomas.palpitaciones: false);
    final fatiga = FatigaInput.dirty(loadedEncuestaSintomas?.fatiga != null ? loadedEncuestaSintomas.fatiga: false);
    final escalofrio = EscalofrioInput.dirty(loadedEncuestaSintomas?.escalofrio != null ? loadedEncuestaSintomas.escalofrio: false);
    final dolorMuscular = DolorMuscularInput.dirty(loadedEncuestaSintomas?.dolorMuscular != null ? loadedEncuestaSintomas.dolorMuscular: false);
    final ninguno = NingunoInput.dirty(loadedEncuestaSintomas?.ninguno != null ? loadedEncuestaSintomas.ninguno: false);
    final fecha = FechaInput.dirty(loadedEncuestaSintomas?.fecha != null ? loadedEncuestaSintomas.fecha: null);

    yield this.state.copyWith(loadedEncuestaSintomas: loadedEncuestaSintomas, editMode: true,
      dolorDeCabeza: dolorDeCabeza,
      mareo: mareo,
      palpitaciones: palpitaciones,
      fatiga: fatiga,
      escalofrio: escalofrio,
      dolorMuscular: dolorMuscular,
      ninguno: ninguno,
      fecha: fecha,
    encuestaSintomasStatusUI: EncuestaSintomasStatusUI.done);

    fechaController.text = DateFormat.yMMMMd('en').format(loadedEncuestaSintomas?.fecha);
  }

  Stream<EncuestaSintomasState> onDeleteEncuestaSintomasId(DeleteEncuestaSintomasById event) async* {
    try {
      await _encuestaSintomasRepository.delete(event.id);
      this.add(InitEncuestaSintomasList());
      yield this.state.copyWith(deleteStatus: EncuestaSintomasDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: EncuestaSintomasDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: EncuestaSintomasDeleteStatus.none);
  }

  Stream<EncuestaSintomasState> onLoadEncuestaSintomasIdForView(LoadEncuestaSintomasByIdForView event) async* {
    yield this.state.copyWith(encuestaSintomasStatusUI: EncuestaSintomasStatusUI.loading);
    try {
      EncuestaSintomas loadedEncuestaSintomas = await _encuestaSintomasRepository.getEncuestaSintomas(event.id);
      yield this.state.copyWith(loadedEncuestaSintomas: loadedEncuestaSintomas, encuestaSintomasStatusUI: EncuestaSintomasStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedEncuestaSintomas: null, encuestaSintomasStatusUI: EncuestaSintomasStatusUI.error);
    }
  }


  Stream<EncuestaSintomasState> onDolorDeCabezaChange(DolorDeCabezaChanged event) async* {
    final dolorDeCabeza = DolorDeCabezaInput.dirty(event.dolorDeCabeza);
    yield this.state.copyWith(
      dolorDeCabeza: dolorDeCabeza,
      formStatus: Formz.validate([
        dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onMareoChange(MareoChanged event) async* {
    final mareo = MareoInput.dirty(event.mareo);
    yield this.state.copyWith(
      mareo: mareo,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
        mareo,
      this.state.palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onPalpitacionesChange(PalpitacionesChanged event) async* {
    final palpitaciones = PalpitacionesInput.dirty(event.palpitaciones);
    yield this.state.copyWith(
      palpitaciones: palpitaciones,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
        palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onFatigaChange(FatigaChanged event) async* {
    final fatiga = FatigaInput.dirty(event.fatiga);
    yield this.state.copyWith(
      fatiga: fatiga,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
        fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onEscalofrioChange(EscalofrioChanged event) async* {
    final escalofrio = EscalofrioInput.dirty(event.escalofrio);
    yield this.state.copyWith(
      escalofrio: escalofrio,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
      this.state.fatiga,
        escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onDolorMuscularChange(DolorMuscularChanged event) async* {
    final dolorMuscular = DolorMuscularInput.dirty(event.dolorMuscular);
    yield this.state.copyWith(
      dolorMuscular: dolorMuscular,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
        dolorMuscular,
      this.state.ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onNingunoChange(NingunoChanged event) async* {
    final ninguno = NingunoInput.dirty(event.ninguno);
    yield this.state.copyWith(
      ninguno: ninguno,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
        ninguno,
      this.state.fecha,
      ]),
    );
  }
  Stream<EncuestaSintomasState> onFechaChange(FechaChanged event) async* {
    final fecha = FechaInput.dirty(event.fecha);
    yield this.state.copyWith(
      fecha: fecha,
      formStatus: Formz.validate([
      this.state.dolorDeCabeza,
      this.state.mareo,
      this.state.palpitaciones,
      this.state.fatiga,
      this.state.escalofrio,
      this.state.dolorMuscular,
      this.state.ninguno,
        fecha,
      ]),
    );
  }

  @override
  Future<void> close() {
    fechaController.dispose();
    return super.close();
  }

}