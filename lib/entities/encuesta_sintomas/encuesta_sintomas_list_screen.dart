import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/bloc/encuesta_sintomas_bloc.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class EncuestaSintomasListScreen extends StatelessWidget {
    EncuestaSintomasListScreen({Key key}) : super(key: MovilRcv2Keys.encuestaSintomasListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<EncuestaSintomasBloc, EncuestaSintomasState>(
      listener: (context, state) {
        if(state.deleteStatus == EncuestaSintomasDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('EncuestaSintomas deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('EncuestaSintomas List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<EncuestaSintomasBloc, EncuestaSintomasState>(
              buildWhen: (previous, current) => previous.encuestaSintomas != current.encuestaSintomas,
              builder: (context, state) {
                return Visibility(
                  visible: state.encuestaSintomasStatusUI == EncuestaSintomasStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (EncuestaSintomas encuestaSintomas in state.encuestaSintomas) encuestaSintomasCard(encuestaSintomas, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEncuestaSintomasCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget encuestaSintomasCard(EncuestaSintomas encuestaSintomas, BuildContext context) {
    EncuestaSintomasBloc encuestaSintomasBloc = BlocProvider.of<EncuestaSintomasBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Dolor De Cabeza : ${encuestaSintomas.dolorDeCabeza.toString()}'),
                  subtitle: Text('Mareo : ${encuestaSintomas.mareo.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesEncuestaSintomasView,
                    arguments: EntityArguments(encuestaSintomas.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesEncuestaSintomasEdit,
                    arguments: EntityArguments(encuestaSintomas.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(encuestaSintomasBloc, context, encuestaSintomas.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(EncuestaSintomasBloc encuestaSintomasBloc, BuildContext context, int id) {
    return BlocProvider<EncuestaSintomasBloc>.value(
      value: encuestaSintomasBloc,
      child: AlertDialog(
        title: new Text('Delete EncuestaSintomas'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              encuestaSintomasBloc.add(DeleteEncuestaSintomasById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
