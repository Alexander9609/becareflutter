import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';

enum ClasificacionValidationError { invalid }
class ClasificacionInput extends FormzInput<String, ClasificacionValidationError> {
  const ClasificacionInput.pure() : super.pure('');
  const ClasificacionInput.dirty([String value = '']) : super.dirty(value);

  @override
  ClasificacionValidationError validator(String value) {
    return null;
  }
}

enum PresionArterialSistolicaValidationError { invalid }
class PresionArterialSistolicaInput extends FormzInput<String, PresionArterialSistolicaValidationError> {
  const PresionArterialSistolicaInput.pure() : super.pure('');
  const PresionArterialSistolicaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionArterialSistolicaValidationError validator(String value) {
    return null;
  }
}

enum PresionArterialDiastolicaValidationError { invalid }
class PresionArterialDiastolicaInput extends FormzInput<String, PresionArterialDiastolicaValidationError> {
  const PresionArterialDiastolicaInput.pure() : super.pure('');
  const PresionArterialDiastolicaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionArterialDiastolicaValidationError validator(String value) {
    return null;
  }
}

