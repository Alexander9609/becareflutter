part of 'estadio_presion_arterial_bloc.dart';

abstract class EstadioPresionArterialEvent extends Equatable {
  const EstadioPresionArterialEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitEstadioPresionArterialList extends EstadioPresionArterialEvent {}

class ClasificacionChanged extends EstadioPresionArterialEvent {
  final String clasificacion;
  
  const ClasificacionChanged({@required this.clasificacion});
  
  @override
  List<Object> get props => [clasificacion];
}
class PresionArterialSistolicaChanged extends EstadioPresionArterialEvent {
  final String presionArterialSistolica;
  
  const PresionArterialSistolicaChanged({@required this.presionArterialSistolica});
  
  @override
  List<Object> get props => [presionArterialSistolica];
}
class PresionArterialDiastolicaChanged extends EstadioPresionArterialEvent {
  final String presionArterialDiastolica;
  
  const PresionArterialDiastolicaChanged({@required this.presionArterialDiastolica});
  
  @override
  List<Object> get props => [presionArterialDiastolica];
}

class EstadioPresionArterialFormSubmitted extends EstadioPresionArterialEvent {}

class LoadEstadioPresionArterialByIdForEdit extends EstadioPresionArterialEvent {
  final int id;

  const LoadEstadioPresionArterialByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteEstadioPresionArterialById extends EstadioPresionArterialEvent {
  final int id;

  const DeleteEstadioPresionArterialById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadEstadioPresionArterialByIdForView extends EstadioPresionArterialEvent {
  final int id;

  const LoadEstadioPresionArterialByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
