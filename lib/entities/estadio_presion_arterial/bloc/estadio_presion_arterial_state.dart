part of 'estadio_presion_arterial_bloc.dart';

enum EstadioPresionArterialStatusUI {init, loading, error, done}
enum EstadioPresionArterialDeleteStatus {ok, ko, none}

class EstadioPresionArterialState extends Equatable {
  final List<EstadioPresionArterial> estadioPresionArterials;
  final EstadioPresionArterial loadedEstadioPresionArterial;
  final bool editMode;
  final EstadioPresionArterialDeleteStatus deleteStatus;
  final EstadioPresionArterialStatusUI estadioPresionArterialStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final ClasificacionInput clasificacion;
  final PresionArterialSistolicaInput presionArterialSistolica;
  final PresionArterialDiastolicaInput presionArterialDiastolica;

  
  EstadioPresionArterialState(
{
    this.estadioPresionArterials = const [],
    this.estadioPresionArterialStatusUI = EstadioPresionArterialStatusUI.init,
    this.loadedEstadioPresionArterial = const EstadioPresionArterial(0,'','','',),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = EstadioPresionArterialDeleteStatus.none,
    this.clasificacion = const ClasificacionInput.pure(),
    this.presionArterialSistolica = const PresionArterialSistolicaInput.pure(),
    this.presionArterialDiastolica = const PresionArterialDiastolicaInput.pure(),
  });

  EstadioPresionArterialState copyWith({
    List<EstadioPresionArterial> estadioPresionArterials,
    EstadioPresionArterialStatusUI estadioPresionArterialStatusUI,
    bool editMode,
    EstadioPresionArterialDeleteStatus deleteStatus,
    EstadioPresionArterial loadedEstadioPresionArterial,
    FormzStatus formStatus,
    String generalNotificationKey,
    ClasificacionInput clasificacion,
    PresionArterialSistolicaInput presionArterialSistolica,
    PresionArterialDiastolicaInput presionArterialDiastolica,
  }) {
    return EstadioPresionArterialState(
      estadioPresionArterials: estadioPresionArterials ?? this.estadioPresionArterials,
      estadioPresionArterialStatusUI: estadioPresionArterialStatusUI ?? this.estadioPresionArterialStatusUI,
      loadedEstadioPresionArterial: loadedEstadioPresionArterial ?? this.loadedEstadioPresionArterial,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      clasificacion: clasificacion ?? this.clasificacion,
      presionArterialSistolica: presionArterialSistolica ?? this.presionArterialSistolica,
      presionArterialDiastolica: presionArterialDiastolica ?? this.presionArterialDiastolica,
    );
  }

  @override
  List<Object> get props => [estadioPresionArterials, estadioPresionArterialStatusUI,
     loadedEstadioPresionArterial, editMode, deleteStatus, formStatus, generalNotificationKey, 
clasificacion,presionArterialSistolica,presionArterialDiastolica,];

  @override
  bool get stringify => true;
}
