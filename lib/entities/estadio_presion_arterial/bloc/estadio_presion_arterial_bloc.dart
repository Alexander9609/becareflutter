import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_repository.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/bloc/estadio_presion_arterial_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'estadio_presion_arterial_events.dart';
part 'estadio_presion_arterial_state.dart';

class EstadioPresionArterialBloc extends Bloc<EstadioPresionArterialEvent, EstadioPresionArterialState> {
  final EstadioPresionArterialRepository _estadioPresionArterialRepository;

  final clasificacionController = TextEditingController();
  final presionArterialSistolicaController = TextEditingController();
  final presionArterialDiastolicaController = TextEditingController();

  EstadioPresionArterialBloc({@required EstadioPresionArterialRepository estadioPresionArterialRepository}) : assert(estadioPresionArterialRepository != null),
        _estadioPresionArterialRepository = estadioPresionArterialRepository, 
  super(EstadioPresionArterialState());

  @override
  void onTransition(Transition<EstadioPresionArterialEvent, EstadioPresionArterialState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<EstadioPresionArterialState> mapEventToState(EstadioPresionArterialEvent event) async* {
    if (event is InitEstadioPresionArterialList) {
      yield* onInitList(event);
    } else if (event is EstadioPresionArterialFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadEstadioPresionArterialByIdForEdit) {
      yield* onLoadEstadioPresionArterialIdForEdit(event);
    } else if (event is DeleteEstadioPresionArterialById) {
      yield* onDeleteEstadioPresionArterialId(event);
    } else if (event is LoadEstadioPresionArterialByIdForView) {
      yield* onLoadEstadioPresionArterialIdForView(event);
    }else if (event is ClasificacionChanged){
      yield* onClasificacionChange(event);
    }else if (event is PresionArterialSistolicaChanged){
      yield* onPresionArterialSistolicaChange(event);
    }else if (event is PresionArterialDiastolicaChanged){
      yield* onPresionArterialDiastolicaChange(event);
    }  }

  Stream<EstadioPresionArterialState> onInitList(InitEstadioPresionArterialList event) async* {
    yield this.state.copyWith(estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.loading);
    List<EstadioPresionArterial> estadioPresionArterials = await _estadioPresionArterialRepository.getAllEstadioPresionArterials();
    yield this.state.copyWith(estadioPresionArterials: estadioPresionArterials, estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.done);
  }

  Stream<EstadioPresionArterialState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        EstadioPresionArterial result;
        if(this.state.editMode) {
          EstadioPresionArterial newEstadioPresionArterial = EstadioPresionArterial(state.loadedEstadioPresionArterial.id,
            this.state.clasificacion.value,  
            this.state.presionArterialSistolica.value,  
            this.state.presionArterialDiastolica.value,  
          );

          result = await _estadioPresionArterialRepository.update(newEstadioPresionArterial);
        } else {
          EstadioPresionArterial newEstadioPresionArterial = EstadioPresionArterial(null,
            this.state.clasificacion.value,  
            this.state.presionArterialSistolica.value,  
            this.state.presionArterialDiastolica.value,  
          );

          result = await _estadioPresionArterialRepository.create(newEstadioPresionArterial);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<EstadioPresionArterialState> onLoadEstadioPresionArterialIdForEdit(LoadEstadioPresionArterialByIdForEdit event) async* {
    yield this.state.copyWith(estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.loading);
    EstadioPresionArterial loadedEstadioPresionArterial = await _estadioPresionArterialRepository.getEstadioPresionArterial(event.id);

    final clasificacion = ClasificacionInput.dirty(loadedEstadioPresionArterial?.clasificacion != null ? loadedEstadioPresionArterial.clasificacion: '');
    final presionArterialSistolica = PresionArterialSistolicaInput.dirty(loadedEstadioPresionArterial?.presionArterialSistolica != null ? loadedEstadioPresionArterial.presionArterialSistolica: '');
    final presionArterialDiastolica = PresionArterialDiastolicaInput.dirty(loadedEstadioPresionArterial?.presionArterialDiastolica != null ? loadedEstadioPresionArterial.presionArterialDiastolica: '');

    yield this.state.copyWith(loadedEstadioPresionArterial: loadedEstadioPresionArterial, editMode: true,
      clasificacion: clasificacion,
      presionArterialSistolica: presionArterialSistolica,
      presionArterialDiastolica: presionArterialDiastolica,
    estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.done);

    clasificacionController.text = loadedEstadioPresionArterial.clasificacion;
    presionArterialSistolicaController.text = loadedEstadioPresionArterial.presionArterialSistolica;
    presionArterialDiastolicaController.text = loadedEstadioPresionArterial.presionArterialDiastolica;
  }

  Stream<EstadioPresionArterialState> onDeleteEstadioPresionArterialId(DeleteEstadioPresionArterialById event) async* {
    try {
      await _estadioPresionArterialRepository.delete(event.id);
      this.add(InitEstadioPresionArterialList());
      yield this.state.copyWith(deleteStatus: EstadioPresionArterialDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: EstadioPresionArterialDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: EstadioPresionArterialDeleteStatus.none);
  }

  Stream<EstadioPresionArterialState> onLoadEstadioPresionArterialIdForView(LoadEstadioPresionArterialByIdForView event) async* {
    yield this.state.copyWith(estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.loading);
    try {
      EstadioPresionArterial loadedEstadioPresionArterial = await _estadioPresionArterialRepository.getEstadioPresionArterial(event.id);
      yield this.state.copyWith(loadedEstadioPresionArterial: loadedEstadioPresionArterial, estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedEstadioPresionArterial: null, estadioPresionArterialStatusUI: EstadioPresionArterialStatusUI.error);
    }
  }


  Stream<EstadioPresionArterialState> onClasificacionChange(ClasificacionChanged event) async* {
    final clasificacion = ClasificacionInput.dirty(event.clasificacion);
    yield this.state.copyWith(
      clasificacion: clasificacion,
      formStatus: Formz.validate([
        clasificacion,
      this.state.presionArterialSistolica,
      this.state.presionArterialDiastolica,
      ]),
    );
  }
  Stream<EstadioPresionArterialState> onPresionArterialSistolicaChange(PresionArterialSistolicaChanged event) async* {
    final presionArterialSistolica = PresionArterialSistolicaInput.dirty(event.presionArterialSistolica);
    yield this.state.copyWith(
      presionArterialSistolica: presionArterialSistolica,
      formStatus: Formz.validate([
      this.state.clasificacion,
        presionArterialSistolica,
      this.state.presionArterialDiastolica,
      ]),
    );
  }
  Stream<EstadioPresionArterialState> onPresionArterialDiastolicaChange(PresionArterialDiastolicaChanged event) async* {
    final presionArterialDiastolica = PresionArterialDiastolicaInput.dirty(event.presionArterialDiastolica);
    yield this.state.copyWith(
      presionArterialDiastolica: presionArterialDiastolica,
      formStatus: Formz.validate([
      this.state.clasificacion,
      this.state.presionArterialSistolica,
        presionArterialDiastolica,
      ]),
    );
  }

  @override
  Future<void> close() {
    clasificacionController.dispose();
    presionArterialSistolicaController.dispose();
    presionArterialDiastolicaController.dispose();
    return super.close();
  }

}