import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class EstadioPresionArterialRepository {
    EstadioPresionArterialRepository();
  
  static final String uriEndpoint = '/estadio-presion-arterials';

  Future<List<EstadioPresionArterial>> getAllEstadioPresionArterials() async {
    final allEstadioPresionArterialsRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<EstadioPresionArterial>>(allEstadioPresionArterialsRequest.body);
  }

  Future<EstadioPresionArterial> getEstadioPresionArterial(int id) async {
    final estadioPresionArterialRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<EstadioPresionArterial>(estadioPresionArterialRequest.body);
  }

  Future<EstadioPresionArterial> create(EstadioPresionArterial estadioPresionArterial) async {
    final estadioPresionArterialRequest = await HttpUtils.postRequest('$uriEndpoint', estadioPresionArterial);
    return JsonMapper.deserialize<EstadioPresionArterial>(estadioPresionArterialRequest.body);
  }

  Future<EstadioPresionArterial> update(EstadioPresionArterial estadioPresionArterial) async {
    final estadioPresionArterialRequest = await HttpUtils.putRequest('$uriEndpoint', estadioPresionArterial);
    return JsonMapper.deserialize<EstadioPresionArterial>(estadioPresionArterialRequest.body);
  }

  Future<void> delete(int id) async {
    final estadioPresionArterialRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
