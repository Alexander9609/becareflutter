import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/bloc/estadio_presion_arterial_bloc.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class EstadioPresionArterialListScreen extends StatelessWidget {
    EstadioPresionArterialListScreen({Key key}) : super(key: MovilRcv2Keys.estadioPresionArterialListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<EstadioPresionArterialBloc, EstadioPresionArterialState>(
      listener: (context, state) {
        if(state.deleteStatus == EstadioPresionArterialDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('EstadioPresionArterial deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('EstadioPresionArterials List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
              buildWhen: (previous, current) => previous.estadioPresionArterials != current.estadioPresionArterials,
              builder: (context, state) {
                return Visibility(
                  visible: state.estadioPresionArterialStatusUI == EstadioPresionArterialStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (EstadioPresionArterial estadioPresionArterial in state.estadioPresionArterials) estadioPresionArterialCard(estadioPresionArterial, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEstadioPresionArterialCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget estadioPresionArterialCard(EstadioPresionArterial estadioPresionArterial, BuildContext context) {
    EstadioPresionArterialBloc estadioPresionArterialBloc = BlocProvider.of<EstadioPresionArterialBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Clasificacion : ${estadioPresionArterial.clasificacion.toString()}'),
                  subtitle: Text('Presion Arterial Sistolica : ${estadioPresionArterial.presionArterialSistolica.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesEstadioPresionArterialView,
                    arguments: EntityArguments(estadioPresionArterial.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesEstadioPresionArterialEdit,
                    arguments: EntityArguments(estadioPresionArterial.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(estadioPresionArterialBloc, context, estadioPresionArterial.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(EstadioPresionArterialBloc estadioPresionArterialBloc, BuildContext context, int id) {
    return BlocProvider<EstadioPresionArterialBloc>.value(
      value: estadioPresionArterialBloc,
      child: AlertDialog(
        title: new Text('Delete EstadioPresionArterials'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              estadioPresionArterialBloc.add(DeleteEstadioPresionArterialById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
