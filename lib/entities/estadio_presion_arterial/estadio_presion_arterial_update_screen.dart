import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/bloc/estadio_presion_arterial_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';

class EstadioPresionArterialUpdateScreen extends StatelessWidget {
  EstadioPresionArterialUpdateScreen({Key key}) : super(key: MovilRcv2Keys.estadioPresionArterialCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<EstadioPresionArterialBloc, EstadioPresionArterialState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesEstadioPresionArterialList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit EstadioPresionArterials':
'Create EstadioPresionArterials';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          clasificacionField(),
          presionArterialSistolicaField(),
          presionArterialDiastolicaField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget clasificacionField() {
        return BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
            buildWhen: (previous, current) => previous.clasificacion != current.clasificacion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<EstadioPresionArterialBloc>().clasificacionController,
                  onChanged: (value) { context.bloc<EstadioPresionArterialBloc>()
                    .add(ClasificacionChanged(clasificacion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'clasificacion'));
            }
        );
      }
      Widget presionArterialSistolicaField() {
        return BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
            buildWhen: (previous, current) => previous.presionArterialSistolica != current.presionArterialSistolica,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<EstadioPresionArterialBloc>().presionArterialSistolicaController,
                  onChanged: (value) { context.bloc<EstadioPresionArterialBloc>()
                    .add(PresionArterialSistolicaChanged(presionArterialSistolica:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionArterialSistolica'));
            }
        );
      }
      Widget presionArterialDiastolicaField() {
        return BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
            buildWhen: (previous, current) => previous.presionArterialDiastolica != current.presionArterialDiastolica,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<EstadioPresionArterialBloc>().presionArterialDiastolicaController,
                  onChanged: (value) { context.bloc<EstadioPresionArterialBloc>()
                    .add(PresionArterialDiastolicaChanged(presionArterialDiastolica:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionArterialDiastolica'));
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(EstadioPresionArterialState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<EstadioPresionArterialBloc>().add(EstadioPresionArterialFormSubmitted()) : null,
          );
        }
    );
  }
}
