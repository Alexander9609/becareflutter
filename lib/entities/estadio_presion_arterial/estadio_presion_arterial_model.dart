import 'package:dart_json_mapper/dart_json_mapper.dart';


@jsonSerializable
class EstadioPresionArterial {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'clasificacion')
  final String clasificacion;

  @JsonProperty(name: 'presionArterialSistolica')
  final String presionArterialSistolica;

  @JsonProperty(name: 'presionArterialDiastolica')
  final String presionArterialDiastolica;
        
 const EstadioPresionArterial (
     this.id,
        this.clasificacion,
        this.presionArterialSistolica,
        this.presionArterialDiastolica,
    );

@override
String toString() {
    return 'EstadioPresionArterial{'+
    'id: $id,' +
        'clasificacion: $clasificacion,' +
        'presionArterialSistolica: $presionArterialSistolica,' +
        'presionArterialDiastolica: $presionArterialDiastolica,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is EstadioPresionArterial &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


