import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/bloc/estadio_presion_arterial_bloc.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class EstadioPresionArterialViewScreen extends StatelessWidget {
  EstadioPresionArterialViewScreen({Key key}) : super(key: MovilRcv2Keys.estadioPresionArterialCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('EstadioPresionArterials View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<EstadioPresionArterialBloc, EstadioPresionArterialState>(
              buildWhen: (previous, current) => previous.loadedEstadioPresionArterial != current.loadedEstadioPresionArterial,
              builder: (context, state) {
                return Visibility(
                  visible: state.estadioPresionArterialStatusUI == EstadioPresionArterialStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    estadioPresionArterialCard(state.loadedEstadioPresionArterial, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesEstadioPresionArterialCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget estadioPresionArterialCard(EstadioPresionArterial estadioPresionArterial, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + estadioPresionArterial.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Clasificacion : ' + estadioPresionArterial.clasificacion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Arterial Sistolica : ' + estadioPresionArterial.presionArterialSistolica.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Arterial Diastolica : ' + estadioPresionArterial.presionArterialDiastolica.toString(), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
