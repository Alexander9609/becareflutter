import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/entities/medicacion/bloc/medicacion_bloc.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:movilRCV2/shared/widgets/drawer/drawer_widget.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

class MedicacionListScreen extends StatelessWidget {
    MedicacionListScreen({Key key}) : super(key: MovilRcv2Keys.medicacionListScreen);
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  BlocListener<MedicacionBloc, MedicacionState>(
      listener: (context, state) {
        if(state.deleteStatus == MedicacionDeleteStatus.ok) {
          Navigator.of(context).pop();
          scaffoldKey.currentState.showSnackBar(new SnackBar(
              content: new Text('Medicacion deleted successfuly')
          ));
        }
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
    title:Text('Medicacions List'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<MedicacionBloc, MedicacionState>(
              buildWhen: (previous, current) => previous.medicacions != current.medicacions,
              builder: (context, state) {
                return Visibility(
                  visible: state.medicacionStatusUI == MedicacionStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    for (Medicacion medicacion in state.medicacions) medicacionCard(medicacion, context)
                  ]),
                );
              }
            ),
          ),
        drawer: BlocProvider<DrawerBloc>(
            create: (context) => DrawerBloc(loginRepository: LoginRepository()),
            child: MovilRcv2Drawer()),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesMedicacionCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
      ),
    );
  }

  Widget medicacionCard(Medicacion medicacion, BuildContext context) {
    MedicacionBloc medicacionBloc = BlocProvider.of<MedicacionBloc>(context);
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.turned_in),
                  title: Text('Descripcion : ${medicacion.descripcion.toString()}'),
                  subtitle: Text('Fecha Actualizacion : ${medicacion.fechaActualizacion.toString()}'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
              child: Text('View'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesMedicacionView,
                    arguments: EntityArguments(medicacion.id)
                ),
                ),
                FlatButton(
                  child: Text('Edit'),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    MovilRcv2Routes.entitiesMedicacionEdit,
                    arguments: EntityArguments(medicacion.id)
                  ),
                ),
                FlatButton(
                  child: Text('Delete'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return deleteDialog(medicacionBloc, context, medicacion.id);
                      },
                    );
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget deleteDialog(MedicacionBloc medicacionBloc, BuildContext context, int id) {
    return BlocProvider<MedicacionBloc>.value(
      value: medicacionBloc,
      child: AlertDialog(
        title: new Text('Delete Medicacions'),
        content: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Yes'),
            onPressed: () {
              medicacionBloc.add(DeleteMedicacionById(id: id));
            },
          ),
          new FlatButton(
            child: new Text('No'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

}
