import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/medicacion/bloc/medicacion_bloc.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class MedicacionViewScreen extends StatelessWidget {
  MedicacionViewScreen({Key key}) : super(key: MovilRcv2Keys.medicacionCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Medicacions View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<MedicacionBloc, MedicacionState>(
              buildWhen: (previous, current) => previous.loadedMedicacion != current.loadedMedicacion,
              builder: (context, state) {
                return Visibility(
                  visible: state.medicacionStatusUI == MedicacionStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    medicacionCard(state.loadedMedicacion, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesMedicacionCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget medicacionCard(Medicacion medicacion, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + medicacion.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Descripcion : ' + medicacion.descripcion.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha Actualizacion : ' + (medicacion?.fechaActualizacion != null ? DateFormat.yMMMMd('en').format(medicacion.fechaActualizacion) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
