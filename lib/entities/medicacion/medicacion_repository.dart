import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class MedicacionRepository {
    MedicacionRepository();
  
  static final String uriEndpoint = '/medicacions';

  Future<List<Medicacion>> getAllMedicacions() async {
    final allMedicacionsRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Medicacion>>(allMedicacionsRequest.body);
  }

  Future<Medicacion> getMedicacion(int id) async {
    final medicacionRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Medicacion>(medicacionRequest.body);
  }

  Future<Medicacion> create(Medicacion medicacion) async {
    final medicacionRequest = await HttpUtils.postRequest('$uriEndpoint', medicacion);
    return JsonMapper.deserialize<Medicacion>(medicacionRequest.body);
  }

  Future<Medicacion> update(Medicacion medicacion) async {
    final medicacionRequest = await HttpUtils.putRequest('$uriEndpoint', medicacion);
    return JsonMapper.deserialize<Medicacion>(medicacionRequest.body);
  }

  Future<void> delete(int id) async {
    final medicacionRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
