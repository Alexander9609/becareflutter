import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_repository.dart';
import 'package:movilRCV2/entities/medicacion/bloc/medicacion_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'medicacion_events.dart';
part 'medicacion_state.dart';

class MedicacionBloc extends Bloc<MedicacionEvent, MedicacionState> {
  final MedicacionRepository _medicacionRepository;

  final descripcionController = TextEditingController();
  final fechaActualizacionController = TextEditingController();

  MedicacionBloc({@required MedicacionRepository medicacionRepository}) : assert(medicacionRepository != null),
        _medicacionRepository = medicacionRepository, 
  super(MedicacionState(null,));

  @override
  void onTransition(Transition<MedicacionEvent, MedicacionState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<MedicacionState> mapEventToState(MedicacionEvent event) async* {
    if (event is InitMedicacionList) {
      yield* onInitList(event);
    } else if (event is MedicacionFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadMedicacionByIdForEdit) {
      yield* onLoadMedicacionIdForEdit(event);
    } else if (event is DeleteMedicacionById) {
      yield* onDeleteMedicacionId(event);
    } else if (event is LoadMedicacionByIdForView) {
      yield* onLoadMedicacionIdForView(event);
    }else if (event is DescripcionChanged){
      yield* onDescripcionChange(event);
    }else if (event is FechaActualizacionChanged){
      yield* onFechaActualizacionChange(event);
    }  }

  Stream<MedicacionState> onInitList(InitMedicacionList event) async* {
    yield this.state.copyWith(medicacionStatusUI: MedicacionStatusUI.loading);
    List<Medicacion> medicacions = await _medicacionRepository.getAllMedicacions();
    yield this.state.copyWith(medicacions: medicacions, medicacionStatusUI: MedicacionStatusUI.done);
  }

  Stream<MedicacionState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Medicacion result;
        if(this.state.editMode) {
          Medicacion newMedicacion = Medicacion(state.loadedMedicacion.id,
            this.state.descripcion.value,  
            this.state.fechaActualizacion.value,  
          );

          result = await _medicacionRepository.update(newMedicacion);
        } else {
          Medicacion newMedicacion = Medicacion(null,
            this.state.descripcion.value,  
            this.state.fechaActualizacion.value,  
          );

          result = await _medicacionRepository.create(newMedicacion);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<MedicacionState> onLoadMedicacionIdForEdit(LoadMedicacionByIdForEdit event) async* {
    yield this.state.copyWith(medicacionStatusUI: MedicacionStatusUI.loading);
    Medicacion loadedMedicacion = await _medicacionRepository.getMedicacion(event.id);

    final descripcion = DescripcionInput.dirty(loadedMedicacion?.descripcion != null ? loadedMedicacion.descripcion: '');
    final fechaActualizacion = FechaActualizacionInput.dirty(loadedMedicacion?.fechaActualizacion != null ? loadedMedicacion.fechaActualizacion: null);

    yield this.state.copyWith(loadedMedicacion: loadedMedicacion, editMode: true,
      descripcion: descripcion,
      fechaActualizacion: fechaActualizacion,
    medicacionStatusUI: MedicacionStatusUI.done);

    descripcionController.text = loadedMedicacion.descripcion;
    fechaActualizacionController.text = DateFormat.yMMMMd('en').format(loadedMedicacion?.fechaActualizacion);
  }

  Stream<MedicacionState> onDeleteMedicacionId(DeleteMedicacionById event) async* {
    try {
      await _medicacionRepository.delete(event.id);
      this.add(InitMedicacionList());
      yield this.state.copyWith(deleteStatus: MedicacionDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: MedicacionDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: MedicacionDeleteStatus.none);
  }

  Stream<MedicacionState> onLoadMedicacionIdForView(LoadMedicacionByIdForView event) async* {
    yield this.state.copyWith(medicacionStatusUI: MedicacionStatusUI.loading);
    try {
      Medicacion loadedMedicacion = await _medicacionRepository.getMedicacion(event.id);
      yield this.state.copyWith(loadedMedicacion: loadedMedicacion, medicacionStatusUI: MedicacionStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedMedicacion: null, medicacionStatusUI: MedicacionStatusUI.error);
    }
  }


  Stream<MedicacionState> onDescripcionChange(DescripcionChanged event) async* {
    final descripcion = DescripcionInput.dirty(event.descripcion);
    yield this.state.copyWith(
      descripcion: descripcion,
      formStatus: Formz.validate([
        descripcion,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<MedicacionState> onFechaActualizacionChange(FechaActualizacionChanged event) async* {
    final fechaActualizacion = FechaActualizacionInput.dirty(event.fechaActualizacion);
    yield this.state.copyWith(
      fechaActualizacion: fechaActualizacion,
      formStatus: Formz.validate([
      this.state.descripcion,
        fechaActualizacion,
      ]),
    );
  }

  @override
  Future<void> close() {
    descripcionController.dispose();
    fechaActualizacionController.dispose();
    return super.close();
  }

}