import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_model.dart';

enum DescripcionValidationError { invalid }
class DescripcionInput extends FormzInput<String, DescripcionValidationError> {
  const DescripcionInput.pure() : super.pure('');
  const DescripcionInput.dirty([String value = '']) : super.dirty(value);

  @override
  DescripcionValidationError validator(String value) {
    return null;
  }
}

enum FechaActualizacionValidationError { invalid }
class FechaActualizacionInput extends FormzInput<DateTime, FechaActualizacionValidationError> {
  FechaActualizacionInput.pure() : super.pure(DateTime.now());
  FechaActualizacionInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaActualizacionValidationError validator(DateTime value) {
    return null;
  }
}

