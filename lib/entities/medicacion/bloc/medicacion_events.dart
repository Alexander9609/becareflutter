part of 'medicacion_bloc.dart';

abstract class MedicacionEvent extends Equatable {
  const MedicacionEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitMedicacionList extends MedicacionEvent {}

class DescripcionChanged extends MedicacionEvent {
  final String descripcion;
  
  const DescripcionChanged({@required this.descripcion});
  
  @override
  List<Object> get props => [descripcion];
}
class FechaActualizacionChanged extends MedicacionEvent {
  final DateTime fechaActualizacion;
  
  const FechaActualizacionChanged({@required this.fechaActualizacion});
  
  @override
  List<Object> get props => [fechaActualizacion];
}

class MedicacionFormSubmitted extends MedicacionEvent {}

class LoadMedicacionByIdForEdit extends MedicacionEvent {
  final int id;

  const LoadMedicacionByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteMedicacionById extends MedicacionEvent {
  final int id;

  const DeleteMedicacionById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadMedicacionByIdForView extends MedicacionEvent {
  final int id;

  const LoadMedicacionByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
