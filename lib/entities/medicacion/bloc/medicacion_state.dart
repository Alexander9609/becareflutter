part of 'medicacion_bloc.dart';

enum MedicacionStatusUI {init, loading, error, done}
enum MedicacionDeleteStatus {ok, ko, none}

class MedicacionState extends Equatable {
  final List<Medicacion> medicacions;
  final Medicacion loadedMedicacion;
  final bool editMode;
  final MedicacionDeleteStatus deleteStatus;
  final MedicacionStatusUI medicacionStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final DescripcionInput descripcion;
  final FechaActualizacionInput fechaActualizacion;

  
  MedicacionState(
FechaActualizacionInput fechaActualizacion,{
    this.medicacions = const [],
    this.medicacionStatusUI = MedicacionStatusUI.init,
    this.loadedMedicacion = const Medicacion(0,'',null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = MedicacionDeleteStatus.none,
    this.descripcion = const DescripcionInput.pure(),
  }):this.fechaActualizacion = fechaActualizacion ?? FechaActualizacionInput.pure()
;

  MedicacionState copyWith({
    List<Medicacion> medicacions,
    MedicacionStatusUI medicacionStatusUI,
    bool editMode,
    MedicacionDeleteStatus deleteStatus,
    Medicacion loadedMedicacion,
    FormzStatus formStatus,
    String generalNotificationKey,
    DescripcionInput descripcion,
    FechaActualizacionInput fechaActualizacion,
  }) {
    return MedicacionState(
        fechaActualizacion,
      medicacions: medicacions ?? this.medicacions,
      medicacionStatusUI: medicacionStatusUI ?? this.medicacionStatusUI,
      loadedMedicacion: loadedMedicacion ?? this.loadedMedicacion,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      descripcion: descripcion ?? this.descripcion,
    );
  }

  @override
  List<Object> get props => [medicacions, medicacionStatusUI,
     loadedMedicacion, editMode, deleteStatus, formStatus, generalNotificationKey, 
descripcion,fechaActualizacion,];

  @override
  bool get stringify => true;
}
