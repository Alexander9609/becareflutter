import 'package:dart_json_mapper/dart_json_mapper.dart';


@jsonSerializable
class Medicacion {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'descripcion')
  final String descripcion;

  @JsonProperty(name: 'fechaActualizacion', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fechaActualizacion;
        
 const Medicacion (
     this.id,
        this.descripcion,
        this.fechaActualizacion,
    );

@override
String toString() {
    return 'Medicacion{'+
    'id: $id,' +
        'descripcion: $descripcion,' +
        'fechaActualizacion: $fechaActualizacion,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Medicacion &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


