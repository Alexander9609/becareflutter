import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/condicion/bloc/condicion_bloc.dart';
import 'package:movilRCV2/entities/condicion/condicion_model.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/routes.dart';
import 'package:intl/intl.dart';
import 'package:movilRCV2/shared/widgets/loading_indicator_widget.dart';

class CondicionViewScreen extends StatelessWidget {
  CondicionViewScreen({Key key}) : super(key: MovilRcv2Keys.condicionCreateScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:Text('Condicions View'),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: BlocBuilder<CondicionBloc, CondicionState>(
              buildWhen: (previous, current) => previous.loadedCondicion != current.loadedCondicion,
              builder: (context, state) {
                return Visibility(
                  visible: state.condicionStatusUI == CondicionStatusUI.done,
                  replacement: LoadingIndicator(),
                  child: Column(children: <Widget>[
                    condicionCard(state.loadedCondicion, context)
                  ]),
                );
              }
            ),
          ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => Navigator.pushNamed(context, MovilRcv2Routes.entitiesCondicionCreate),
          child: Icon(Icons.add, color: Theme.of(context).iconTheme.color,),
          backgroundColor: Theme.of(context).primaryColor,
        )
    );
  }

  Widget condicionCard(Condicion condicion, BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width * 0.90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Id : ' + condicion.id.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Descripcion : ' + condicion.descripcion.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Oximetria Referencia : ' + condicion.oximetriaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Temperatura Referencia : ' + condicion.temperaturaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Ritmo Cardiaco Referencia : ' + condicion.ritmoCardiacoReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Sistolica Referencia : ' + condicion.presionSistolicaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
                Text('Presion Diastolica Referencia : ' + condicion.presionDiastolicaReferencia.toString(), style: Theme.of(context).textTheme.bodyText1,),
              Text('Fecha Actualizacion : ' + (condicion?.fechaActualizacion != null ? DateFormat.yMMMMd('en').format(condicion.fechaActualizacion) : ''), style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
      ),
    );
  }
}
