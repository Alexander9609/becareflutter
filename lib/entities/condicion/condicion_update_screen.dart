import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/entities/condicion/bloc/condicion_bloc.dart';
import 'package:movilRCV2/keys.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:movilRCV2/entities/condicion/condicion_model.dart';

class CondicionUpdateScreen extends StatelessWidget {
  CondicionUpdateScreen({Key key}) : super(key: MovilRcv2Keys.condicionCreateScreen);

  @override
  Widget build(BuildContext context) {
    return BlocListener<CondicionBloc, CondicionState>(
      listener: (context, state) {
        if(state.formStatus.isSubmissionSuccess){
          Navigator.pushNamed(context, MovilRcv2Routes.entitiesCondicionList);
        }
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: BlocBuilder<CondicionBloc, CondicionState>(
                buildWhen: (previous, current) => previous.editMode != current.editMode,
                builder: (context, state) {
                String title = state.editMode == true ?'Edit Condicions':
'Create Condicions';
                 return Text(title);
                }
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(15.0),
            child: Column(children: <Widget>[settingsForm(context)]),
          ),
      ),
    );
  }

  Widget settingsForm(BuildContext context) {
    return Form(
      child: Wrap(runSpacing: 15, children: <Widget>[
          descripcionField(),
          oximetriaReferenciaField(),
          temperaturaReferenciaField(),
          ritmoCardiacoReferenciaField(),
          presionSistolicaReferenciaField(),
          presionDiastolicaReferenciaField(),
          fechaActualizacionField(),
        validationZone(),
        submit(context)
      ]),
    );
  }

      Widget descripcionField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.descripcion != current.descripcion,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().descripcionController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(DescripcionChanged(descripcion:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'descripcion'));
            }
        );
      }
      Widget oximetriaReferenciaField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.oximetriaReferencia != current.oximetriaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().oximetriaReferenciaController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(OximetriaReferenciaChanged(oximetriaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'oximetriaReferencia'));
            }
        );
      }
      Widget temperaturaReferenciaField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.temperaturaReferencia != current.temperaturaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().temperaturaReferenciaController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(TemperaturaReferenciaChanged(temperaturaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'temperaturaReferencia'));
            }
        );
      }
      Widget ritmoCardiacoReferenciaField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.ritmoCardiacoReferencia != current.ritmoCardiacoReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().ritmoCardiacoReferenciaController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(RitmoCardiacoReferenciaChanged(ritmoCardiacoReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'ritmoCardiacoReferencia'));
            }
        );
      }
      Widget presionSistolicaReferenciaField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.presionSistolicaReferencia != current.presionSistolicaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().presionSistolicaReferenciaController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(PresionSistolicaReferenciaChanged(presionSistolicaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionSistolicaReferencia'));
            }
        );
      }
      Widget presionDiastolicaReferenciaField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.presionDiastolicaReferencia != current.presionDiastolicaReferencia,
            builder: (context, state) {
              return TextFormField(
                  controller: context.bloc<CondicionBloc>().presionDiastolicaReferenciaController,
                  onChanged: (value) { context.bloc<CondicionBloc>()
                    .add(PresionDiastolicaReferenciaChanged(presionDiastolicaReferencia:value)); },
                  keyboardType:TextInputType.text,                  decoration: InputDecoration(
                      labelText:'presionDiastolicaReferencia'));
            }
        );
      }
      Widget fechaActualizacionField() {
        return BlocBuilder<CondicionBloc, CondicionState>(
            buildWhen: (previous, current) => previous.fechaActualizacion != current.fechaActualizacion,
            builder: (context, state) {
              return DateTimeField(
                controller: context.bloc<CondicionBloc>().fechaActualizacionController,
                onChanged: (value) { context.bloc<CondicionBloc>().add(FechaActualizacionChanged(fechaActualizacion: value)); },
                format: DateFormat.yMMMMd('en'),
                keyboardType: TextInputType.datetime,
            decoration: InputDecoration(labelText:'fechaActualizacion',),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      locale: Locale('en'),
                      context: context,
                      firstDate: DateTime(1950),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2050));
                },
              );
            }
        );
      }


  Widget validationZone() {
    return BlocBuilder<CondicionBloc, CondicionState>(
        buildWhen:(previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          return Visibility(
              visible: state.formStatus.isSubmissionFailure ||  state.formStatus.isSubmissionSuccess,
              child: Center(
                child: generateNotificationText(state, context),
              ));
        });
  }

  Widget generateNotificationText(CondicionState state, BuildContext context) {
    String notificationTranslated = '';
    MaterialColor notificationColors;

    if (state.generalNotificationKey.toString().compareTo(HttpUtils.errorServerKey) == 0) {
      notificationTranslated ='Something wrong when calling the server, please try again';
      notificationColors = Theme.of(context).errorColor;
    } else if (state.generalNotificationKey.toString().compareTo(HttpUtils.badRequestServerKey) == 0) {
      notificationTranslated ='Something wrong happened with the received data';
      notificationColors = Theme.of(context).errorColor;
    }

    return Text(
      notificationTranslated,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: Theme.of(context).textTheme.bodyText1.fontSize,
          color: notificationColors),
    );
  }

  submit(BuildContext context) {
    return BlocBuilder<CondicionBloc, CondicionState>(
        buildWhen: (previous, current) => previous.formStatus != current.formStatus,
        builder: (context, state) {
          String buttonLabel = state.editMode == true ?
'Edit':
'Create';
          return RaisedButton(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Visibility(
                    replacement: CircularProgressIndicator(value: null),
                    visible: !state.formStatus.isSubmissionInProgress,
                    child: Text(buttonLabel),
                  ),
                )),
            onPressed: state.formStatus.isValidated ? () => context.bloc<CondicionBloc>().add(CondicionFormSubmitted()) : null,
          );
        }
    );
  }
}
