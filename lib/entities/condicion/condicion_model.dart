import 'package:dart_json_mapper/dart_json_mapper.dart';


@jsonSerializable
class Condicion {

  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'descripcion')
  final String descripcion;

  @JsonProperty(name: 'oximetriaReferencia')
  final String oximetriaReferencia;

  @JsonProperty(name: 'temperaturaReferencia')
  final String temperaturaReferencia;

  @JsonProperty(name: 'ritmoCardiacoReferencia')
  final String ritmoCardiacoReferencia;

  @JsonProperty(name: 'presionSistolicaReferencia')
  final String presionSistolicaReferencia;

  @JsonProperty(name: 'presionDiastolicaReferencia')
  final String presionDiastolicaReferencia;

  @JsonProperty(name: 'fechaActualizacion', converterParams: {'format': 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\''})
  final DateTime fechaActualizacion;
        
 const Condicion (
     this.id,
        this.descripcion,
        this.oximetriaReferencia,
        this.temperaturaReferencia,
        this.ritmoCardiacoReferencia,
        this.presionSistolicaReferencia,
        this.presionDiastolicaReferencia,
        this.fechaActualizacion,
    );

@override
String toString() {
    return 'Condicion{'+
    'id: $id,' +
        'descripcion: $descripcion,' +
        'oximetriaReferencia: $oximetriaReferencia,' +
        'temperaturaReferencia: $temperaturaReferencia,' +
        'ritmoCardiacoReferencia: $ritmoCardiacoReferencia,' +
        'presionSistolicaReferencia: $presionSistolicaReferencia,' +
        'presionDiastolicaReferencia: $presionDiastolicaReferencia,' +
        'fechaActualizacion: $fechaActualizacion,' +
    '}';
   }

@override
bool operator == (Object other) => 
    identical(this, other) ||
    other is Condicion &&
    id == other.id ;


@override
int get hashCode => 
    id.hashCode ;
}


