import 'package:movilRCV2/entities/condicion/condicion_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:dart_json_mapper/dart_json_mapper.dart';

class CondicionRepository {
    CondicionRepository();
  
  static final String uriEndpoint = '/condicions';

  Future<List<Condicion>> getAllCondicions() async {
    final allCondicionsRequest = await HttpUtils.getRequest(uriEndpoint);
    return JsonMapper.deserialize<List<Condicion>>(allCondicionsRequest.body);
  }

  Future<Condicion> getCondicion(int id) async {
    final condicionRequest = await HttpUtils.getRequest('$uriEndpoint/$id');
    return JsonMapper.deserialize<Condicion>(condicionRequest.body);
  }

  Future<Condicion> create(Condicion condicion) async {
    final condicionRequest = await HttpUtils.postRequest('$uriEndpoint', condicion);
    return JsonMapper.deserialize<Condicion>(condicionRequest.body);
  }

  Future<Condicion> update(Condicion condicion) async {
    final condicionRequest = await HttpUtils.putRequest('$uriEndpoint', condicion);
    return JsonMapper.deserialize<Condicion>(condicionRequest.body);
  }

  Future<void> delete(int id) async {
    final condicionRequest = await HttpUtils.deleteRequest('$uriEndpoint/$id');
  }
}
