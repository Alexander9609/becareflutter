import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:formz/formz.dart';

import 'package:movilRCV2/entities/condicion/condicion_model.dart';
import 'package:movilRCV2/entities/condicion/condicion_repository.dart';
import 'package:movilRCV2/entities/condicion/bloc/condicion_form_model.dart';
import 'package:movilRCV2/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

part 'condicion_events.dart';
part 'condicion_state.dart';

class CondicionBloc extends Bloc<CondicionEvent, CondicionState> {
  final CondicionRepository _condicionRepository;

  final descripcionController = TextEditingController();
  final oximetriaReferenciaController = TextEditingController();
  final temperaturaReferenciaController = TextEditingController();
  final ritmoCardiacoReferenciaController = TextEditingController();
  final presionSistolicaReferenciaController = TextEditingController();
  final presionDiastolicaReferenciaController = TextEditingController();
  final fechaActualizacionController = TextEditingController();

  CondicionBloc({@required CondicionRepository condicionRepository}) : assert(condicionRepository != null),
        _condicionRepository = condicionRepository, 
  super(CondicionState(null,));

  @override
  void onTransition(Transition<CondicionEvent, CondicionState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<CondicionState> mapEventToState(CondicionEvent event) async* {
    if (event is InitCondicionList) {
      yield* onInitList(event);
    } else if (event is CondicionFormSubmitted) {
      yield* onSubmit();
    } else if (event is LoadCondicionByIdForEdit) {
      yield* onLoadCondicionIdForEdit(event);
    } else if (event is DeleteCondicionById) {
      yield* onDeleteCondicionId(event);
    } else if (event is LoadCondicionByIdForView) {
      yield* onLoadCondicionIdForView(event);
    }else if (event is DescripcionChanged){
      yield* onDescripcionChange(event);
    }else if (event is OximetriaReferenciaChanged){
      yield* onOximetriaReferenciaChange(event);
    }else if (event is TemperaturaReferenciaChanged){
      yield* onTemperaturaReferenciaChange(event);
    }else if (event is RitmoCardiacoReferenciaChanged){
      yield* onRitmoCardiacoReferenciaChange(event);
    }else if (event is PresionSistolicaReferenciaChanged){
      yield* onPresionSistolicaReferenciaChange(event);
    }else if (event is PresionDiastolicaReferenciaChanged){
      yield* onPresionDiastolicaReferenciaChange(event);
    }else if (event is FechaActualizacionChanged){
      yield* onFechaActualizacionChange(event);
    }  }

  Stream<CondicionState> onInitList(InitCondicionList event) async* {
    yield this.state.copyWith(condicionStatusUI: CondicionStatusUI.loading);
    List<Condicion> condicions = await _condicionRepository.getAllCondicions();
    yield this.state.copyWith(condicions: condicions, condicionStatusUI: CondicionStatusUI.done);
  }

  Stream<CondicionState> onSubmit() async* {
    if (this.state.formStatus.isValidated) {
      yield this.state.copyWith(formStatus: FormzStatus.submissionInProgress);
      try {
        Condicion result;
        if(this.state.editMode) {
          Condicion newCondicion = Condicion(state.loadedCondicion.id,
            this.state.descripcion.value,  
            this.state.oximetriaReferencia.value,  
            this.state.temperaturaReferencia.value,  
            this.state.ritmoCardiacoReferencia.value,  
            this.state.presionSistolicaReferencia.value,  
            this.state.presionDiastolicaReferencia.value,  
            this.state.fechaActualizacion.value,  
          );

          result = await _condicionRepository.update(newCondicion);
        } else {
          Condicion newCondicion = Condicion(null,
            this.state.descripcion.value,  
            this.state.oximetriaReferencia.value,  
            this.state.temperaturaReferencia.value,  
            this.state.ritmoCardiacoReferencia.value,  
            this.state.presionSistolicaReferencia.value,  
            this.state.presionDiastolicaReferencia.value,  
            this.state.fechaActualizacion.value,  
          );

          result = await _condicionRepository.create(newCondicion);
        }

        if (result == null) {
          yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
              generalNotificationKey: HttpUtils.badRequestServerKey);
        } else {
          yield this.state.copyWith(formStatus: FormzStatus.submissionSuccess,
              generalNotificationKey: HttpUtils.successResult);
        }
      } catch (e) {
        yield this.state.copyWith(formStatus: FormzStatus.submissionFailure,
            generalNotificationKey: HttpUtils.errorServerKey);
      }
    }
  }

  Stream<CondicionState> onLoadCondicionIdForEdit(LoadCondicionByIdForEdit event) async* {
    yield this.state.copyWith(condicionStatusUI: CondicionStatusUI.loading);
    Condicion loadedCondicion = await _condicionRepository.getCondicion(event.id);

    final descripcion = DescripcionInput.dirty(loadedCondicion?.descripcion != null ? loadedCondicion.descripcion: '');
    final oximetriaReferencia = OximetriaReferenciaInput.dirty(loadedCondicion?.oximetriaReferencia != null ? loadedCondicion.oximetriaReferencia: '');
    final temperaturaReferencia = TemperaturaReferenciaInput.dirty(loadedCondicion?.temperaturaReferencia != null ? loadedCondicion.temperaturaReferencia: '');
    final ritmoCardiacoReferencia = RitmoCardiacoReferenciaInput.dirty(loadedCondicion?.ritmoCardiacoReferencia != null ? loadedCondicion.ritmoCardiacoReferencia: '');
    final presionSistolicaReferencia = PresionSistolicaReferenciaInput.dirty(loadedCondicion?.presionSistolicaReferencia != null ? loadedCondicion.presionSistolicaReferencia: '');
    final presionDiastolicaReferencia = PresionDiastolicaReferenciaInput.dirty(loadedCondicion?.presionDiastolicaReferencia != null ? loadedCondicion.presionDiastolicaReferencia: '');
    final fechaActualizacion = FechaActualizacionInput.dirty(loadedCondicion?.fechaActualizacion != null ? loadedCondicion.fechaActualizacion: null);

    yield this.state.copyWith(loadedCondicion: loadedCondicion, editMode: true,
      descripcion: descripcion,
      oximetriaReferencia: oximetriaReferencia,
      temperaturaReferencia: temperaturaReferencia,
      ritmoCardiacoReferencia: ritmoCardiacoReferencia,
      presionSistolicaReferencia: presionSistolicaReferencia,
      presionDiastolicaReferencia: presionDiastolicaReferencia,
      fechaActualizacion: fechaActualizacion,
    condicionStatusUI: CondicionStatusUI.done);

    descripcionController.text = loadedCondicion.descripcion;
    oximetriaReferenciaController.text = loadedCondicion.oximetriaReferencia;
    temperaturaReferenciaController.text = loadedCondicion.temperaturaReferencia;
    ritmoCardiacoReferenciaController.text = loadedCondicion.ritmoCardiacoReferencia;
    presionSistolicaReferenciaController.text = loadedCondicion.presionSistolicaReferencia;
    presionDiastolicaReferenciaController.text = loadedCondicion.presionDiastolicaReferencia;
    fechaActualizacionController.text = DateFormat.yMMMMd('en').format(loadedCondicion?.fechaActualizacion);
  }

  Stream<CondicionState> onDeleteCondicionId(DeleteCondicionById event) async* {
    try {
      await _condicionRepository.delete(event.id);
      this.add(InitCondicionList());
      yield this.state.copyWith(deleteStatus: CondicionDeleteStatus.ok);
    } catch (e) {
      yield this.state.copyWith(deleteStatus: CondicionDeleteStatus.ko,
          generalNotificationKey: HttpUtils.errorServerKey);
    }
    yield this.state.copyWith(deleteStatus: CondicionDeleteStatus.none);
  }

  Stream<CondicionState> onLoadCondicionIdForView(LoadCondicionByIdForView event) async* {
    yield this.state.copyWith(condicionStatusUI: CondicionStatusUI.loading);
    try {
      Condicion loadedCondicion = await _condicionRepository.getCondicion(event.id);
      yield this.state.copyWith(loadedCondicion: loadedCondicion, condicionStatusUI: CondicionStatusUI.done);
    } catch(e) {
      yield this.state.copyWith(loadedCondicion: null, condicionStatusUI: CondicionStatusUI.error);
    }
  }


  Stream<CondicionState> onDescripcionChange(DescripcionChanged event) async* {
    final descripcion = DescripcionInput.dirty(event.descripcion);
    yield this.state.copyWith(
      descripcion: descripcion,
      formStatus: Formz.validate([
        descripcion,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onOximetriaReferenciaChange(OximetriaReferenciaChanged event) async* {
    final oximetriaReferencia = OximetriaReferenciaInput.dirty(event.oximetriaReferencia);
    yield this.state.copyWith(
      oximetriaReferencia: oximetriaReferencia,
      formStatus: Formz.validate([
      this.state.descripcion,
        oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onTemperaturaReferenciaChange(TemperaturaReferenciaChanged event) async* {
    final temperaturaReferencia = TemperaturaReferenciaInput.dirty(event.temperaturaReferencia);
    yield this.state.copyWith(
      temperaturaReferencia: temperaturaReferencia,
      formStatus: Formz.validate([
      this.state.descripcion,
      this.state.oximetriaReferencia,
        temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onRitmoCardiacoReferenciaChange(RitmoCardiacoReferenciaChanged event) async* {
    final ritmoCardiacoReferencia = RitmoCardiacoReferenciaInput.dirty(event.ritmoCardiacoReferencia);
    yield this.state.copyWith(
      ritmoCardiacoReferencia: ritmoCardiacoReferencia,
      formStatus: Formz.validate([
      this.state.descripcion,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
        ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onPresionSistolicaReferenciaChange(PresionSistolicaReferenciaChanged event) async* {
    final presionSistolicaReferencia = PresionSistolicaReferenciaInput.dirty(event.presionSistolicaReferencia);
    yield this.state.copyWith(
      presionSistolicaReferencia: presionSistolicaReferencia,
      formStatus: Formz.validate([
      this.state.descripcion,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
        presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onPresionDiastolicaReferenciaChange(PresionDiastolicaReferenciaChanged event) async* {
    final presionDiastolicaReferencia = PresionDiastolicaReferenciaInput.dirty(event.presionDiastolicaReferencia);
    yield this.state.copyWith(
      presionDiastolicaReferencia: presionDiastolicaReferencia,
      formStatus: Formz.validate([
      this.state.descripcion,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
        presionDiastolicaReferencia,
      this.state.fechaActualizacion,
      ]),
    );
  }
  Stream<CondicionState> onFechaActualizacionChange(FechaActualizacionChanged event) async* {
    final fechaActualizacion = FechaActualizacionInput.dirty(event.fechaActualizacion);
    yield this.state.copyWith(
      fechaActualizacion: fechaActualizacion,
      formStatus: Formz.validate([
      this.state.descripcion,
      this.state.oximetriaReferencia,
      this.state.temperaturaReferencia,
      this.state.ritmoCardiacoReferencia,
      this.state.presionSistolicaReferencia,
      this.state.presionDiastolicaReferencia,
        fechaActualizacion,
      ]),
    );
  }

  @override
  Future<void> close() {
    descripcionController.dispose();
    oximetriaReferenciaController.dispose();
    temperaturaReferenciaController.dispose();
    ritmoCardiacoReferenciaController.dispose();
    presionSistolicaReferenciaController.dispose();
    presionDiastolicaReferenciaController.dispose();
    fechaActualizacionController.dispose();
    return super.close();
  }

}