part of 'condicion_bloc.dart';

abstract class CondicionEvent extends Equatable {
  const CondicionEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InitCondicionList extends CondicionEvent {}

class DescripcionChanged extends CondicionEvent {
  final String descripcion;
  
  const DescripcionChanged({@required this.descripcion});
  
  @override
  List<Object> get props => [descripcion];
}
class OximetriaReferenciaChanged extends CondicionEvent {
  final String oximetriaReferencia;
  
  const OximetriaReferenciaChanged({@required this.oximetriaReferencia});
  
  @override
  List<Object> get props => [oximetriaReferencia];
}
class TemperaturaReferenciaChanged extends CondicionEvent {
  final String temperaturaReferencia;
  
  const TemperaturaReferenciaChanged({@required this.temperaturaReferencia});
  
  @override
  List<Object> get props => [temperaturaReferencia];
}
class RitmoCardiacoReferenciaChanged extends CondicionEvent {
  final String ritmoCardiacoReferencia;
  
  const RitmoCardiacoReferenciaChanged({@required this.ritmoCardiacoReferencia});
  
  @override
  List<Object> get props => [ritmoCardiacoReferencia];
}
class PresionSistolicaReferenciaChanged extends CondicionEvent {
  final String presionSistolicaReferencia;
  
  const PresionSistolicaReferenciaChanged({@required this.presionSistolicaReferencia});
  
  @override
  List<Object> get props => [presionSistolicaReferencia];
}
class PresionDiastolicaReferenciaChanged extends CondicionEvent {
  final String presionDiastolicaReferencia;
  
  const PresionDiastolicaReferenciaChanged({@required this.presionDiastolicaReferencia});
  
  @override
  List<Object> get props => [presionDiastolicaReferencia];
}
class FechaActualizacionChanged extends CondicionEvent {
  final DateTime fechaActualizacion;
  
  const FechaActualizacionChanged({@required this.fechaActualizacion});
  
  @override
  List<Object> get props => [fechaActualizacion];
}

class CondicionFormSubmitted extends CondicionEvent {}

class LoadCondicionByIdForEdit extends CondicionEvent {
  final int id;

  const LoadCondicionByIdForEdit({@required this.id});

  @override
  List<Object> get props => [id];
}

class DeleteCondicionById extends CondicionEvent {
  final int id;

  const DeleteCondicionById({@required this.id});

  @override
  List<Object> get props => [id];
}

class LoadCondicionByIdForView extends CondicionEvent {
  final int id;

  const LoadCondicionByIdForView({@required this.id});

  @override
  List<Object> get props => [id];
}
