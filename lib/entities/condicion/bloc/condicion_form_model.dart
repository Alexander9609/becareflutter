import 'package:formz/formz.dart';
import 'package:movilRCV2/entities/condicion/condicion_model.dart';

enum DescripcionValidationError { invalid }
class DescripcionInput extends FormzInput<String, DescripcionValidationError> {
  const DescripcionInput.pure() : super.pure('');
  const DescripcionInput.dirty([String value = '']) : super.dirty(value);

  @override
  DescripcionValidationError validator(String value) {
    return null;
  }
}

enum OximetriaReferenciaValidationError { invalid }
class OximetriaReferenciaInput extends FormzInput<String, OximetriaReferenciaValidationError> {
  const OximetriaReferenciaInput.pure() : super.pure('');
  const OximetriaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  OximetriaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum TemperaturaReferenciaValidationError { invalid }
class TemperaturaReferenciaInput extends FormzInput<String, TemperaturaReferenciaValidationError> {
  const TemperaturaReferenciaInput.pure() : super.pure('');
  const TemperaturaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  TemperaturaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum RitmoCardiacoReferenciaValidationError { invalid }
class RitmoCardiacoReferenciaInput extends FormzInput<String, RitmoCardiacoReferenciaValidationError> {
  const RitmoCardiacoReferenciaInput.pure() : super.pure('');
  const RitmoCardiacoReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  RitmoCardiacoReferenciaValidationError validator(String value) {
    return null;
  }
}

enum PresionSistolicaReferenciaValidationError { invalid }
class PresionSistolicaReferenciaInput extends FormzInput<String, PresionSistolicaReferenciaValidationError> {
  const PresionSistolicaReferenciaInput.pure() : super.pure('');
  const PresionSistolicaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionSistolicaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum PresionDiastolicaReferenciaValidationError { invalid }
class PresionDiastolicaReferenciaInput extends FormzInput<String, PresionDiastolicaReferenciaValidationError> {
  const PresionDiastolicaReferenciaInput.pure() : super.pure('');
  const PresionDiastolicaReferenciaInput.dirty([String value = '']) : super.dirty(value);

  @override
  PresionDiastolicaReferenciaValidationError validator(String value) {
    return null;
  }
}

enum FechaActualizacionValidationError { invalid }
class FechaActualizacionInput extends FormzInput<DateTime, FechaActualizacionValidationError> {
  FechaActualizacionInput.pure() : super.pure(DateTime.now());
  FechaActualizacionInput.dirty([DateTime value]) : super.dirty(value);

  @override
  FechaActualizacionValidationError validator(DateTime value) {
    return null;
  }
}

