part of 'condicion_bloc.dart';

enum CondicionStatusUI {init, loading, error, done}
enum CondicionDeleteStatus {ok, ko, none}

class CondicionState extends Equatable {
  final List<Condicion> condicions;
  final Condicion loadedCondicion;
  final bool editMode;
  final CondicionDeleteStatus deleteStatus;
  final CondicionStatusUI condicionStatusUI;

  final FormzStatus formStatus;
  final String generalNotificationKey;

  final DescripcionInput descripcion;
  final OximetriaReferenciaInput oximetriaReferencia;
  final TemperaturaReferenciaInput temperaturaReferencia;
  final RitmoCardiacoReferenciaInput ritmoCardiacoReferencia;
  final PresionSistolicaReferenciaInput presionSistolicaReferencia;
  final PresionDiastolicaReferenciaInput presionDiastolicaReferencia;
  final FechaActualizacionInput fechaActualizacion;

  
  CondicionState(
FechaActualizacionInput fechaActualizacion,{
    this.condicions = const [],
    this.condicionStatusUI = CondicionStatusUI.init,
    this.loadedCondicion = const Condicion(0,'','','','','','',null,),
    this.editMode = false,
    this.formStatus = FormzStatus.pure,
    this.generalNotificationKey = '',
    this.deleteStatus = CondicionDeleteStatus.none,
    this.descripcion = const DescripcionInput.pure(),
    this.oximetriaReferencia = const OximetriaReferenciaInput.pure(),
    this.temperaturaReferencia = const TemperaturaReferenciaInput.pure(),
    this.ritmoCardiacoReferencia = const RitmoCardiacoReferenciaInput.pure(),
    this.presionSistolicaReferencia = const PresionSistolicaReferenciaInput.pure(),
    this.presionDiastolicaReferencia = const PresionDiastolicaReferenciaInput.pure(),
  }):this.fechaActualizacion = fechaActualizacion ?? FechaActualizacionInput.pure()
;

  CondicionState copyWith({
    List<Condicion> condicions,
    CondicionStatusUI condicionStatusUI,
    bool editMode,
    CondicionDeleteStatus deleteStatus,
    Condicion loadedCondicion,
    FormzStatus formStatus,
    String generalNotificationKey,
    DescripcionInput descripcion,
    OximetriaReferenciaInput oximetriaReferencia,
    TemperaturaReferenciaInput temperaturaReferencia,
    RitmoCardiacoReferenciaInput ritmoCardiacoReferencia,
    PresionSistolicaReferenciaInput presionSistolicaReferencia,
    PresionDiastolicaReferenciaInput presionDiastolicaReferencia,
    FechaActualizacionInput fechaActualizacion,
  }) {
    return CondicionState(
        fechaActualizacion,
      condicions: condicions ?? this.condicions,
      condicionStatusUI: condicionStatusUI ?? this.condicionStatusUI,
      loadedCondicion: loadedCondicion ?? this.loadedCondicion,
      editMode: editMode ?? this.editMode,
      formStatus: formStatus ?? this.formStatus,
      generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
      deleteStatus: deleteStatus ?? this.deleteStatus,
      descripcion: descripcion ?? this.descripcion,
      oximetriaReferencia: oximetriaReferencia ?? this.oximetriaReferencia,
      temperaturaReferencia: temperaturaReferencia ?? this.temperaturaReferencia,
      ritmoCardiacoReferencia: ritmoCardiacoReferencia ?? this.ritmoCardiacoReferencia,
      presionSistolicaReferencia: presionSistolicaReferencia ?? this.presionSistolicaReferencia,
      presionDiastolicaReferencia: presionDiastolicaReferencia ?? this.presionDiastolicaReferencia,
    );
  }

  @override
  List<Object> get props => [condicions, condicionStatusUI,
     loadedCondicion, editMode, deleteStatus, formStatus, generalNotificationKey, 
descripcion,oximetriaReferencia,temperaturaReferencia,ritmoCardiacoReferencia,presionSistolicaReferencia,presionDiastolicaReferencia,fechaActualizacion,];

  @override
  bool get stringify => true;
}
