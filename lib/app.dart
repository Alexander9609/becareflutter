import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movilRCV2/account/login/bloc/login_bloc.dart';
import 'package:movilRCV2/account/login/login_repository.dart';
import 'package:movilRCV2/account/register/bloc/register_bloc.dart';
import 'package:movilRCV2/account/settings/settings_screen.dart';
import 'package:movilRCV2/main/bloc/main_bloc.dart';
import 'package:movilRCV2/routes.dart';
import 'package:movilRCV2/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:movilRCV2/shared/repository/account_repository.dart';
import 'package:movilRCV2/themes.dart';
import 'account/settings/bloc/settings_bloc.dart';
import 'package:movilRCV2/shared/models/entity_arguments.dart';

import 'account/login/login_screen.dart';
import 'account/register/register_screen.dart';


import 'package:movilRCV2/entities/alarma/bloc/alarma_bloc.dart'; 
import 'package:movilRCV2/entities/alarma/alarma_list_screen.dart';
import 'package:movilRCV2/entities/alarma/alarma_update_screen.dart';
import 'package:movilRCV2/entities/alarma/alarma_view_screen.dart';
import 'package:movilRCV2/entities/alarma/alarma_repository.dart';
import 'package:movilRCV2/entities/condicion/bloc/condicion_bloc.dart'; 
import 'package:movilRCV2/entities/condicion/condicion_list_screen.dart';
import 'package:movilRCV2/entities/condicion/condicion_update_screen.dart';
import 'package:movilRCV2/entities/condicion/condicion_view_screen.dart';
import 'package:movilRCV2/entities/condicion/condicion_repository.dart';
import 'package:movilRCV2/entities/dispositivos/bloc/dispositivos_bloc.dart'; 
import 'package:movilRCV2/entities/dispositivos/dispositivos_list_screen.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_update_screen.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_view_screen.dart';
import 'package:movilRCV2/entities/dispositivos/dispositivos_repository.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/bloc/encuesta_sintomas_bloc.dart'; 
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_list_screen.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_update_screen.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_view_screen.dart';
import 'package:movilRCV2/entities/encuesta_sintomas/encuesta_sintomas_repository.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/bloc/estadio_presion_arterial_bloc.dart'; 
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_list_screen.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_update_screen.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_view_screen.dart';
import 'package:movilRCV2/entities/estadio_presion_arterial/estadio_presion_arterial_repository.dart';
import 'package:movilRCV2/entities/eventos/bloc/eventos_bloc.dart'; 
import 'package:movilRCV2/entities/eventos/eventos_list_screen.dart';
import 'package:movilRCV2/entities/eventos/eventos_update_screen.dart';
import 'package:movilRCV2/entities/eventos/eventos_view_screen.dart';
import 'package:movilRCV2/entities/eventos/eventos_repository.dart';
import 'package:movilRCV2/entities/fisiometria_1/bloc/fisiometria_1_bloc.dart'; 
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_list_screen.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_update_screen.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_view_screen.dart';
import 'package:movilRCV2/entities/fisiometria_1/fisiometria_1_repository.dart';
import 'package:movilRCV2/entities/ips/bloc/ips_bloc.dart'; 
import 'package:movilRCV2/entities/ips/ips_list_screen.dart';
import 'package:movilRCV2/entities/ips/ips_update_screen.dart';
import 'package:movilRCV2/entities/ips/ips_view_screen.dart';
import 'package:movilRCV2/entities/ips/ips_repository.dart';
import 'package:movilRCV2/entities/medicacion/bloc/medicacion_bloc.dart'; 
import 'package:movilRCV2/entities/medicacion/medicacion_list_screen.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_update_screen.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_view_screen.dart';
import 'package:movilRCV2/entities/medicacion/medicacion_repository.dart';
import 'package:movilRCV2/entities/paciente/bloc/paciente_bloc.dart'; 
import 'package:movilRCV2/entities/paciente/paciente_list_screen.dart';
import 'package:movilRCV2/entities/paciente/paciente_update_screen.dart';
import 'package:movilRCV2/entities/paciente/paciente_view_screen.dart';
import 'package:movilRCV2/entities/paciente/paciente_repository.dart';
import 'package:movilRCV2/entities/presion_sanguinea/bloc/presion_sanguinea_bloc.dart'; 
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_list_screen.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_update_screen.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_view_screen.dart';
import 'package:movilRCV2/entities/presion_sanguinea/presion_sanguinea_repository.dart';
// jhipster-merlin-needle-import-add - JHipster will add new imports here

class MovilRcv2App extends StatelessWidget {
  const MovilRcv2App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MovilRcv2 app',
      theme: Themes.jhLight,
      routes: {
        MovilRcv2Routes.login: (context) {
          return BlocProvider<LoginBloc>(
            create: (context) => LoginBloc(loginRepository: LoginRepository()),
            child: LoginScreen());
        },
        MovilRcv2Routes.register: (context) {
          return BlocProvider<RegisterBloc>(
            create: (context) => RegisterBloc(accountRepository: AccountRepository()),
            child: RegisterScreen());
        },
        MovilRcv2Routes.main: (context) {
          return BlocProvider<MainBloc>(
            create: (context) => MainBloc(accountRepository: AccountRepository())
              ..add(Init()),
            child: MainScreen());
        },
      MovilRcv2Routes.settings: (context) {
        return BlocProvider<SettingsBloc>(
          create: (context) => SettingsBloc(accountRepository: AccountRepository())
            ..add(LoadCurrentUser()),
          child: SettingsScreen());
        },
        MovilRcv2Routes.entitiesAlarmaList: (context) {
          return BlocProvider<AlarmaBloc>(
            create: (context) => AlarmaBloc(alarmaRepository: AlarmaRepository())
            ..add(InitAlarmaList()),
            child: AlarmaListScreen());
          },
        MovilRcv2Routes.entitiesAlarmaCreate: (context) {
                return BlocProvider<AlarmaBloc>(
                  create: (context) => AlarmaBloc(alarmaRepository: AlarmaRepository()),
                  child: AlarmaUpdateScreen());
                },
        MovilRcv2Routes.entitiesAlarmaEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<AlarmaBloc>(
                   create: (context) => AlarmaBloc(alarmaRepository: AlarmaRepository())
                  ..add(LoadAlarmaByIdForEdit(id: arguments.id)),
                child: AlarmaUpdateScreen());
            },
        MovilRcv2Routes.entitiesAlarmaView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<AlarmaBloc>(
                    create: (context) => AlarmaBloc(alarmaRepository: AlarmaRepository())
                    ..add(LoadAlarmaByIdForView(id: arguments.id)),
                    child: AlarmaViewScreen());
                },
        MovilRcv2Routes.entitiesCondicionList: (context) {
          return BlocProvider<CondicionBloc>(
            create: (context) => CondicionBloc(condicionRepository: CondicionRepository())
            ..add(InitCondicionList()),
            child: CondicionListScreen());
          },
        MovilRcv2Routes.entitiesCondicionCreate: (context) {
                return BlocProvider<CondicionBloc>(
                  create: (context) => CondicionBloc(condicionRepository: CondicionRepository()),
                  child: CondicionUpdateScreen());
                },
        MovilRcv2Routes.entitiesCondicionEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<CondicionBloc>(
                   create: (context) => CondicionBloc(condicionRepository: CondicionRepository())
                  ..add(LoadCondicionByIdForEdit(id: arguments.id)),
                child: CondicionUpdateScreen());
            },
        MovilRcv2Routes.entitiesCondicionView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<CondicionBloc>(
                    create: (context) => CondicionBloc(condicionRepository: CondicionRepository())
                    ..add(LoadCondicionByIdForView(id: arguments.id)),
                    child: CondicionViewScreen());
                },
        MovilRcv2Routes.entitiesDispositivosList: (context) {
          return BlocProvider<DispositivosBloc>(
            create: (context) => DispositivosBloc(dispositivosRepository: DispositivosRepository())
            ..add(InitDispositivosList()),
            child: DispositivosListScreen());
          },
        MovilRcv2Routes.entitiesDispositivosCreate: (context) {
                return BlocProvider<DispositivosBloc>(
                  create: (context) => DispositivosBloc(dispositivosRepository: DispositivosRepository()),
                  child: DispositivosUpdateScreen());
                },
        MovilRcv2Routes.entitiesDispositivosEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<DispositivosBloc>(
                   create: (context) => DispositivosBloc(dispositivosRepository: DispositivosRepository())
                  ..add(LoadDispositivosByIdForEdit(id: arguments.id)),
                child: DispositivosUpdateScreen());
            },
        MovilRcv2Routes.entitiesDispositivosView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<DispositivosBloc>(
                    create: (context) => DispositivosBloc(dispositivosRepository: DispositivosRepository())
                    ..add(LoadDispositivosByIdForView(id: arguments.id)),
                    child: DispositivosViewScreen());
                },
        MovilRcv2Routes.entitiesEncuestaSintomasList: (context) {
          return BlocProvider<EncuestaSintomasBloc>(
            create: (context) => EncuestaSintomasBloc(encuestaSintomasRepository: EncuestaSintomasRepository())
            ..add(InitEncuestaSintomasList()),
            child: EncuestaSintomasListScreen());
          },
        MovilRcv2Routes.entitiesEncuestaSintomasCreate: (context) {
                return BlocProvider<EncuestaSintomasBloc>(
                  create: (context) => EncuestaSintomasBloc(encuestaSintomasRepository: EncuestaSintomasRepository()),
                  child: EncuestaSintomasUpdateScreen());
                },
        MovilRcv2Routes.entitiesEncuestaSintomasEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EncuestaSintomasBloc>(
                   create: (context) => EncuestaSintomasBloc(encuestaSintomasRepository: EncuestaSintomasRepository())
                  ..add(LoadEncuestaSintomasByIdForEdit(id: arguments.id)),
                child: EncuestaSintomasUpdateScreen());
            },
        MovilRcv2Routes.entitiesEncuestaSintomasView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EncuestaSintomasBloc>(
                    create: (context) => EncuestaSintomasBloc(encuestaSintomasRepository: EncuestaSintomasRepository())
                    ..add(LoadEncuestaSintomasByIdForView(id: arguments.id)),
                    child: EncuestaSintomasViewScreen());
                },
        MovilRcv2Routes.entitiesEstadioPresionArterialList: (context) {
          return BlocProvider<EstadioPresionArterialBloc>(
            create: (context) => EstadioPresionArterialBloc(estadioPresionArterialRepository: EstadioPresionArterialRepository())
            ..add(InitEstadioPresionArterialList()),
            child: EstadioPresionArterialListScreen());
          },
        MovilRcv2Routes.entitiesEstadioPresionArterialCreate: (context) {
                return BlocProvider<EstadioPresionArterialBloc>(
                  create: (context) => EstadioPresionArterialBloc(estadioPresionArterialRepository: EstadioPresionArterialRepository()),
                  child: EstadioPresionArterialUpdateScreen());
                },
        MovilRcv2Routes.entitiesEstadioPresionArterialEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EstadioPresionArterialBloc>(
                   create: (context) => EstadioPresionArterialBloc(estadioPresionArterialRepository: EstadioPresionArterialRepository())
                  ..add(LoadEstadioPresionArterialByIdForEdit(id: arguments.id)),
                child: EstadioPresionArterialUpdateScreen());
            },
        MovilRcv2Routes.entitiesEstadioPresionArterialView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EstadioPresionArterialBloc>(
                    create: (context) => EstadioPresionArterialBloc(estadioPresionArterialRepository: EstadioPresionArterialRepository())
                    ..add(LoadEstadioPresionArterialByIdForView(id: arguments.id)),
                    child: EstadioPresionArterialViewScreen());
                },
        MovilRcv2Routes.entitiesEventosList: (context) {
          return BlocProvider<EventosBloc>(
            create: (context) => EventosBloc(eventosRepository: EventosRepository())
            ..add(InitEventosList()),
            child: EventosListScreen());
          },
        MovilRcv2Routes.entitiesEventosCreate: (context) {
                return BlocProvider<EventosBloc>(
                  create: (context) => EventosBloc(eventosRepository: EventosRepository()),
                  child: EventosUpdateScreen());
                },
        MovilRcv2Routes.entitiesEventosEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EventosBloc>(
                   create: (context) => EventosBloc(eventosRepository: EventosRepository())
                  ..add(LoadEventosByIdForEdit(id: arguments.id)),
                child: EventosUpdateScreen());
            },
        MovilRcv2Routes.entitiesEventosView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<EventosBloc>(
                    create: (context) => EventosBloc(eventosRepository: EventosRepository())
                    ..add(LoadEventosByIdForView(id: arguments.id)),
                    child: EventosViewScreen());
                },
        MovilRcv2Routes.entitiesFisiometria1List: (context) {
          return BlocProvider<Fisiometria1Bloc>(
            create: (context) => Fisiometria1Bloc(fisiometria1Repository: Fisiometria1Repository())
            ..add(InitFisiometria1List()),
            child: Fisiometria1ListScreen());
          },
        MovilRcv2Routes.entitiesFisiometria1Create: (context) {
                return BlocProvider<Fisiometria1Bloc>(
                  create: (context) => Fisiometria1Bloc(fisiometria1Repository: Fisiometria1Repository()),
                  child: Fisiometria1UpdateScreen());
                },
        MovilRcv2Routes.entitiesFisiometria1Edit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<Fisiometria1Bloc>(
                   create: (context) => Fisiometria1Bloc(fisiometria1Repository: Fisiometria1Repository())
                  ..add(LoadFisiometria1ByIdForEdit(id: arguments.id)),
                child: Fisiometria1UpdateScreen());
            },
        MovilRcv2Routes.entitiesFisiometria1View: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<Fisiometria1Bloc>(
                    create: (context) => Fisiometria1Bloc(fisiometria1Repository: Fisiometria1Repository())
                    ..add(LoadFisiometria1ByIdForView(id: arguments.id)),
                    child: Fisiometria1ViewScreen());
                },
        MovilRcv2Routes.entitiesIpsList: (context) {
          return BlocProvider<IpsBloc>(
            create: (context) => IpsBloc(iPSRepository: IpsRepository())
            ..add(InitIpsList()),
            child: IpsListScreen());
          },
        MovilRcv2Routes.entitiesIpsCreate: (context) {
                return BlocProvider<IpsBloc>(
                  create: (context) => IpsBloc(iPSRepository: IpsRepository()),
                  child: IpsUpdateScreen());
                },
        MovilRcv2Routes.entitiesIpsEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<IpsBloc>(
                   create: (context) => IpsBloc(iPSRepository: IpsRepository())
                  ..add(LoadIpsByIdForEdit(id: arguments.id)),
                child: IpsUpdateScreen());
            },
        MovilRcv2Routes.entitiesIpsView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<IpsBloc>(
                    create: (context) => IpsBloc(iPSRepository: IpsRepository())
                    ..add(LoadIpsByIdForView(id: arguments.id)),
                    child: IpsViewScreen());
                },
        MovilRcv2Routes.entitiesMedicacionList: (context) {
          return BlocProvider<MedicacionBloc>(
            create: (context) => MedicacionBloc(medicacionRepository: MedicacionRepository())
            ..add(InitMedicacionList()),
            child: MedicacionListScreen());
          },
        MovilRcv2Routes.entitiesMedicacionCreate: (context) {
                return BlocProvider<MedicacionBloc>(
                  create: (context) => MedicacionBloc(medicacionRepository: MedicacionRepository()),
                  child: MedicacionUpdateScreen());
                },
        MovilRcv2Routes.entitiesMedicacionEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<MedicacionBloc>(
                   create: (context) => MedicacionBloc(medicacionRepository: MedicacionRepository())
                  ..add(LoadMedicacionByIdForEdit(id: arguments.id)),
                child: MedicacionUpdateScreen());
            },
        MovilRcv2Routes.entitiesMedicacionView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<MedicacionBloc>(
                    create: (context) => MedicacionBloc(medicacionRepository: MedicacionRepository())
                    ..add(LoadMedicacionByIdForView(id: arguments.id)),
                    child: MedicacionViewScreen());
                },
        MovilRcv2Routes.entitiesPacienteList: (context) {
          return BlocProvider<PacienteBloc>(
            create: (context) => PacienteBloc(pacienteRepository: PacienteRepository())
            ..add(InitPacienteList()),
            child: PacienteListScreen());
          },
        MovilRcv2Routes.entitiesPacienteCreate: (context) {
                return BlocProvider<PacienteBloc>(
                  create: (context) => PacienteBloc(pacienteRepository: PacienteRepository()),
                  child: PacienteUpdateScreen());
                },
        MovilRcv2Routes.entitiesPacienteEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<PacienteBloc>(
                   create: (context) => PacienteBloc(pacienteRepository: PacienteRepository())
                  ..add(LoadPacienteByIdForEdit(id: arguments.id)),
                child: PacienteUpdateScreen());
            },
        MovilRcv2Routes.entitiesPacienteView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<PacienteBloc>(
                    create: (context) => PacienteBloc(pacienteRepository: PacienteRepository())
                    ..add(LoadPacienteByIdForView(id: arguments.id)),
                    child: PacienteViewScreen());
                },
        MovilRcv2Routes.entitiesPresionSanguineaList: (context) {
          return BlocProvider<PresionSanguineaBloc>(
            create: (context) => PresionSanguineaBloc(presionSanguineaRepository: PresionSanguineaRepository())
            ..add(InitPresionSanguineaList()),
            child: PresionSanguineaListScreen());
          },
        MovilRcv2Routes.entitiesPresionSanguineaCreate: (context) {
                return BlocProvider<PresionSanguineaBloc>(
                  create: (context) => PresionSanguineaBloc(presionSanguineaRepository: PresionSanguineaRepository()),
                  child: PresionSanguineaUpdateScreen());
                },
        MovilRcv2Routes.entitiesPresionSanguineaEdit: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<PresionSanguineaBloc>(
                   create: (context) => PresionSanguineaBloc(presionSanguineaRepository: PresionSanguineaRepository())
                  ..add(LoadPresionSanguineaByIdForEdit(id: arguments.id)),
                child: PresionSanguineaUpdateScreen());
            },
        MovilRcv2Routes.entitiesPresionSanguineaView: (context) {
            EntityArguments arguments = ModalRoute.of(context).settings.arguments;
                return BlocProvider<PresionSanguineaBloc>(
                    create: (context) => PresionSanguineaBloc(presionSanguineaRepository: PresionSanguineaRepository())
                    ..add(LoadPresionSanguineaByIdForView(id: arguments.id)),
                    child: PresionSanguineaViewScreen());
                },
        // jhipster-merlin-needle-route-add - JHipster will add new routes here
      },
    );
  }
}
